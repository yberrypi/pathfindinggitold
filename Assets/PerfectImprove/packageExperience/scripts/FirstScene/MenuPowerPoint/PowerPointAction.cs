﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerPointAction : MonoBehaviour {

	public List<string> listeIndex;
	public List<Texture> listeImage;
	int slideActuel=0;
	public GameObject textAssocie;
	public RawImage imageAssocie;
	public Text numero;
	public GameObject btnLancerDemo;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnNext(){
		slideActuel++;
		if (slideActuel > Mathf.Min (listeImage.Count, listeIndex.Count)) {
			reset ();
		} else {
			setImageText ();
		}
	}

	public void OnPrevious(){
		slideActuel--;
		if (slideActuel < 0) {
			slideActuel = Mathf.Min (listeImage.Count, listeIndex.Count)-1;
			setImageText ();
		} else {
			setImageText ();
		}
	}

	public void reset(){
		slideActuel = 0;
		setImageText ();
	}

	void setImageText(){
		textAssocie.GetComponent<ChoixMot> ().changeMot (listeIndex [slideActuel]);
		imageAssocie.texture = listeImage [slideActuel];
		numero.text = (slideActuel + 1).ToString () + "/" + Mathf.Min (listeImage.Count, listeIndex.Count);
		if ((slideActuel + 1) == Mathf.Min (listeImage.Count, listeIndex.Count)) {
			btnLancerDemo.SetActive (true);

		} else {
			btnLancerDemo.SetActive (false);
		}
	}
}
