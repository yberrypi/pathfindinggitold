﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene1Manager : MonoBehaviour {
	public GameObject canvasLangue;
	public GameObject canvasInfo;
	public GameObject CanvasBoutonRetourPowerPoint;
	public GameObject camera;
	public List<GameObject> listePresentoirs;


	public void ValiderLangue(){
		canvasLangue.SetActive (true);
		canvasInfo.SetActive (true);
		if (camera.GetComponent<BougerObjet> ().sens) {
			canvasLangue.GetComponent<BougerObjet> ().delai = 0;
			canvasLangue.GetComponent<BougerObjet> ().Tempsdeplacement = 2;
		} else {
			canvasLangue.GetComponent<BougerObjet> ().delai = 2;
			canvasLangue.GetComponent<BougerObjet> ().Tempsdeplacement = 4;
		}


		canvasLangue.GetComponent<BougerObjet> ().changePosition ();

		canvasInfo.GetComponent<BougerObjet> ().changePosition ();
		canvasInfo.GetComponent<PowerPointAction> ().reset ();
		camera.GetComponent<BougerObjet> ().changePosition ();
	}

	public void OuvrirChoixDemo(){
		canvasInfo.SetActive (true);
		CanvasBoutonRetourPowerPoint.SetActive (true);
		foreach (GameObject g in listePresentoirs) {
			g.SetActive (true);
		}
		canvasInfo.GetComponent<BougerObjet> ().changePosition ();
		CanvasBoutonRetourPowerPoint.GetComponent<BougerObjet> ().changePosition ();
		canvasInfo.GetComponent<PowerPointAction> ().reset ();
		foreach (GameObject g in listePresentoirs) {
			g.GetComponent<BougerObjet> ().changePosition ();
			g.GetComponent<MakeObjectAppear> ().shown = !g.GetComponent<MakeObjectAppear> ().shown;
		}
	}



	public void OpenScene2(){
		SceneManager.LoadScene (1);
	}

	public void OpenSceneExperience(){
		SceneManager.LoadScene (2);
	}


	public void ManageGameDescrp(GameObject gcurent){
		foreach(GameObject g in listePresentoirs){
			g.GetComponent<MakeObjectAppear>().FermerBoardAssocie();
		}
		gcurent.GetComponent<MakeObjectAppear> ().OuvrirBoardAssocie ();

	}
}
