﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangementLangue : MonoBehaviour {
	public GameObject HighlightAssocie;
	public string langueAssocie;
	// Use this for initialization
	void Start () {
		if (ParametreGeneral.langue != langueAssocie) {
			HighlightAssocie.SetActive (false);
		} else {
			HighlightAssocie.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (ParametreGeneral.langue != langueAssocie) {
			HighlightAssocie.SetActive (false);
		} else {
			HighlightAssocie.SetActive (true);
		}
	}

	public void OnClick(){
		ParametreGeneral.langue = langueAssocie;
	}
}
