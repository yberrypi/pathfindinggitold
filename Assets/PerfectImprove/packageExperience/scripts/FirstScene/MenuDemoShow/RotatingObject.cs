﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObject : MonoBehaviour {
	public bool rotate=false;
	public float vitesseRotation;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (rotate) {
			transform.Rotate(new Vector3(0,360*Time.deltaTime/vitesseRotation,0));
		}
	}
}
