﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeObjectAppear : MonoBehaviour {
	public bool shown;
	public GameObject objetAssocie;
	public GameObject feuEmmission;
	public GameObject boardAssocie;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (shown && !GetComponent<BougerObjet> ().move) {
			objetAssocie.SetActive (true);
			objetAssocie.GetComponent<RotatingObject> ().rotate = true;
			feuEmmission.SetActive (true);
		} else {
			objetAssocie.SetActive (false);
			objetAssocie.GetComponent<RotatingObject> ().rotate = false;
			feuEmmission.SetActive (false);
			boardAssocie.SetActive (false);
		}
	}

	public void OpenBoardAssocie(){
		GameObject.Find ("SceneManager").GetComponent<Scene1Manager>().ManageGameDescrp(gameObject);
	}

	public void FermerBoardAssocie(){
		boardAssocie.SetActive (false);
	}

	public void OuvrirBoardAssocie(){
		boardAssocie.SetActive (true);
	}
}
