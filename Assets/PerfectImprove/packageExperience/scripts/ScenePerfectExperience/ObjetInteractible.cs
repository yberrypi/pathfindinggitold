﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetInteractible : MonoBehaviour {
	public int id;
	public bool validate;
	public GameObject Rouge;
	public GameObject jaune;
	public GameObject vert;
	public bool gauche;
	public GameObject fleche;
    public AudioClip clipRight;
    public AudioClip clipWrong;
	public string denomination;

    private AudioSource source;
    private Collider currentCol = null;

	public EspaceManettes emAssocie;

	// Use this for initialization
	void Start () {
		validate = false;
		/*
        if (!MainGauche)
        {
            MainGauche = GameObject.Find("ManetteGauche").transform;
        }
        if (!MainDroite)
        {
            MainDroite = GameObject.Find("ManetteDroite").transform;
        }
		*/
        source = GetComponent<AudioSource>();
        if (!source)
        {
            source = gameObject.AddComponent<AudioSource>();
        }
	}

	public void ObjectValidate()
    {
        StartCoroutine(ValidateTimer());
	}

    IEnumerator ValidateTimer()
    {
        if (id == SceneManagerExperience.CurrentObject.id)
        {
            if (SceneManagerExperience.sound)
            {
                source.PlayOneShot(clipRight);
            }
            validate = true;
            ChangeColor("vert");
            fleche.SetActive(false);
        }
        else
        {
            ChangeColor("rouge");
            if (SceneManagerExperience.sound)
            {
                source.PlayOneShot(clipWrong);
            }
        }

        yield return new WaitForSeconds(1f);

        ChangeColor("rien");
    }

	public void LancerMain(){

		emAssocie.LancerMain (gauche,transform.position);
    }

	public void BecomeCurrent(){
		validate = false;
		if (SceneManagerExperience.difficulty == 0) {
			fleche.SetActive (true);
		}
		ChangeColor ("jaune");
	}

	public void reset(){
		validate = false;
		ChangeColor("rien");
		fleche.SetActive (false);
	}

	void ChangeColor(string s){
		if (s == "vert" || SceneManagerExperience.difficulty < 2) {

            Rouge.SetActive(s == "rouge");
            jaune.SetActive(s == "jaune");
            vert.SetActive(s == "vert");

		} else {
			Rouge.SetActive (false);
			jaune.SetActive (false);
			vert.SetActive (false);
		}
	}

	private void OnTriggerEnter(Collider col){
        if (!currentCol)
        {
            currentCol = col;

			Transform main = gauche ? emAssocie.manetteGauche.transform : emAssocie.manetteDroite.transform;

            if (col.name == main.name)
            {
                BougerObjet bo = main.GetComponent<BougerObjet>();

                bo.arrive = main.localPosition;
                bo.objectifAtteint();
                ObjectValidate();
                bo.changePosition();
                bo.BlockRaycast = true;
            }
        }
	}

    private void OnTriggerExit(Collider other)
    {
        if (currentCol == other)
        {
            currentCol = null;
        }
    }
}
