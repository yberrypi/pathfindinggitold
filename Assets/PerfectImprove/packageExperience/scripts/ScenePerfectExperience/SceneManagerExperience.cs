﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class SceneManagerExperience : MonoBehaviour {
	
	public List<Produit> lProduit;

	public EspaceManettes emAssocie;
	public Material perfectMaterial;
	public GameObject back;
	public Text denominationProduitText;
	public bool estJoueur;
	public bool finishBoucle;

    List<ObjetInteractible> lObjet;
    float tmpsSequentiel = 0f;

    public static int difficulty = 0;
    public static GameObject currentMovingObject;
    public static ObjetInteractible CurrentObject;
    public static Produit currentProduit;
    public static int numeroCurrentObject = 0;
    public static int stock = 0;
    public static bool sound = true;
    public static float tempsTotal = 0f;
    public static List<List<float>> ListTemps;
    public static bool viveCanPick = false;

    // Use this for initialization
    void Start () {

		int i = 1;
		foreach(Produit p in lProduit){
			foreach(ObjetInteractible obj in p.lobj){
				if (obj.id == 0) {
					obj.id = i;
					i++;
				}
			}
		}
		creerListTemps ();

		/*
		if (estJoueur) {
			currentProduit = newProduct ();
			GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ().changeProduit (currentProduit.name);
		
			lObjet = currentProduit.lobj;
			denominationProduitText.text = currentProduit.denominationProduit;
			CurrentObject = lObjet [numeroCurrentObject];
			CurrentObject.BecomeCurrent ();
			moveObjectNextStep (numeroCurrentObject);

		}
		*/

	}
	
	// Update is called once per frame
	void Update () {
		
		if (CurrentObject != null && CurrentObject.validate) {
			next ();
		}
		tempsTotal += Time.deltaTime;
		tmpsSequentiel += Time.deltaTime;
	}

	public void reset(){

		if (lObjet != null) {
			foreach (ObjetInteractible obj in lObjet) {
				obj.reset ();
			}
		}
		if (estJoueur) {
			currentProduit = newProduct ();
			NetworkManager.instance.changeProduit (currentProduit.name);
		
			denominationProduitText.text = currentProduit.denominationProduit;
			lObjet = currentProduit.lobj;
			numeroCurrentObject = 0;
			CurrentObject = lObjet [numeroCurrentObject];
			CurrentObject.BecomeCurrent ();
			if (ListTemps != null) {
				creerListTemps ();
			}
			moveObjectNextStep (numeroCurrentObject);
		}

	}

	public void next(){
		
		if (numeroCurrentObject == 0) {
			tmpsSequentiel = 0f;

		} else {
			ListTemps [1] [numeroCurrentObject - 1] = tmpsSequentiel;
			tmpsSequentiel = 0f;
		}
		if (numeroCurrentObject < lObjet.Count - 1) {
			
			numeroCurrentObject++;
			moveObjectNextStep (numeroCurrentObject);
			CurrentObject = lObjet [numeroCurrentObject];

			CurrentObject.BecomeCurrent ();
		} else {
			setTotal ();
			moveObjectNextStep (lObjet.Count);
			SequenceFini ();
		}
		NetworkManager.instance.changeNumSeq (numeroCurrentObject);
	}

	public void SequenceFini(){
		if (difficulty == 0) {
			difficulty++;
		}
		if (difficulty > 2) {
			difficulty = 1;
		}
		reset ();
	}
	public void launchNextScene(){

	}

	public void moveObjectNextStep(int step){

        ObjetInteractible oi = CurrentObject.GetComponent<ObjetInteractible>();

        switch (currentProduit.name){
		case "A":
			switch (step) { 
			case 0:
				GameObject TemoinA = currentProduit.Temoin;
				GameObject nouv = Instantiate (TemoinA);
				nouv.SetActive (true);
                nouv.transform.SetParent(TemoinA.transform.parent);
                nouv.transform.localScale = TemoinA.transform.localScale;
                nouv.transform.localPosition = TemoinA.transform.localPosition;
				nouv.name = "currentMovingObject";
				currentMovingObject = nouv;
                viveCanPick = false;

				break;

			case 1:
                currentMovingObject.GetComponent<BougerObjet>().changePosition();
				break;

			case 2:
                currentMovingObject.transform.localPosition = new Vector3(1.516f, 2.386f, 1.007f);
                Renderer ren = currentMovingObject.GetComponent<Renderer>();
				ren.material = perfectMaterial;
                ren.material.color = Random.ColorHSV(0f, 1f, 0f, 1f, 1f, 1f);
                viveCanPick = true;
				break;

			case 3:
				if (!ManageVive.IsVivePresent) {
					if (oi.gauche) {
						currentMovingObject.transform.SetParent (emAssocie.manetteGauche.transform);
					} else {
						currentMovingObject.transform.SetParent (emAssocie.manetteDroite.transform);
					}
					currentMovingObject.transform.localPosition = new Vector3 (21, -416, 293);
				}
				emAssocie.haveObjectInHand = true;
				emAssocie.objetPossede = currentMovingObject;
                viveCanPick = false;
                break;

			case 4:
				emAssocie.haveObjectInHand = false;
				emAssocie.objetPossede = null;
                currentMovingObject.transform.parent = back.transform;
                int j = stock % 4;
                int i = stock % 8;
                if (i > 3)
                {
                    i = 1;
                }
                else
                {
                    i = 0;
                }

                int k = stock / 8;


                currentMovingObject.transform.localPosition = new Vector3(-0.0005f - i * 0.002f, -0.009f + j * 0.0016f, 0.00499f + k * 0.002f);
				currentMovingObject.name = "stockA_n°" + stock.ToString ();
                currentMovingObject = null;
				stock++;

				finishBoucle = true;
				break;

			default:

				break;


			}
			break;

		case "B":
			switch (step) { 
			case 0:
				GameObject TemoinB = currentProduit.Temoin;
				GameObject nouv = Instantiate (TemoinB);
				nouv.SetActive (true);
				nouv.transform.SetParent (TemoinB.transform.parent);
				nouv.transform.localScale = TemoinB.transform.localScale;
				nouv.transform.localPosition = TemoinB.transform.localPosition;
                nouv.name = "currentMovingObject";
				currentMovingObject = nouv;
                viveCanPick = true;
				break;

			case 1:

                if (!ManageVive.IsVivePresent)
                {
                    if (oi.gauche)
                    {
						currentMovingObject.transform.SetParent(emAssocie.manetteGauche.transform);
                    }
                    else
                    {
						currentMovingObject.transform.SetParent(emAssocie.manetteDroite.transform);
                    }
                    currentMovingObject.transform.localPosition = new Vector3(21, -416, 293);
                    currentMovingObject.transform.localRotation = Quaternion.identity;
                }
				emAssocie.haveObjectInHand = true;
				emAssocie.objetPossede = currentMovingObject;
                viveCanPick = false;
				break;

			case 2:
				emAssocie.haveObjectInHand = false;
				emAssocie.objetPossede = null;
				currentMovingObject.transform.SetParent (currentProduit.Temoin.transform.parent);
				currentMovingObject.transform.localPosition = new Vector3 (1.516f, 2.386f, 1.007f);
				currentMovingObject.transform.localRotation = Quaternion.identity;
                break;

			case 3:
				/*currentMovingObject.transform.localPosition = new Vector3 (1.516f, 2.386f, 1.007f);
				currentMovingObject.transform.localRotation = Quaternion.identity;*/
                Renderer ren = currentMovingObject.GetComponent<Renderer>();
                ren.material = perfectMaterial;
                ren.material.color = Random.ColorHSV(0f, 1f, 0f, 1f, 1f, 1f);
                viveCanPick = true;
				break;

			case 4:
                if (!ManageVive.IsVivePresent)
                {
                    if (oi.gauche)
                    {
						currentMovingObject.transform.SetParent(emAssocie.manetteGauche.transform);
                    }
                    else
                    {
						currentMovingObject.transform.SetParent(emAssocie.manetteDroite.transform);
                    }
                    currentMovingObject.transform.localPosition = new Vector3(21, -416, 293);
                    currentMovingObject.transform.localRotation = Quaternion.identity;
                }
                viveCanPick = false;
				emAssocie.haveObjectInHand = true;
				emAssocie.objetPossede = currentMovingObject;
				break;

			case 5:
				emAssocie.haveObjectInHand = false;
				emAssocie.objetPossede = null;
                currentMovingObject.transform.parent = back.transform;
                int j = stock % 4;
                int i = stock % 8;
                if (i > 3)
                {
                    i = 1;
                }
                else
                {
                    i = 0;
                }

                int k = stock / 8;


                currentMovingObject.transform.localPosition = new Vector3(-0.0005f - i * 0.002f, -0.009f + j * 0.0016f, 0.00499f + k * 0.002f);
                currentMovingObject.transform.localEulerAngles = new Vector3(90, 0, 0);
				currentMovingObject.name = "stockB_n°" + stock.ToString ();
                currentMovingObject = null;
				stock++;

				finishBoucle = true;

				break;

			default:

				break;


			}
			break;
		}
	}

	public void creerListTemps(){
		ListTemps = new List<List<float>> ();
		for (int i = 0; i < 2; i++) {
			int max = MaxTemps ();
			List<float> temp = new List<float> ();
			for (int j = 0; j < max; j++) {
				temp.Add (0);
			}
			ListTemps.Add (temp);
		}
	}

	int MaxTemps(){
		int res = 0;
		foreach (Produit p in lProduit) {
			if (p.lobj.Count > res) {
				res = p.lobj.Count;
			}
		}
		return res;
	}

	public void setTotal(){
		float res = 0;
		for (int i = 0; i < lObjet.Count-1; i++) {
			res += ListTemps [1] [i];
		}
		ListTemps [1] [lObjet.Count - 1] = res;
	}

	Produit newProduct(){
		int r = Random.Range (0, lProduit.Count);
		return lProduit [r];
	}

	public void newProduct(string nom){
		StartCoroutine ("ieNewProd",nom);
	}

	public void validateObjectif(int num){
		if (num < lObjet.Count) {
			sound = false;
			for (int i = 0; i < num; i++) {
				numeroCurrentObject = i;
				CurrentObject = lObjet [i];
				CurrentObject.BecomeCurrent ();
				CurrentObject.ObjectValidate();
				next ();
			}
			sound = true;
		}
	}

	IEnumerator ieNewProd(string nom){
		
		Debug.Log ("nouveau produit nom: "+nom);

		while (!finishBoucle) {
			
			yield return null;
		}

		if (lObjet != null) {
			foreach (ObjetInteractible obj in lObjet) {
				obj.reset ();
			}
		}
			Produit encours = new Produit ();
			//int r = Random.Range (0, lProduit.Count);
			//encours = lProduit [r];

			foreach (Produit p in lProduit) {
				if (p.name == nom) {
					encours = p;
				} 
			}


			currentProduit = encours;
			denominationProduitText.text = currentProduit.denominationProduit;
			lObjet = currentProduit.lobj;
			numeroCurrentObject = 0;
			CurrentObject = lObjet [numeroCurrentObject];
			CurrentObject.BecomeCurrent ();
			moveObjectNextStep (numeroCurrentObject);
			creerListTemps ();
			finishBoucle = false;
		Debug.Log ("finish bouce passe a false");
		yield return null;
	}
}
