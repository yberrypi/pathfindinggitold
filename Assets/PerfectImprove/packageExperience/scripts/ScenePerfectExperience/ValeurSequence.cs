﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValeurSequence : MonoBehaviour {
	public Text denomination;
	public Text moyenne;
	public Text derniere;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeValeurTotal(string denominationObjet, float tempsMoyenne,float tempsDerniere){
		denomination.text = denominationObjet;
		moyenne.text = tempsMoyenne.ToString ("F3");
		derniere.text = tempsDerniere.ToString ("F3");
	}

	public void ChangerChamps( float tempsMoyenne,float tempsDerniere){
		moyenne.text = tempsMoyenne.ToString ("F3");
		derniere.text = tempsDerniere.ToString ("F3");
	}
}
