﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasAffichageManager : MonoBehaviour {
	public GameObject ensembleParametre;
	public GameObject boutonAffichage;
	public GameObject RetourMenu;
	public GameObject espaceToggle;
	public Text stockValeur;
	public Text tempsValeur;
	public List<GameObject> ListSequence;
	public GameObject temoinSequence;
	 int difficultyCurrent;
	int currentStock;
	// Use this for initialization
	void Start () {
		difficultyCurrent = 0;
		currentStock = 0;
		stockValeur.text = currentStock.ToString();
		initListSequence ();
		masquerParametre ();
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (SceneManagerExperience.difficulty);
		if (difficultyCurrent != SceneManagerExperience.difficulty && espaceToggle.activeSelf) {
			difficultyCurrent = SceneManagerExperience.difficulty;
			changeDifficultyToggle ();

		}
		if (currentStock != SceneManagerExperience.stock) {
			currentStock = SceneManagerExperience.stock;
			changeValeurStat ();
		}
		tempsValeur.text = SceneManagerExperience.tempsTotal.ToString ("F3");
	}

	public void afficherParametre(){
		ensembleParametre.SetActive (true);
		boutonAffichage.SetActive (false);
		RetourMenu.SetActive (false);
		changeDifficultyToggle ();


	}

	public void masquerParametre(){
		ensembleParametre.SetActive (false);
		boutonAffichage.SetActive (true);
		RetourMenu.SetActive (true);
	}

	void changeDifficultyToggle(){
		foreach (Toggle t in espaceToggle.transform.GetComponentsInChildren<Toggle>()) {
			if (t.GetComponent<ToggleForce> ().difficulteAssocie == SceneManagerExperience.difficulty) {
				t.isOn = true;
			}
		}
	}

	public void retourSceneIntro(){
		SceneManager.LoadScene (0);
	}

	 void changeValeurStat(){
		stockValeur.text = currentStock.ToString();
		int i = 0;
		foreach(GameObject g in ListSequence){
			float calculMoyenne = ((SceneManagerExperience.ListTemps [0] [i] * (currentStock - 1)) + SceneManagerExperience.ListTemps [1] [i]) / currentStock;
			SceneManagerExperience.ListTemps [0] [i] = calculMoyenne;
			g.GetComponent<ValeurSequence> ().ChangerChamps (calculMoyenne, SceneManagerExperience.ListTemps [1] [i]);
			i++;
		}
	}

	public void initListSequence(){
		
		List < ObjetInteractible > lobj= new List<ObjetInteractible> ();
		foreach(ObjetInteractible obj in GameObject.Find("SceneManager").GetComponent<SceneManagerExperience>().lProduit[0].lobj ){
			lobj.Add (obj);
		
		}

		for (int i = 0; i < lobj.Count; i++) {
			if (i < lobj.Count ) {
				GameObject nouv = Instantiate (temoinSequence) as GameObject;
				nouv.transform.SetParent (temoinSequence.transform.parent);
				nouv.transform.localScale = temoinSequence.transform.localScale;
				if (i < lobj.Count - 1) { 
					string nom = lobj [i].denomination + " / " + lobj [i + 1].denomination;
					nouv.name = nom;
					nouv.GetComponent<ValeurSequence> ().ChangeValeurTotal (nom, 0, 0);
					nouv.transform.localPosition = new Vector3 (0, 95 - 40 * i, 0);
				} else {
					string nom = "Total:";
					nouv.name = nom;
					nouv.GetComponent<ValeurSequence> ().ChangeValeurTotal (nom, 0, 0);
					nouv.transform.localPosition = new Vector3 (0, 95 - 40 * i, 0);
				}
				nouv.SetActive (true);
				ListSequence.Add (nouv);
			}
		}

	}
}
