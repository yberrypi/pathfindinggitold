﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspaceManettes : MonoBehaviour {
	public GameObject manetteGauche;
	public GameObject manetteDroite;
	public bool haveObjectInHand=false;
	public GameObject objetPossede;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void LancerMain(bool gauche,Vector3 position){

		Transform main = gauche ? manetteGauche.transform : manetteDroite.transform;
		if (haveObjectInHand) {
			objetPossede.transform.SetParent (main);
			objetPossede.transform.localPosition = new Vector3 (21, -416, 293);
		}
		BougerObjet bo = main.GetComponent<BougerObjet>();
		bo.arrive = position - main.parent.position;
		bo.changePosition();
		bo.BlockRaycast = true;
	}

}
