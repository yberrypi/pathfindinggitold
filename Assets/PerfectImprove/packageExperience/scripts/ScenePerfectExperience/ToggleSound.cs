﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSound : MonoBehaviour {
	public bool etat;

	public void OnClick(){
		if (GetComponent<Toggle> ().isOn) {
			SceneManagerExperience.sound = etat;
		} 
	}
}
