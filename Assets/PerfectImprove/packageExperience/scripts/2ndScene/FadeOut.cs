﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour {
	public float tempsFade;
	public float delai;
	public bool LaunchFade;
	public bool sens;
	float tempsPasse;
	Color w=Color.white;
	// Use this for initialization
	void start(){
		if (tempsFade == 0) {
			tempsFade = 2;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (LaunchFade) {
			Color c = w;
			tempsPasse += Time.deltaTime;
			if (tempsPasse > delai) {
				if (tempsPasse < tempsFade) {
					if (sens) {
						c.a = 1 - (tempsPasse-delai)  / (tempsFade-delai);

					} else {
						c.a =  (tempsPasse-delai) / (tempsFade-delai);
					}
				} else {
					if (sens) {
						c.a = 0;
					

					} else {
						c.a = 1;
					}
					tempsPasse = 0;
					sens = !sens;
					LaunchFade = false;
				}
				GetComponent<Image> ().color = c;
			}
		}
	}
}
