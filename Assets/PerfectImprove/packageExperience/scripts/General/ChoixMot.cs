﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChoixMot : MonoBehaviour {
	 
	public string id;
	public int indice;



	void Start(){

	}
	
	// Update is called once per frame
	void Update () {
		if (GenerateurMot.init) {
			if (indice == 0) {
				getIndice ();

			}  
			getMot ();
		}
	}

	void getIndice(){
		if (id.Length <= 0)
		{

			indice =GenerateurMot.GetInd("Default");

		}

		else

		{

			indice = GenerateurMot.GetInd (id);

		}

	}

	void getMot(){
		GetComponent<Text>().text=GenerateurMot.GetMot (indice);
	}

	public void changeMot(string nouveauMot){
		id = nouveauMot;
		indice = 0;

	}
	
}
