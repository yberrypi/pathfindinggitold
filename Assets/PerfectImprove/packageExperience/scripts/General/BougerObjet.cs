﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*classe permettant de faire des aller retour d'objet entre 2 points
 * 
 * -les variables permettre de savoir quand on declanche le deplacement et pdt cb de temps on veux que cette action s'execute
 * 
 * 
 * */

public class BougerObjet : MonoBehaviour {
	public Vector3 depart;
	public Vector3 arrive;
	Vector3 trajectoire;
	public bool sens=true;
	public float Tempsdeplacement;
	public bool move = false;
	float tempPasse;
	// fais disparaitre l'element si il est sur arrive
	public bool disapear;
	public float delai;
	public bool LookAtObject;
	public bool BlockRaycast;


	void Start(){
		
		calculerTrajectoire ();

		//sens = true;
		if (Tempsdeplacement == 0) {
			Tempsdeplacement = 1;
		}
	}

	public void changePosition(){
		calculerTrajectoire ();
		move = true;
	}

	void Update(){
		if (move) {
			
			
			tempPasse += Time.deltaTime;
			if (LookAtObject) {
				transform.forward = -trajectoire;
				//transform.RotateAround (Vector3.forward, 160);
			}
			if (tempPasse > delai) {

                Vector3 vec = Time.deltaTime / (Tempsdeplacement - delai) * trajectoire;

				if (sens) {
					transform.localPosition += vec;
				} else {
					transform.localPosition -= vec;
				}
			}
			if (tempPasse > Tempsdeplacement) {
				if (LookAtObject) {
					transform.up = -trajectoire;
				}
				if (sens) {
					transform.localPosition = arrive;
					sens = !sens;
					move = false;
					tempPasse = 0;
					if (disapear) {
						gameObject.SetActive (false);
					}

				} else {
					transform.localPosition = depart;
					if (LookAtObject) {
						transform.localEulerAngles = new Vector3 (0, 180, 0);
					}
					sens = !sens;
					move = false;
					tempPasse = 0;
				}

			}

		}
	}

	public void objectifAtteint(){
        sens = !sens;
        move = false;
        tempPasse = 0;

		if (!sens && disapear) {
			gameObject.SetActive (false);
		}
	}
	public void calculerTrajectoire(){
		trajectoire = arrive - depart;
	}
}
