﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnMove : MonoBehaviour {


	
	// Update is called once per frame
	void Update () {
		if (GetComponent<BougerObjet> ().move) {
			Debug.Log ("ici");
			if (!GetComponent<AudioSource> ().isPlaying) {
				GetComponent<AudioSource> ().Play ();
			}
		} else {
			GetComponent<AudioSource> ().Stop ();
		}
	}
}
