﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GenerateurMot : MonoBehaviour {
	public static int numLangue;
	public static List<List<string>> liste_mot;
	public static bool init;
	// Use this for initialization
	void Start () {
		init = false;
		LoadCsv ();
		numLangue = 1;
	}
	
	// Update is called once per frame
	void Update () {
		switch(ParametreGeneral.langue){
		case "fr":
			numLangue = 1;
			break;
		case "en":
			numLangue = 2;
			break;
		default:
			numLangue=1;
			break;
		}

	}

	public static int GetInd(string id){
		int i = 0;

		foreach(List<string> ls in liste_mot){
			if (ls [0] == id) {
				return i;
			}
			i++;
		}
		return 0;
	}

	public static string GetMot(int id){

		return liste_mot [id] [numLangue];
	}


	 void LoadCsv(){
		liste_mot = new List<List<string>> ();
		//StreamReader rd = null;
		//#if UNITY_EDITOR

		TextAsset level = Resources.Load<TextAsset>("fichierLangues");
	
		if(level != null)
		{
			using(StreamReader rd = new StreamReader(new MemoryStream(level.bytes)))
			{
				
				string line = rd.ReadLine ();

				while (line != null) {
					
					List<string> tmp = new List<string> ();
					string[] mot = line.Split (';');
					for (int i = 0; i < mot.Length; i++) {
						mot[i] = mot[i].Replace ("’", "'");
						mot[i] = mot[i].Replace ("–", "-");
						mot[i] = mot[i].Replace ("&amp;", "&");
						mot[i] = mot[i].Replace ("‘", "'");
						mot[i] = mot[i].Replace ("“", "\"");
						mot[i] = mot[i].Replace ("”", "\"");
						mot[i] = mot[i].Replace ("<br>", "\n");
						tmp.Add (mot [i]);
					}
					liste_mot.Add (tmp);
					line = rd.ReadLine ();

				}
				rd.Close ();
			}
		}



		/*
		WWW w=new WWW("file:///"+Application.dataPath +"/StreamingAssets/fichierLangues.csv");
		Debug.Log("ici");
		while(!w.isDone){
			Debug.Log(w.size);
			yield return new WaitForSeconds(0.1f);
		}
		Debug.Log(w.size);
		string t=w.text;
		Debug.Log(t);
		string[] splitRetourLigne=t.Split('\n');
		foreach(string line in splitRetourLigne){
			List<string> tmp = new List<string> ();
			string[] mot = line.Split (';');
			for (int i = 0; i < mot.Length; i++) {
				
				mot[i] = mot[i].Replace ("’", "'");
				mot[i] = mot[i].Replace ("–", "-");
				mot[i] = mot[i].Replace ("&amp;", "&");
				mot[i] = mot[i].Replace ("‘", "'");
				mot[i] = mot[i].Replace ("“", "\"");
				mot[i] = mot[i].Replace ("”", "\"");
				mot[i] = mot[i].Replace ("<br>", "\n");
				tmp.Add (mot [i]);
			}
			liste_mot.Add (tmp);
		}
		*/

	/*	#endif
		#if UNITY_ANDROID

		TextAsset level = Resources.Load<TextAsset>("fichierLangues");
		Debug.Log(" la");
		if(level != null)
		{
			using(StreamReader rd = new StreamReader(new MemoryStream(level.bytes)))
			{
				Debug.Log("ici");
				string line = rd.ReadLine ();

				while (line != null) {
					List<string> tmp = new List<string> ();
					string[] mot = line.Split (';');
					for (int i = 0; i < mot.Length; i++) {
						mot[i] = mot[i].Replace ("’", "'");
						mot[i] = mot[i].Replace ("–", "-");
						mot[i] = mot[i].Replace ("&amp;", "&");
						mot[i] = mot[i].Replace ("‘", "'");
						mot[i] = mot[i].Replace ("“", "\"");
						mot[i] = mot[i].Replace ("”", "\"");
						mot[i] = mot[i].Replace ("<br>", "\n");
						tmp.Add (mot [i]);
					}
					liste_mot.Add (tmp);
					line = rd.ReadLine ();

				}
				rd.Close ();
			}
		}
		/*WWW w=new WWW("jar:file://" + Application.dataPath + "!/assets/");
		string t=w.text;
		string[] splitRetourLigne=t.Split("\n");
		foreach(string line  in splitRetourLigne){
			List<string> tmp = new List<string> ();
			string[] mot = line.Split (';');
			for (int i = 0; i < mot.Length; i++) {
				mot[i] = mot[i].Replace ("’", "'");
				mot[i] = mot[i].Replace ("–", "-");
				mot[i] = mot[i].Replace ("&amp;", "&");
				mot[i] = mot[i].Replace ("‘", "'");
				mot[i] = mot[i].Replace ("“", "\"");
				mot[i] = mot[i].Replace ("”", "\"");
				mot[i] = mot[i].Replace ("<br>", "\n");
				tmp.Add (mot [i]);
			}
			liste_mot.Add (tmp);
		}

		#endif
		*/


		init = true;
	}
}
