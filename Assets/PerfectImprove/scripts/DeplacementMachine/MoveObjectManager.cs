﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectManager : MonoBehaviour {
	public GameObject movingObject;
	public GameObject CameraAssocie;
	private Vector3 pointDeDepart;
	private Vector3 positionDepartObjet;
	private Vector3 positionReset;
	private bool moving;
	public bool canBePlace=true;
	private float HauteurObjet;
	public ball_raycasting brAssocie;
	public bool rotation;

	//Variable pour savoir Click ou double click
	float tempsEntreClick;
	public float tempsMaxEntreClick;
	bool firstClick;

	//variable pour l'update position

	Vector3 mouseFirstPosition;
	public float puissanceRotation=10;



	private static MoveObjectManager _instance = null;

	public static MoveObjectManager instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject obj = new GameObject("MoveObjectManager");

				_instance = obj.AddComponent<MoveObjectManager>();

				DontDestroyOnLoad(obj);
			}

			return _instance;
		}
	}
	// Use this for initialization
	void Start () {
		CameraAssocie = GameObject.Find ("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		if (moving) {
			UpdatePositionObject ();
			if (rotation) {
				if (Input.GetMouseButtonDown(0)) {
					InitMousePosition ();
				}
				if (Input.GetMouseButton (0)) {
					UpdateRotationMouse ();
				}
			}
			if (firstClick) {
				if (Input.GetMouseButtonDown (0)) {
					//trop de temps entre les 2 click
					if (tempsEntreClick > tempsMaxEntreClick) {
						tempsEntreClick = 0;
					
					} 
					//on effectue un tir et on reinitialise
					else {
						if ( canBePlace) {
							positionnerObjet ();
						}
						tempsEntreClick = 0;
						firstClick = false;
					}
				} else {
					tempsEntreClick += Time.deltaTime;
					if (tempsEntreClick > tempsMaxEntreClick) {
						rotation = !rotation;
						#if UNITY_EDITOR
						mouseMove.stopEditorMovement = rotation;
						#endif
						firstClick = false;
					}
				}
			} else {
				if (Input.GetMouseButtonDown (0)) {
					firstClick = true;
				} 
			}


		}
	}

	public void init(GameObject objetSelectionne){
		if (brAssocie!=null) {
			brAssocie.raycastOn = false;
		}

		movingObject = objetSelectionne;
		positionReset=objetSelectionne.transform.position;
		pointDeDepart = calculPointWithForward (CameraAssocie.transform.forward, CameraAssocie.transform.position);
		HauteurObjet = movingObject.transform.position.y;
		positionDepartObjet = new Vector3(pointDeDepart.x,movingObject.transform.position.y,pointDeDepart.z);
		movingObject.transform.position = positionDepartObjet;
		//movingObject.GetComponent<BoxCollider> ().enabled = false;
		movingObject.AddComponent<checkCollision> ().momAssocie=this;
		movingObject.GetComponent<checkCollision>().initColor(movingObject.GetComponent<MeshRenderer>().materials);
		movingObject.AddComponent<Rigidbody> ().isKinematic = false;
		movingObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;

		moving=true;
	}

	public void reset(){
		if (brAssocie != null) {
			brAssocie.raycastOn = true;
		}
		Destroy (movingObject.GetComponent<checkCollision> ());
		Destroy (movingObject.GetComponent<Rigidbody> ());
		movingObject = null;
		pointDeDepart = Vector3.zero;
		positionDepartObjet = Vector3.zero;
		moving=false;
	}



	public void cancel(){
		if (brAssocie != null) {
			brAssocie.raycastOn = true;
		}
		movingObject.GetComponent<checkCollision>().resetColor();
	//	movingObject.GetComponent<BoxCollider> ().enabled = true;
		Destroy (movingObject.GetComponent<checkCollision> ());
		Destroy (movingObject.GetComponent<Rigidbody> ());
		movingObject.transform.position = positionDepartObjet;
		movingObject = null;
		pointDeDepart = Vector3.zero;
		positionDepartObjet = Vector3.zero;

		moving = false;
	}

	private void positionnerObjet(){
		if (brAssocie != null) {
			brAssocie.raycastOn = true;
		}
		movingObject.GetComponent<checkCollision>().resetColor();
		//movingObject.GetComponent<BoxCollider> ().enabled = true;
		Destroy (movingObject.GetComponent<checkCollision> ());
		Destroy (movingObject.GetComponent<Rigidbody> ());
		movingObject = null;
		pointDeDepart = Vector3.zero;
		positionDepartObjet = Vector3.zero;

		moving = false;
	}

	private Vector3 calculPointWithForward(Vector3 forwardCamera,Vector3 cameraPosition){
		float coeff = 0;
		if(forwardCamera.y<0){
			coeff=(cameraPosition.y-HauteurObjet)/forwardCamera.y;

		}
		else if(moving){
			return pointDeDepart;
		}
		return cameraPosition - (forwardCamera * coeff+new Vector3(0,HauteurObjet,0));
	}

	private void UpdatePositionObject(){
		Vector3 nouvellePosition=calculPointWithForward (CameraAssocie.transform.forward, CameraAssocie.transform.position);
		Vector3 diffPos = pointDeDepart - nouvellePosition;
		movingObject.transform.position = positionDepartObjet - diffPos;

	}

	private void InitMousePosition (){
		mouseFirstPosition = Input.mousePosition;
	}

	private void UpdateRotationMouse(){
		Transform tObjet = movingObject.transform;
		Vector2 diffPos=Input.mousePosition-mouseFirstPosition;
		tObjet.RotateAround (tObjet.position, tObjet.up, diffPos.x*puissanceRotation);
	}
}
