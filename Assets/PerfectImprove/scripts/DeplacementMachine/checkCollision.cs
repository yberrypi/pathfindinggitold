﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkCollision : MonoBehaviour {
	public MoveObjectManager momAssocie;
	private int collisionCount=0;
	Color[] couleurMaterialInitial;
	Color[] closestGreenMaterial;
	Color[] closestRedMaterial;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (collisionCount);
	}

	void OnCollisionEnter(Collision collision)
	{
		collisionCount++;
		momAssocie.canBePlace = false;
		if (collisionCount == 1) {
			TurnRed ();
		}

	}

	void OnCollisionExit(Collision collision)
	{
		collisionCount--;
		if (collisionCount == 0) {
			momAssocie.canBePlace = true;
			TurnGreen ();
		}
	}

	public void initColor(Material[] lmat){
		couleurMaterialInitial = new Color[lmat.Length];
		closestGreenMaterial=new Color[lmat.Length];
		closestRedMaterial=new Color[lmat.Length];
		int i = 0;
		foreach(Material mat in lmat ){
			Color c = mat.color;
			couleurMaterialInitial [i] = c;
			closestGreenMaterial[i] = closestGreen (c);
			closestRedMaterial[i] = closestRed (c);
			i++;
		}
		TurnGreen ();
	}

	public void resetColor(){
		int i = 0;
		foreach(Material mat in gameObject.GetComponent<MeshRenderer>().materials){
			mat.color = couleurMaterialInitial [i];
			i++;
		}
	}

	private Color closestGreen(Color c){
		float grisAssocie = c.grayscale;
		return new Color (0, grisAssocie, 0, 0.5f);
	}

	private Color closestRed(Color c){
		float grisAssocie = c.grayscale;
		return new Color (grisAssocie, 0, 0, 0.5f);
	}

	public void TurnGreen(){
		Material[] lMaterial= gameObject.GetComponent<MeshRenderer> ().materials;
		int i = 0;
		foreach(Material mat in lMaterial){
			mat.color = closestGreenMaterial [i];
			i++;
		}
	}

	public void TurnRed(){
		Material[] lMaterial= gameObject.GetComponent<MeshRenderer> ().materials;
		int i = 0;
		foreach(Material mat in lMaterial){
			mat.color = closestRedMaterial [i];
			i++;
		}
	}
}
