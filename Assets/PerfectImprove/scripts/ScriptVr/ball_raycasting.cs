﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.VR;


//classe permetant de reconnaitre les elements regardés
/*denomination actuellement utilisé:
 * 
 * 
 * -drap (permet de changer de langues)
 * -bttn (acces a l'invoke des boutons)
 * 
 * 
 * */

public class ball_raycasting : MonoBehaviour
{
	Camera mycam;
	GameObject lastfocussed = null;
	float		timecount = 0.0f;
	public Image reticle;
	bool alreadyawake;
	public float tempsSelection;
	public LayerMask lm;
	public MoveObjectManager momAssocie;
	public bool raycastOn=true;


	void Awake ()
	{
		reticle.gameObject.SetActive (false);
		VRSettings.renderScale = 1;
		alreadyawake = false;
		mycam = GetComponent<Camera>();
	
	}

	
	void Update ()
	{
		if (momAssocie == null) {
			momAssocie = MoveObjectManager.instance;
		}
		if (NetworkManager.instance.currentLevel > 0) {
			mycam.clearFlags = CameraClearFlags.Skybox;
		}
		if (!NetworkManager.instance.Spectator) {

			Ray ray;
			GameObject previous;

			ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);


			previous = lastfocussed;
			lastfocussed = null;
		
	
			RaycastHit hit;
			//on regarde ce qu'il ya devant
			if (Physics.Raycast (ray, out hit, 1.0e8f, lm) && raycastOn) {
				lastfocussed = hit.collider.gameObject;


			}

			

			if (lastfocussed == null || lastfocussed.name == "FondBloquant" || lastfocussed != previous ) {
			
				timecount = 0.0f;
				alreadyawake = false;
				reticle.gameObject.SetActive (false);
			} 
			else {
			

				timecount += Time.deltaTime;
				if (timecount > tempsSelection) {
					if (!alreadyawake) {

						//on recupere une denomination devant notre collider

						actionOnGame (lastfocussed.name, true);

							
						//lastfocussed.GetComponent<Button>().onClick.Invoke();
						alreadyawake = true;
					}
					reticle.gameObject.SetActive (false);
				} else {
				
					reticle.gameObject.SetActive (true);
					float res = timecount / tempsSelection;
					reticle.fillAmount = res;
				}
					
			}
	

		}
	}

	public void setLastfocused(GameObject g){
		lastfocussed = g;
	}

	public void actionOnGame(string nom,bool estJoueur){
		string denomination = nom.Substring (0, 4);
		if (!estJoueur) {
			lastfocussed =GameObject.Find(nom);
		}
		if (estJoueur && !NetworkManager.instance.Spectator) {
			if (transform.parent.GetComponent<Joueur> ().connected) {
                NetworkManager.instance.sendClick (nom);
			}
		}
		//on switch les denominations pour faire l'actrion associé au bon collider
		switch (denomination) {

		    case("drap"):
			    Debug.Log ("drapeau selectionner");
			    lastfocussed.GetComponent<Button>().onClick.Invoke();
			    //lastfocussed.GetComponent<clickToMenuVR> ().OnClick ();
			    break;
		    case("bttn"):
			    Debug.Log ("bouton selectionner");
			    lastfocussed.GetComponent<Button>().onClick.Invoke();
			    //lastfocussed.GetComponent<clickToMenuVR> ().OnClick ();
			    break;
		    case("objt"):
			    Debug.Log ("objet selectionner");
			    //lastfocussed.transform.parent.GetComponent<MakeObjectAppear> ().OpenBoardAssocie ();
			    break;
		    case("lncm"):
			    Debug.Log ("lancement selectionner");

			    lastfocussed.transform.parent.GetComponent<TestExperience> ().OpenExp (estJoueur);
		
			    break;
		    case("pntr"):
			    Debug.Log ("pointer selectionner");
			    lastfocussed.GetComponent<Button>().onClick.Invoke();
			    //lastfocussed.transform.parent.GetComponent<MakeObjectAppear> ().OpenBoardAssocie ();
			    break;
		    case("inpt"):
			    Debug.Log ("input selectionner");
                NetworkManager.instance.ShowClavier (lastfocussed.GetComponent<InputField>());
			    //lastfocussed.transform.parent.GetComponent<MakeObjectAppear> ().OpenBoardAssocie ();
			    break;
		    case("lttr"):
			    Debug.Log ("lettre selectionner");
			    lastfocussed.GetComponent<Button> ().onClick.Invoke ();
			    lastfocussed = null;
			    //lastfocussed.transform.parent.GetComponent<MakeObjectAppear> ().OpenBoardAssocie ();
			    break;
		    case("tggl"):
			    Debug.Log ("toggle selectionner");
                Toggle toggle = lastfocussed.GetComponent<Toggle>();
			    toggle.isOn = !toggle.isOn;
			    break;
		    case("chgs"):
			    Debug.Log ("monde selectionner");
			    if (estJoueur) {
				    lastfocussed.GetComponent<Button> ().onClick.Invoke ();
			    }
			    break;
			case("move"):
				Debug.Log ("moving object selectionner");
				if (estJoueur) {
					momAssocie.brAssocie = this;
					momAssocie.init (lastfocussed);
				}
				break;
		    case("objE"):
			    //Debug.Log ("objet validé");
			    //lastfocussed.GetComponent<ObjetInteractible> ().ObjectValidate();
			    Debug.Log ("main Lance");
				/*
                Transform currentMovingObject = GameObject.Find("currentMovingObject").transform;

			    switch(SceneManagerExperience.currentProduit.name){
			        case "A":
				        if (SceneManagerExperience.CurrentObject.name == "objE_backStock" && lastfocussed.name == "objE_backStock") {
                            currentMovingObject.SetParent (GameObject.Find ("ManetteDroite").transform);
                            currentMovingObject.localPosition = new Vector3 (21, -416, 293);
				        }
				        break;
			        case "B":
				        //Debug.Log (SceneManagerExperience.numeroCurrentObject);
				        if (SceneManagerExperience.CurrentObject.name == "objE_PieceAttraper" && lastfocussed.name == "objE_PieceAttraper" && SceneManagerExperience.numeroCurrentObject == 1) {
                            currentMovingObject.SetParent (GameObject.Find ("ManetteGauche").transform);
                            currentMovingObject.localPosition = new Vector3 (21, -416, 293);
                            currentMovingObject.localRotation = Quaternion.identity;
				        }
				        if (SceneManagerExperience.CurrentObject.name == "objE_backStock" && lastfocussed.name == "objE_backStock") {

                            currentMovingObject.SetParent (GameObject.Find ("ManetteDroite").transform);
                            currentMovingObject.localPosition = new Vector3 (21, -416, 293);
				        }
				        break;
			        }
			        */
			        lastfocussed.GetComponent<ObjetInteractible> ().LancerMain();
			    break;
		    default:
			    Debug.Log ("cas de denomination non renseigné");
			    break;

		}
	}
}
