﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerPoint : MonoBehaviour {
	List<Transform> listFils;
	int index=0;
	float temps;
	bool stop;

	// Use this for initialization
	void Start () {
		listFils = new List<Transform> ();
		foreach(Transform child in transform){
	
			listFils.Add(child);
		}
		for (int i = 1; i < listFils.Count; i++) {
			listFils [i].gameObject.SetActive (false);
		}
		index = 0;
		temps = 0;
		stop = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (stop) {
			temps += Time.deltaTime;
		}
		if (temps > 5) {
			temps = 0;
			listFils [index].gameObject.SetActive (false);
			index++;
			if (index == listFils.Count) {
				index = 0;
			}
			listFils [index].gameObject.SetActive (true);

		}
		
	
	}

	public void ClickStop(){
		stop = !stop;

	}

	public void restart(){
		listFils [index].gameObject.SetActive (false);
		temps = 0;
		index = 0;
		listFils [index].gameObject.SetActive (true);
	}

	public void next(){
		listFils [index].gameObject.SetActive (false);
		index++;
		if (index == listFils.Count) {
			index = 0;
		}
		temps = 0;
		listFils [index].gameObject.SetActive (true);
	}

	public void previous(){
		listFils [index].gameObject.SetActive (false);
		index--;
		if (index == -1) {
			index = listFils.Count-1;
		}
		temps = 0;
		listFils [index].gameObject.SetActive (true);
	}
}
