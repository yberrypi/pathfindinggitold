﻿using UnityEngine;
using System.Collections;

public class BoutonPlayPause : MonoBehaviour {
	public 	GameObject play;
	public GameObject pause;
	bool start;

	public void OnClick(){
		start = !start;
		play.SetActive (start);
		pause.SetActive (!start);
	}
}
