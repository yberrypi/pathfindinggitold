﻿using UnityEngine;
using System.Collections;

public class hidePointer : MonoBehaviour {
	public GameObject pointer;
	public GameObject fullscreen;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (fullscreen.activeSelf) {
			pointer.SetActive (false);
		} else {
			pointer.SetActive (true);
		}
	}
}
