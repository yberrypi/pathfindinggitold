﻿using UnityEngine;
using System.Collections;

public class followCamera : MonoBehaviour {
	public GameObject mycam;
	public bool droit;
	// Use this for initialization
	void Start () {
		mycam = GameObject.Find ("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 newForward = mycam.transform.position - transform.position;
		if (droit) {
			newForward.y = 0;
		}
		transform.forward = -newForward;

	}
}
