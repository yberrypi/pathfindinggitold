﻿using UnityEngine;
using System.Collections;

public class StopVoiture : MonoBehaviour {
	public Animation anim;
	bool reprise=false;
	float tempsreprise=0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (reprise) {
			if(tempsreprise<1){
				tempsreprise += Time.deltaTime / 2;
			}
			else if (tempsreprise < 2) {
				tempsreprise += Time.deltaTime / 2;

					anim ["anime_01"].speed = tempsreprise-1;

			} else {
				tempsreprise = 0;
				reprise = false;
			}
		}


	}

	void OnCollisionEnter(Collision collision){
		
		if (collision.collider.name == "ObjetDeplacement" ) {
			stopVoiture ();
            NetworkManager.instance.stop (getTimeAnim());
		}

		//collision.collider.name;

	}

	void OnCollisionExit(Collision collision){
		
		if (collision.collider.name == "ObjetDeplacement" ) {
			repriseVoiture ();
            NetworkManager.instance.reprise ();
		}


	}

	public float getTimeAnim(){
		return anim ["anime_01"].time;
	}

	public void setTimeAnim(float timeIn){
		
		anim ["anime_01"].time = timeIn;
	}

	public void stopVoiture(){
		anim ["anime_01"].speed = 0;
	}

	public void stopVoiture(float temps){
		anim ["anime_01"].speed = 0;
		setTimeAnim (temps);
	}

	public void repriseVoiture(){
		reprise = true;
	}
}
