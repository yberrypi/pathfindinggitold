﻿using UnityEngine;
using System.Collections;

public class moveOnTouch : MonoBehaviour {
	public Transform cam;
	public bool contact;
	public bool immobile;
	float tempsReaction;
	public float tempsAvantMouvement;

    Rigidbody rb;

	// Use this for initialization
	void Start () {
		tempsReaction = 0f;
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        bool touch = Input.GetMouseButton(0);

		if ((touch || x != 0f || y != 0f) && !contact && !immobile) {

            Vector3 vec = Vector3.zero;

            if (y != 0f)
            {
                vec += y * 2f * Time.deltaTime * cam.transform.forward;
            }

            if (x != 0f)
            {
                vec += x * 2f * Time.deltaTime * cam.transform.right;
            }

            if (touch)
            {
                if (tempsReaction > tempsAvantMouvement)
                {
                    vec += 2f * Time.deltaTime * cam.transform.forward;
                }
                else
                {
                    tempsReaction += Time.deltaTime;
                }
            }
            else
            {
                tempsReaction = 0f;
            }
            vec.y = 0f;
            transform.position += vec;

		} else {
			tempsReaction = 0f;
		}
		contact = false;
	}

	void OnCollisionEnter(Collision collision){

		contact = true;
	}

}
