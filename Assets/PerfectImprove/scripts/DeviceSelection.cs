﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DeviceSelection : MonoBehaviour {

    clientsetting client = null;
    Dropdown      cbo = null;
    private bool  InitOK = false;
    GameObject topbar_nameline = null;


    void Init()
    {
        InitOK = true;
    }

    // Use this for initialization
	IEnumerator Start () {
        poi_data[] alldevices = poi_data.GetAllDevice();
        cbo = gameObject.GetComponent<Dropdown>();
        client = GameObject.FindObjectOfType<clientsetting>();
        GameObject CanvasScreen = GameObject.Find("CanvasScreen");

        if( CanvasScreen != null )
            topbar_nameline = CanvasScreen.FindInChildren( "nameline" );
            

        if (cbo != null)
        {
            cbo.options.Clear();
            cbo.options.Add(new Dropdown.OptionData("Usine"));
        }

        if (client != null)
        {
            while (!client.PlayerConfigInitOK)
                yield return null;
        }

        if (alldevices != null)
        {
            bool test = false;
            while (!test)
            {
                test = true;
                for (int i = 0; i < alldevices.Length; i++)
                    if (!alldevices[i].TagsConfigInitOK)
                        test = false;
                yield return null;
            }
        }

        if( (cbo != null) && (client != null) && (alldevices != null) )
        {
            if( (client != null) && (cbo != null) )
            {
                for (int i = 0; i < alldevices.Length; i++)
                {
                    cbo.options.Add(new Dropdown.OptionData(alldevices[i].TagsConfig.Name));
                }
            }
        }

        Init();
	}
	
	// Update is called once per frame
	void Update () {

        if( InitOK )
        {
            if( client != null )
            {

                if( topbar_nameline != null )
                {
                    topbar_nameline.GetComponent<Text>().text = client.PlayerConfig.Area;
                }

                if( cbo != null )
                {
                    for (int i = 0; i < cbo.options.Count; i++)
                    {
                        if (cbo.options[i].text == client.PlayerConfig.Area)
                        {
                            cbo.value = i;
                            break;
                        }
                    }
                }
                
            }
        }
	
	}


	public void PressedItem()
	{
        if (InitOK)
        {
            if ((client != null) && (cbo != null))
            {
                int idx = cbo.value;
                if ((idx >= 0) && (idx <= cbo.options.Count))
                {
                    client.PlayerConfig.Area = cbo.options[idx].text;
                    client.PlayerConfig.SaveConfig();
                }
            }
        }
		
	}
}
