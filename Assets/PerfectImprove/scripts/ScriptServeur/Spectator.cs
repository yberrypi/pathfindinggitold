﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spectator : MonoBehaviour {

	public class spec
	{
		public string nom;
		public int id;
	}

	public bool connected;
	public List<spec> listNomJoueur=new List<spec>();
	public int currentViewer = 0;
	public NetworkManager NMAssocie;
	public Text tNomJoueur;
	float temps=0;
	int count=0;
	bool predictMove=false;
	float tempsEntreDeuxDonnee;
	Vector3 diffPos=Vector3.zero;
	Vector3 diffAngle=Vector3.zero;

	bool first=true;

	public void  MoveCamera(string nom,int id,Vector3 pos,Vector3 rot,int hisLevel,int ourLevel){
		

		if (!contain(id)) {

			spec tmp = new spec ();
			tmp.id = id;
			tmp.nom = nom;
			listNomJoueur.Add (tmp);
		} 

		if(id==listNomJoueur[currentViewer].id){
			if (first) {
				transform.localPosition = pos;
				transform.localEulerAngles = rot;
				first = false;
			} else {

				tempsEntreDeuxDonnee = temps;
				temps = 0;
				if (tempsEntreDeuxDonnee == 0) {
					diffPos = Vector3.zero;
					diffAngle = Vector3.zero;
				} else {
					diffPos = (pos - transform.localPosition) / tempsEntreDeuxDonnee;
					diffAngle =new Vector3(Mathf.DeltaAngle(transform.localEulerAngles.x,rot.x ) / tempsEntreDeuxDonnee,Mathf.DeltaAngle(transform.localEulerAngles.y,rot.y ) / tempsEntreDeuxDonnee,Mathf.DeltaAngle(transform.localEulerAngles.z,rot.z ) / tempsEntreDeuxDonnee);
				}
				//positionDepart = pos;

				//predictMove = true;
				transform.localPosition = pos;
				transform.localEulerAngles = rot;

			}


			if (connected && hisLevel != ourLevel) {
				NMAssocie.ChangeLevel (hisLevel);
			}

			if (connected) {
				tNomJoueur.text = listNomJoueur [currentViewer].nom;
			}
		}

	}

	void Update(){
		temps += Time.deltaTime;
		if (predictMove) {
			if (tempsEntreDeuxDonnee == 0) {
				tempsEntreDeuxDonnee = 1;
			}
			if (temps < 2*(tempsEntreDeuxDonnee)) {
				Vector3 vec =diffAngle*Time.deltaTime;
				transform.localEulerAngles += vec;
			
				 vec = diffPos*Time.deltaTime;

				transform.localPosition += vec;

			} else {
				predictMove = false;
			}
		}

		if (connected && Input.GetMouseButtonDown (0)) {
			first = true;
			predictMove = false;
			temps = 0;
			if (currentViewer == (listNomJoueur.Count - 1)) {
				currentViewer = 0;
			} else {
				currentViewer++;
			}
		}
	}

	public bool contain(int id){
		foreach (spec s in listNomJoueur) {
			if (s.id == id) {
				
				return true;
			} 
		}
		return false;
	}
}
