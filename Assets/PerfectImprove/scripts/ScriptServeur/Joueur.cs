﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joueur : MonoBehaviour {
	public bool connected=false;
	public bool idAsked=false;
	public int idAssocie=0;
	public NetworkManager netManag;
	public GameObject Camera;

    private void Awake()
    {
        if (!netManag)
        {
            netManag = NetworkManager.instance;
        }
    }

    // Update is called once per frame
    void Update () {
		
		if (connected && idAssocie == 0 && !idAsked) {
			idAsked = true;
			askId ();
		}
		if (idAssocie > 0 && connected) {
			sendPosRot ();
		}
	}

	void askId(){
		netManag.askId ();
	}

	void sendPosRot(){
		netManag.sendPosRot (idAssocie, transform.localPosition, Camera.transform.localEulerAngles);
	}

	public void ChangeLevelPosition(int level){
		switch (level) {
		case 0:
			transform.localPosition=new Vector3 (5, 1.8f, 0.5f);
			break;
		case 1:
			transform.localPosition=new Vector3 (-18, 1.8f, -1.5f);
			break;
		case 2:
			transform.localPosition=new Vector3 (-4, 1.8f, -4f);
			break;
		default:
			transform.localPosition=new Vector3 (0, 0, 0);
			break;

		}


	}

}
