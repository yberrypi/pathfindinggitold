﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;

public class AutreJoueur : MonoBehaviour {
	public string nomAssocie;
	public int IdAssocie;
	public Animator anim;
	public Vector3 Destination;
	public bool predictMove=false;
	 Vector3 positionArrivee;
	 float AngleArrivee;
	Vector3 positionDepart;
	float AngleDepart;
	public Text nomAffichage;
	 bool visible=true;
	bool front=false;
	bool left=false;
	bool right=false;
	bool idle=false;
	public Rigidbody rigidbodyAssocie;
	public GameObject materialLunette;

	Vector3 diffPos=Vector3.zero;
	Vector3 diffAngle=Vector3.zero;

	float tempsEntreDeuxDonnee;
	float temps;
	bool first=true;
	 float sameAnimation;
	 bool sameAnimationLaunch;



	// Use this for initialization
	void Start () {
		anim.Play ("HumanoidIdle");
		visible = true;
	}
	
	// Update is called once per frame
	void Update () {
		rigidbodyAssocie.velocity = new Vector3(0f,0f,0f); 
		rigidbodyAssocie.angularVelocity = new Vector3(0f,0f,0f);
		temps += Time.deltaTime;
		sameAnimation += Time.deltaTime;
		if (predictMove) {
			
			if (tempsEntreDeuxDonnee == 0) {
				tempsEntreDeuxDonnee = 1;
			}
			if (temps < 2*(tempsEntreDeuxDonnee)) {

				Vector3 vec =diffAngle*Time.deltaTime;
				transform.localEulerAngles += vec;
				vec = diffPos*Time.deltaTime;
				vec.y = 0;
				transform.localPosition += vec;

			} else {
				predictMove = false;
			}
		}
		if (temps > 5) {
			visible = false;

		}
		if (temps > 60) {
			Destroy (gameObject);
		}
		if (visible) {
			GetComponent<BoxCollider> ().enabled = true;
			foreach (SkinnedMeshRenderer sr in transform.GetComponentsInChildren<SkinnedMeshRenderer> ()) {
				sr.enabled = true;
			}
			nomAffichage.gameObject.SetActive (true);
		} else {
			first = true;
			GetComponent<BoxCollider> ().enabled = false;
			foreach (SkinnedMeshRenderer sr in transform.GetComponentsInChildren<SkinnedMeshRenderer> ()) {
				sr.enabled = false;
			}
			nomAffichage.gameObject.SetActive (false);
		}
	}

	public void movePlayer(Vector3 pos, Vector3 rot,int hislevel,int ourlevel,Color c){
		materialLunette.GetComponent<Renderer>().material.color = c;
		if (hislevel == ourlevel) {
			visible = true;
			if (first) {
				Vector3 vec = pos;
				vec.y = 0;
				transform.localPosition = vec;
				vec = rot;
				vec.x = 0;
				vec.z = 0;
				transform.localEulerAngles = vec;
				diffPos = Vector3.zero;
				diffAngle = Vector3.zero;
				first = false;
			} else {
				
				tempsEntreDeuxDonnee = temps;
				temps = 0;
				predictMove = true;

				if (tempsEntreDeuxDonnee == 0) {
					diffPos = Vector3.zero;
					diffAngle = Vector3.zero;
				} else {
					diffPos = (pos - transform.localPosition) / tempsEntreDeuxDonnee;
					diffPos.y = 0;
					diffAngle = new Vector3 (0, Mathf.DeltaAngle (transform.localEulerAngles.y, rot.y) / tempsEntreDeuxDonnee, 0);
					//Debug.Log (diffAngle);
				}
				Vector3 vec = pos;
				vec.y = 0;
				transform.localPosition = vec;
				vec = rot;
				vec.x = 0;
				vec.z = 0;
				transform.localEulerAngles = vec;
			}

			if (Vector3.Distance(diffPos,Vector3.zero)>(0.01) ) {
				if (!front) {
					
					front = true;
					left = false;
					right = false;
					idle = false;
					anim.CrossFade ("HumanoidWalk", 0.05f);
					//anim.Play ("HumanoidWalk");
				}
				/*	if (Mathf.Abs (diffAngle.y * 10) > 10) {

				if (diffAngle.y < 0) {
						//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalkRight")) {
						if (!right) {
							Debug.Log ("right");
							front = false;
							left = false;
							right = true;
							idle = false;
							anim.CrossFade ("HumanoidWalkRight", 0.05f);
							//anim.Play ("HumanoidWalkRight");
						}
					} else {
						//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalkLeft")) {
						if (!left) {
							Debug.Log ("left");
							front = false;
							left = true;
							right = false;
							idle = false;
							anim.CrossFade ("HumanoidWalkLeft", 0.05f);
							//anim.Play ("HumanoidWalkLeft");
						}
					}

			


				} else {
					//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalk")) {
					if (!front) {
						Debug.Log ("front");
						front = true;
						left = false;
						right = false;
						idle = false;
						anim.CrossFade ("HumanoidWalk", 0.05f);
						//anim.Play ("HumanoidWalk");
					}
				}*/
			} else {
				


				//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidIdle")) {
				if (!idle) {

					sameAnimation = 0;
					sameAnimationLaunch = false;
					front = false;
					left = false;
					right = false;
					idle = true;

					//anim.Play ("HumanoidIdle");
				} else {
					if (sameAnimation > 0.1f) {
						if (!sameAnimationLaunch) {
							anim.CrossFade ("HumanoidIdle", 0.05f);
							sameAnimationLaunch = true;
						}
					}
				}
			}
		} else {
			visible = false;
		}
	}

	public void movePlayer(Vector3 pos, Vector3 rot,int hislevel,int ourlevel){
		
		if (hislevel == ourlevel) {

            try
            {
    			visible = true;
    			if (first) {
    				Vector3 vec = pos;
    				vec.y = 0;
    				transform.localPosition = vec;
    				vec = rot;
    				vec.x = 0;
    				vec.z = 0;
    				transform.localEulerAngles = vec;
    				diffPos = Vector3.zero;
    				diffAngle = Vector3.zero;
    				first = false;
    			} else {

    				tempsEntreDeuxDonnee = temps;
    				temps = 0;
    				predictMove = true;

    				if (tempsEntreDeuxDonnee == 0) {
    					diffPos = Vector3.zero;
    					diffAngle = Vector3.zero;
    				} else {
    					diffPos = (pos - transform.localPosition) / tempsEntreDeuxDonnee;
    					diffPos.y = 0;
    					diffAngle = new Vector3 (0, Mathf.DeltaAngle (transform.localEulerAngles.y, rot.y) / tempsEntreDeuxDonnee, 0);
    					//Debug.Log (diffAngle);
    				}
    				Vector3 vec = pos;
    				vec.y = 0;
    				transform.localPosition = vec;
    				vec = rot;
    				vec.x = 0;
    				vec.z = 0;
    				transform.localEulerAngles = vec;
    			}

    			if (Vector3.Distance(diffPos,Vector3.zero)>(0.01) ) {
    				if (!front) {

    					front = true;
    					left = false;
    					right = false;
    					idle = false;
    					anim.CrossFade ("HumanoidWalk", 0.05f);
    					//anim.Play ("HumanoidWalk");
    				}
    				/*	if (Mathf.Abs (diffAngle.y * 10) > 10) {

    				if (diffAngle.y < 0) {
    						//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalkRight")) {
    						if (!right) {
    							Debug.Log ("right");
    							front = false;
    							left = false;
    							right = true;
    							idle = false;
    							anim.CrossFade ("HumanoidWalkRight", 0.05f);
    							//anim.Play ("HumanoidWalkRight");
    						}
    					} else {
    						//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalkLeft")) {
    						if (!left) {
    							Debug.Log ("left");
    							front = false;
    							left = true;
    							right = false;
    							idle = false;
    							anim.CrossFade ("HumanoidWalkLeft", 0.05f);
    							//anim.Play ("HumanoidWalkLeft");
    						}
    					}

    			


    				} else {
    					//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidWalk")) {
    					if (!front) {
    						Debug.Log ("front");
    						front = true;
    						left = false;
    						right = false;
    						idle = false;
    						anim.CrossFade ("HumanoidWalk", 0.05f);
    						//anim.Play ("HumanoidWalk");
    					}
    				}*/
    			} else {



    				//if (!anim.GetCurrentAnimatorStateInfo (0).IsName ("HumanoidIdle")) {
    				if (!idle) {

    					sameAnimation = 0;
    					sameAnimationLaunch = false;
    					front = false;
    					left = false;
    					right = false;
    					idle = true;

    					//anim.Play ("HumanoidIdle");
    				} else {
    					if (sameAnimation > 0.1f) {
    						if (!sameAnimationLaunch) {
    							anim.CrossFade ("HumanoidIdle", 0.05f);
    							sameAnimationLaunch = true;
    						}
    					}
    				}
                        
                }
            }
            catch( Exception ex )
            {
                visible = false;
            }
		} else {
			visible = false;
		}
	}

	public void setNom(string txt){
		nomAssocie = txt;
		nomAffichage.text = nomAssocie;

	}
}
