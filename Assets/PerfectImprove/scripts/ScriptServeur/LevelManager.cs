﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public class objetLevel{
		public string PoiLever;
		public bool experience;
		public string produitExp;
		public int currentEtapeExp;

		public objetLevel(){
			PoiLever=null;
			experience=false;
		}

	}

	List<objetLevel> lobj=new List<objetLevel>();

	public void ChangePoi(string obj,int level){
		Debug.Log ("changePOI");
		while((lobj.Count-1)<level){
			objetLevel ob = new objetLevel ();
			lobj.Add (ob);
		}
		if (obj != null) {
			lobj [level].PoiLever = obj;
		} else {
			lobj [level].PoiLever = null;
		}

	}

	public void changeExp(bool active,int level){
		Debug.Log ("changeExp");
		while((lobj.Count-1)<level){
			objetLevel ob = new objetLevel ();
			lobj.Add (ob);
		}
		lobj [level].experience = active;
	}

	public void changeSettingExp(string nom,int level){
		while((lobj.Count-1)<level){
			objetLevel ob = new objetLevel ();
			lobj.Add (ob);
		}
		lobj [level].produitExp= nom;
	}																													

	public void changeSettingExp(int num,int level){
		while((lobj.Count-1)<level){
			objetLevel ob = new objetLevel ();
			lobj.Add (ob);
		}
		lobj [level].currentEtapeExp = num;
	}		
	public void chargeLevel(int level){
		Debug.Log ("chargeLevel");
		SceneManagerExperience.stock = 0;
		if (level < (lobj.Count)) {
			objetLevel ob = lobj [level];

			if (GameObject.Find (ob.PoiLever)) {
				GameObject.Find (ob.PoiLever).GetComponent<Button>().onClick.Invoke();
			}
			if (ob.experience) {
				GameObject.Find ("lncm_ExperienceLauncher").transform.parent.GetComponent<TestExperience> ().OpenExp (false);
				SceneManagerExperience scExp =	GameObject.Find ("EspaceExperience").GetComponent<SceneManagerExperience> ();

				scExp.finishBoucle = true;
				Destroy (SceneManagerExperience.currentMovingObject);
				scExp.newProduct (ob.produitExp);

				//SceneManagerExperience.numeroCurrentObject = ob.currentEtapeExp;
				Debug.Log (SceneManagerExperience.numeroCurrentObject);

				scExp.validateObjectif (ob.currentEtapeExp);

				/*for (int i = 0; i < ob.currentEtapeExp+1; i++) {
					scExp.moveObjectNextStep (i);
				}*/


			}
		}
	}

	public void UpdateLevel(string nom,int level){
		string denomination = nom.Substring (0, 4);
		switch (denomination) {

		case("drap"):
			Debug.Log ("drapeau selectionner");
		
			break;
		case("bttn"):
			Debug.Log ("bouton selectionner");
			if (nom == "bttn_QuitterSimulation") {
				changeExp (false,level);
			}
			break;
		case("objt"):
			
			break;
		case("lncm"):
			Debug.Log ("lancement selectionner");

			changeExp (true,level);

			break;
		case("pntr"):
			Debug.Log ("pointer selectionner");
			ChangePoi (nom,level);
			break;
		case("inpt"):
			Debug.Log ("input selectionner");

			break;
		case("lttr"):
			Debug.Log ("lettre selectionner");

			//lastfocussed.transform.parent.GetComponent<MakeObjectAppear> ().OpenBoardAssocie ();
			break;
		case("tggl"):
			Debug.Log ("toggle selectionner");
		
			break;
		case("chgs"):
			Debug.Log ("monde selectionner");

			break;
		case("objE"):
			//Debug.Log ("objet validé");
			//lastfocussed.GetComponent<ObjetInteractible> ().ObjectValidate();
		
			break;
		default:
			Debug.Log ("cas de denomination non renseigné");
			break;

		}

	}
}
