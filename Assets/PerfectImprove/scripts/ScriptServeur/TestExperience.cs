﻿using UnityEngine;
using Valve.VR.InteractionSystem;

public class TestExperience : MonoBehaviour {
	public GameObject BoutonLauncher;
	public GameObject EspaceExp;
	public GameObject joueur;
	public Vector3 vecPos;
	public GameObject Machine;
	public GameObject PoiMachine;

    public TeleportPoint teleportStart;
    public TeleportArea[] teleportAreas;
    public TeleportPoint[] teleportPoints;

    private SceneManagerExperience manager;

	// Use this for initialization
	void Start () {
		joueur = GameObject.Find ("ObjetDeplacement");
        manager = EspaceExp.GetComponent<SceneManagerExperience>();
	}

	public void OpenExp(bool estJoueur){
		Destroy (SceneManagerExperience.currentMovingObject);
		BoutonLauncher.SetActive (false);
		EspaceExp.SetActive (true);
        BoxCollider box = Machine.GetComponent<BoxCollider>();
        if (box)
        {
            box.enabled = false;
        }
        else
        {
            GetComponent<MeshCollider>().enabled = false;
        }
		PoiMachine.SetActive (false);
		if (estJoueur) {

            if (!ManageVive.IsVivePresent)
            {
                joueur.GetComponent<moveOnTouch>().immobile = true;
                joueur.transform.localPosition = vecPos;

                Vector3 angles = new Vector3(0f, -Camera.main.transform.localEulerAngles.y, 0f);
                joueur.transform.localEulerAngles = angles;
            }
            else
            {
                Player player = joueur.GetComponent<Player>();
                Vector3 playerFeetOffset = player.trackingOriginTransform.position - player.feetPositionGuess;
                player.trackingOriginTransform.position = teleportStart.transform.position + playerFeetOffset;

                GameObject.Find("ManetteGauche").SetActive(false);
                GameObject.Find("ManetteDroite").SetActive(false);
            }
			
			manager.finishBoucle = true;
			
			manager.estJoueur = true;
			manager.reset ();
		} else {
			manager.finishBoucle = true;
			Debug.Log ("finish bouce passe a true");
			manager.estJoueur = false;
        }

        if (ManageVive.IsVivePresent)
        {
            foreach (TeleportArea ta in teleportAreas)
            {
                ta.SetLocked(true);
            }

            foreach (TeleportPoint tp in teleportPoints)
            {
                tp.SetLocked(false);
            }
        }
        else
        {
            ChangerMonde.Active = false;
        }
    }

	public void CloseExp(){
		manager.estJoueur = false;
	
		if (!NetworkManager.instance.Spectator && !ManageVive.IsVivePresent) {
			joueur.GetComponent<moveOnTouch> ().immobile = false;
		}
		BoutonLauncher.SetActive (true);
		EspaceExp.SetActive (false);
        BoxCollider box = Machine.GetComponent<BoxCollider>();
        if (box)
        {
            box.enabled = true;
        }
        else
        {
            GetComponent<MeshCollider>().enabled = true;
        }
        PoiMachine.SetActive (true);

        if (ManageVive.IsVivePresent)
        {
            foreach (TeleportArea ta in teleportAreas)
            {
                ta.SetLocked(false);
            }

            foreach (TeleportPoint tp in teleportPoints)
            {
                tp.SetLocked(true);
            }
        }
        else
        {
            ChangerMonde.Active = true;
        }
    }
}
