﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArrow : MonoBehaviour {
	Vector3 positionInitial=new Vector3(1000,1000,1000);
	bool sens;
	float temps;
	// Use this for initialization
	void Awake () {
		if (positionInitial.x ==1000) {
			positionInitial = transform.localPosition;
		} else {
			transform.localPosition = positionInitial;
		}
		temps = 0;
		sens = true;
	}
	
	// Update is called once per frame
	void Update () {
		temps += Time.deltaTime;
		if (temps < 0.5f) {
			Vector3 vec = transform.localPosition;
			if (sens) {
				vec.y -= Time.deltaTime * 0.05f;
			} else {
				vec.y += Time.deltaTime * 0.05f;
			}
			transform.localPosition = vec;
		} else {
			temps = 0;
			sens = !sens;
		}
	}
}
