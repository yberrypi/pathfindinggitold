﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agrandirBouton : MonoBehaviour {
	bool sens = true;
	float temps = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		temps += Time.deltaTime;
		GameObject joueur = GameObject.Find ("ObjetDeplacement");
		Vector3 joueurPos = joueur.transform.position;
		Vector3 objet = transform.position;
		if (/*joueurPos.z < objet.z &&*/Vector3.Distance (joueurPos, objet) < 5) {
			if (temps < 0.5f) {
				Vector3 vec = transform.localScale;
				if (sens) {
					vec += Time.deltaTime * 0.1f * Vector3.one;
				} else {
					vec -= Time.deltaTime * 0.1f * Vector3.one;
				}
				transform.localScale = vec;
			} else {
				temps = 0;
				sens = !sens;
			}
		} else {
			temps = 0;
			sens = true;
			transform.localScale = Vector3.one;
		}

	}
}
