﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GestionJoueur : MonoBehaviour {
	int id=2;
	public GameObject Temoin;
	public List<AutreJoueur> lAutreJoueurs;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public int getid(){
		int res = id;
		id++;
		return res;
	}

	public void GetPositionRotation(string nom,int id,Vector3 pos,Vector3 rot,int hisLevel,int ourLevel){
		int num = 0;
		num = idExiste (id);
	
		if (num>-1) {
			movePlayer (num, pos, rot,hisLevel,ourLevel);
		} else {
			if (hisLevel == ourLevel) {
				createJoueur (nom, id, pos, rot);
			}
		}
	}

	public void GetPositionRotation(string nom,int id,Vector3 pos,Vector3 rot,int hisLevel,int ourLevel,Color c){
		int num = 0;
		num = idExiste (id);

		if (num>-1) {
			movePlayer (num, pos, rot,hisLevel,ourLevel,c);
		} else {
			if (hisLevel == ourLevel) {
				createJoueur (nom, id, pos, rot,c);
			}
		}
	}

	public int idExiste(int idDonne){
		int i = 0;
		foreach (AutreJoueur j in lAutreJoueurs) {
			
			if (idDonne == j.IdAssocie) {
				return i;
			}
			i++;
		}
		return -1;
	}



	void createJoueur(string nom,int id,Vector3 pos,Vector3 rot){
		GameObject nouv = Instantiate (Temoin);
		nouv.transform.SetParent (Temoin.transform.parent);
		nouv.transform.localPosition = pos;
		nouv.transform.localEulerAngles = rot;
		nouv.GetComponent<AutreJoueur> ().IdAssocie = id;
		nouv.GetComponent<AutreJoueur> ().setNom (nom);

		lAutreJoueurs.Add (nouv.GetComponent<AutreJoueur> ());
		nouv.name = "joueur: " + nom;
		nouv.SetActive (true);


	}

	void createJoueur(string nom,int id,Vector3 pos,Vector3 rot,Color c){
		GameObject nouv = Instantiate (Temoin);
		nouv.transform.SetParent (Temoin.transform.parent);
		nouv.transform.localPosition = pos;
		nouv.transform.localEulerAngles = rot;
		nouv.GetComponent<AutreJoueur> ().IdAssocie = id;
		nouv.GetComponent<AutreJoueur> ().setNom (nom);
		nouv.GetComponent<AutreJoueur> ().materialLunette.GetComponent<Renderer>().material.color = c;
		lAutreJoueurs.Add (nouv.GetComponent<AutreJoueur> ());
		nouv.name = "joueur: " + nom;
		nouv.SetActive (true);


	}

	void movePlayer(int num,Vector3 pos,Vector3 rot,int hislevel,int ourlevel,Color c){
		lAutreJoueurs [num].movePlayer (pos, rot,hislevel,ourlevel,c);
	}

	void movePlayer(int num,Vector3 pos,Vector3 rot,int hislevel,int ourlevel){
		lAutreJoueurs [num].movePlayer (pos, rot,hislevel,ourlevel);
	}


}
