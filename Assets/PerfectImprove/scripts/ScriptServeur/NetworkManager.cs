﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour {

    private static NetworkManager _instance = null;

    public static NetworkManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject obj = new GameObject("NetworkManager");

                _instance = obj.AddComponent<NetworkManager>();

                DontDestroyOnLoad(obj);
            }

            return _instance;
        }
    }

    private const string typeName = "PerfectIndustry_ServeurBalade";
	public  string gameName = "";
	public HostData[] hostList;

	NetworkView netview;

	//zone canvas
	public GameObject EcranIntro;
	public GameObject EcranJoindre;
	public GameObject EcranCreation;

	public InputField IFnom;
	public InputField IFnomServer;

	public GameObject boutonCacheRejoindre;
	public GameObject boutonCacheCreer;
	public GameObject boutonCacheLancer;
	public GameObject boutonCacheRetour;

	public GameObject temoinServeur;
	public GameObject JoueurAssocie;
	public GestionJoueur gjAssocie;

	public ball_raycasting brAssocie;
	public Spectator SpecAssocie;
	public LevelManager lmAssocie;


	GameObject voiture;
	//public GameObject zonePi;

	public int currentLevel;

	public string nomJoueur;

	bool ecranJoindreAfficher;

	public Clavier clavierAssocie;

	public bool Spectator;


	public Text debugText;

	//ajout pour fps

	public  bool combattant;
	public GameManager gm;
	public List<Color> listColor;
	GameObject ensemblePOI;
	GameObject PartieExperience;
	GameObject Teleportation;

	public void OuvrirJoinServeur(){
		
		nomJoueur = IFnom.text;
		AfficherEcran (2);
		afficherListeServer ();
	}

	public void OuvrirCreerServeur(){
		nomJoueur = IFnom.text;
		AfficherEcran (3);
	}

	public void retourEcranIntro(){
		AfficherEcran (1);
	}
	public void Awake()
	{
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        combattant = false;
		DontDestroyOnLoad (gameObject);
		
	}
	// Use this for initialization
	IEnumerator Start () {
        //90.86.146.242  public
        //192.168.1.104 prive à utilise
        /*	MasterServer.ipAddress = "192.168.1.104";
            MasterServer.port =23466;
            Network.natFacilitatorIP = "192.168.1.104";
            Network.natFacilitatorPort =50005;
            */

        if (!Spectator)
        {
            DontDestroyOnLoad(JoueurAssocie.transform.parent.gameObject);


            if (FindObjectsOfType(GetType()).Length > 1)
            {
                Destroy(gameObject);
                Destroy(JoueurAssocie.transform.parent.gameObject);
            }
        }

        while (!downloadmanager.AllDownloaded) {
			yield return null;
		}

		currentLevel = 0;

		if (!Spectator) {
			AfficherEcran (1);
		} else {
			afficherListeServer ();
		}
		netview = GetComponent<NetworkView>();
		RefreshHostList ();




	}

	// Update is called once per frame
	void Update () {
		
		//TestConnection ();
	}



	public void StartServer()
	{
		gameName = IFnomServer.text;
		//Debug.Log (Network.HavePublicAddress ());
		Network.InitializeSecurity();
		//Network.InitializeServer(10, 25000, !Network.HavePublicAddress());
        Network.InitializeServer(10, 43653, !Network.HavePublicAddress());
		MasterServer.RegisterHost(typeName, gameName);


	}

	void OnServerInitialized()
	{
		Debug.Log("Server Initializied");
		AfficherEcran (0);
		JoueurAssocie.GetComponent<Joueur> ().connected = true;
		JoueurAssocie.GetComponent<Joueur> ().idAssocie = 1;
        if (!ManageVive.IsVivePresent)
        {
            JoueurAssocie.GetComponent<moveOnTouch>().immobile = false;
        }
		ChangeLevel(1);
		//zonePi.SetActive (true);

	}

	public void refresh(){
		afficherListeServer ();
	}

	void afficherListeServer(){
		
		RefreshHostList ();
		foreach (Transform t in temoinServeur.transform.parent) {
			if (t.gameObject != temoinServeur) {
				Destroy (t.gameObject);
			}
		}
		HostData[] hostData = MasterServer.PollHostList();
		int i = 0;
		while (i < hostData.Length) {
			Debug.Log (hostData[i].gameName);
			GameObject nouv = Instantiate (temoinServeur);
			nouv.transform.SetParent (temoinServeur.transform.parent);
			nouv.transform.localScale = temoinServeur.transform.localScale;
			string name = hostData [i].gameName;
			nouv.name = "bttn_"+name;
			nouv.transform.GetChild (0).GetComponent<Text>().text=name;
			nouv.transform.GetChild (1).GetComponent<Text> ().text = hostData [i].connectedPlayers.ToString ();
			nouv.transform.GetChild (2).GetComponent<Text> ().text ="/"+ hostData [i].playerLimit.ToString();
			nouv.transform.localRotation = temoinServeur.transform.localRotation;
			if (i % 2==0) {
				nouv.GetComponent<Image> ().color = Color.white;
			}
			else{
				nouv.GetComponent<Image> ().color = new Color (0.45f, 0.9f, 0.9f);
			}
			if (!Spectator) {
				nouv.transform.localPosition = new Vector3 (0, 76.6f - 20 * i, 0);
			} else {
				Transform par = nouv.transform.parent;
				float hauteurParent = par.GetComponent<RectTransform> ().rect.height / 2;
				float hauteurNouv = nouv.GetComponent<RectTransform> ().rect.height / 2;
				float hauteur = hauteurParent - hauteurNouv;
				nouv.transform.localPosition = new Vector3 (0, hauteur -2*hauteurNouv* i, 0);
				nouv.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, nouv.GetComponent<RectTransform> ().sizeDelta.y);
			}
		

			nouv.GetComponent<ServeurDonne> ().hostData = hostData [i];
			//nouv.GetComponent<ServeurDonne> ().nm = this;
			nouv.SetActive (true);
		
			i++;
		}
	}

	public void RefreshHostList()
	{
		MasterServer.RequestHostList(typeName);
	}

	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}

	public void JoinServer(HostData hostData)
	{
		Network.Connect(hostData);
	}

	void OnConnectedToServer()
	{
		Debug.Log("Server Joined");
		if (!Spectator) {
			AfficherEcran (0);
			JoueurAssocie.GetComponent<Joueur> ().connected = true;
			JoueurAssocie.GetComponent<moveOnTouch> ().immobile = false;
		} else {
			SpecAssocie.connected = false;
		}
		ChangeLevel(1);

		//zonePi.SetActive (true);
	}

    public void QuitServer()
    {
        if (combattant)
        {
            quitterPartie();
        }
        Network.Disconnect();
        if (Network.connections.Length == 0)
        {
            MasterServer.UnregisterHost();
        }
    }

    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        Debug.Log("Server Quit");

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void AfficherEcran(int numEcran){
		switch (numEcran) {
		case 0:
			EcranIntro.SetActive (false);
			EcranJoindre.SetActive (false);
			EcranCreation.SetActive (false);
			break;
		case 1:
			EcranIntro.SetActive (true);
			EcranJoindre.SetActive (false);
			EcranCreation.SetActive (false);
			break;
		case 2:
			EcranIntro.SetActive (false);
			EcranJoindre.SetActive (true);
			EcranCreation.SetActive (false);
			break;
		case 3:

			EcranIntro.SetActive (false);
			EcranJoindre.SetActive (false);
			EcranCreation.SetActive (true);
			break;
		}
	}

	public void ShowClavier(InputField newIf){
		
		clavierAssocie.gameObject.SetActive (true);
		newIf.text = "";
		clavierAssocie.ifAsssocie = newIf;

		boutonCacheCreer.SetActive (false);
		boutonCacheLancer.SetActive (false);
		boutonCacheRejoindre.SetActive (false);
		boutonCacheRetour.SetActive (false);


	}

	public void HideClavier(){
		clavierAssocie.gameObject.SetActive (false);
	
		boutonCacheCreer.SetActive (true);
		boutonCacheLancer.SetActive (true);
		boutonCacheRejoindre.SetActive (true);
		boutonCacheRetour.SetActive (true);
	}


	public void askId(){
		if (!Spectator) {
			netview.RPC ("AskId", RPCMode.Server);
		}
	}

	[RPC]
	void AskId (NetworkMessageInfo info)
	{
		if (!Spectator) {
			int id = gjAssocie.getid ();

			netview.RPC ("GetId", info.sender, id);
		}

	}


	[RPC]
	void GetId (int id)
	{
		if (!Spectator) {
			JoueurAssocie.GetComponent<Joueur> ().idAssocie = id;
		}
	}





	public void sendPosRot(int id,Vector3 pos,Vector3 rot){
		if (!Spectator) {
			Color c = JoueurAssocie.GetComponent<fighter> ().couleurAssocie;
            Vector3 v = (Vector4)c;
			if (!JoueurAssocie.GetComponent<fighter> ().vivant) {
				//netview.RPC ("SendPosRot", RPCMode.Others, nomJoueur, id, pos, rot, currentLevel,false,v);
				netview.RPC ("SendPosRot", RPCMode.Others, nomJoueur, id, pos, rot, currentLevel,false);
			} else {
				//netview.RPC ("SendPosRot", RPCMode.Others, nomJoueur, id, pos, rot, currentLevel,combattant,v);
				netview.RPC ("SendPosRot", RPCMode.Others, nomJoueur, id, pos, rot, currentLevel,combattant);
			}

		}
	}


    
	[RPC]
	void SendPosRot (string nom,int num,Vector3 pos,Vector3 rot,int level,bool comb)
	{

		if (combattant == comb) {
			if (!Spectator) {
				
				gjAssocie.GetPositionRotation (nom, num, pos, rot, level, currentLevel);
			} else {
				SpecAssocie.MoveCamera (nom, num, pos, rot, level, currentLevel);
			}
		}

	}
    

    /*
	[RPC]
	void SendPosRot (string nom,int num,Vector3 pos,Vector3 rot,int level,bool comb,Vector3 col)
	{
		Color c = Color.white;
		Debug.Log (c);
		if (combattant) {
			c = (Vector4)col;
		} 
		if (combattant == comb) {
			if (!Spectator) {

				gjAssocie.GetPositionRotation (nom, num, pos, rot, level, currentLevel,c);
			} else {
				SpecAssocie.MoveCamera (nom, num, pos, rot, level, currentLevel);
			}
		}

	}
    */


	public void sendClick(string nom){
		if (!Spectator) {
			netview.RPC ("SendClick", RPCMode.Others, nom, currentLevel);
		}
	}

	[RPC]
	void SendClick (string nom,int level,NetworkMessageInfo info)
	{
		if (level == currentLevel) {
			brAssocie.actionOnGame (nom, false);

		}
		lmAssocie.UpdateLevel (nom, level);

	}



	public void changeProduit(string nom){
		
		netview.RPC ("ChangeProduit", RPCMode.Others, nom, currentLevel);

	}

	[RPC]
	void ChangeProduit (string nom,int level,NetworkMessageInfo info)
	{
		if (level == currentLevel) {
			GameObject.Find ("EspaceExperience").GetComponent<SceneManagerExperience> ().newProduct (nom);
		} 
		lmAssocie.changeSettingExp (nom, level);
	}



	public void askCurrentProduit(){
		Debug.Log ("askproduit");
		netview.RPC ("AskCurrentProduit", RPCMode.Others, currentLevel);

	}

	[RPC]
	void AskCurrentProduit (int level,NetworkMessageInfo info)
	{
		if (level == currentLevel) {
			GameObject tmp = GameObject.Find ("EspaceExperience");
			if (tmp.GetComponent<SceneManagerExperience> ().estJoueur) {
				changeProduit(SceneManagerExperience.currentProduit.name);
			}
		}
	}


	public void changeNumSeq(int num){

		netview.RPC ("ChangeNumSeq", RPCMode.Others, num, currentLevel);

	}



	[RPC]
	void ChangeNumSeq (int num,int level,NetworkMessageInfo info)
	{
		lmAssocie.changeSettingExp (num, level);
	}

	public void ChangeLevel(int newLevel){
		
		if (Spectator) {
			SpecAssocie.connected = false;
		}
		StartCoroutine(ActionLevelChange (newLevel));

	}

	public void askVoiturePos(){

		netview.RPC ("AskVoiturePos", RPCMode.Others);

	}



	[RPC]
	void AskVoiturePos (NetworkMessageInfo info)
	{
		if (currentLevel == 1) {
			
			netview.RPC ("SendVoiturePos", info.sender, voiture.GetComponent<StopVoiture> ().getTimeAnim ());
		}
	}

	[RPC]
	void SendVoiturePos (float time,NetworkMessageInfo info)
	{
		if (currentLevel == 1) {
			float timedelta = (float)(Network.time - info.timestamp);
			Debug.Log ( timedelta);
			float timef = time + timedelta;
			voiture.GetComponent<StopVoiture> ().setTimeAnim (timef);
		}
	}




	public void stop(float temps){
		netview.RPC ("StopAllCar", RPCMode.Others,temps);
	}

	[RPC]
	void StopAllCar (float temps,NetworkMessageInfo info)
	{
		if (currentLevel == 1) {
			voiture.GetComponent<StopVoiture>().stopVoiture(temps);
		}
	}



	public void reprise(){
		netview.RPC ("RepriseAllCar", RPCMode.Others);
	}

	[RPC]
	void RepriseAllCar (NetworkMessageInfo info)
	{
		if (currentLevel == 1) {
			voiture.GetComponent<StopVoiture>().repriseVoiture();
		}
	}



	IEnumerator ActionLevelChange(int newLevel){

		downloadmanager.Instance.restartManager ();

        //SceneManager.LoadScene (newLevel);
        AsyncOperation asyncLoadLevel = SceneManager.LoadSceneAsync (newLevel, LoadSceneMode.Single);

		while (!asyncLoadLevel.isDone) {
			yield return null;
		}
		if (Spectator) {
			SpecAssocie.connected = true;
		}
		currentLevel = newLevel;

		if (!Spectator) {
			JoueurAssocie.GetComponent<Joueur> ().ChangeLevelPosition (newLevel);
		}
		lmAssocie.chargeLevel (newLevel);
		if (currentLevel == 1) {
			ensemblePOI = GameObject.Find ("ZonePI");
			PartieExperience = GameObject.Find ("CanvasLauncherExperience");
			Teleportation = GameObject.Find ("CanvasMap");
			gm.getListSpawn ();
			voiture=GameObject.Find("SC_anime_01");
			askVoiturePos ();

		}
		netview.RPC ("askEtatJeu", RPCMode.Server);
		yield return null;
	}

	public void TestConnection()
	{
		// Start/Poll the connection test, report the results in a label and
		// react to the results accordingly
		ConnectionTesterStatus connectionTestResult = Network.TestConnection();
		string testMessage = "";



		switch (connectionTestResult)
		{
		case ConnectionTesterStatus.Error:
			testMessage = "Problem determining NAT capabilities";

			break;

		case ConnectionTesterStatus.Undetermined:
			testMessage = "Undetermined NAT capabilities";

			break;

		case ConnectionTesterStatus.PublicIPIsConnectable:
			testMessage = "Directly connectable public IP address.";

			break;

			// This case is a bit special as we now need to check if we can
			// circumvent the blocking by using NAT punchthrough
		case ConnectionTesterStatus.PublicIPPortBlocked:
			testMessage = "Non-connectable public IP address (port " +
				Network.proxyPort + " blocked), running a server is impossible.";
			

		
			break;

		case ConnectionTesterStatus.PublicIPNoServerStarted:
			testMessage = "Public IP address but server not initialized, " +
				"it must be started to check server accessibility. Restart " +
				"connection test when ready.";
			break;

		case ConnectionTesterStatus.LimitedNATPunchthroughPortRestricted:
			testMessage = "Limited NAT punchthrough capabilities. Cannot " +
				"connect to all types of NAT servers. Running a server " +
				"is ill advised as not everyone can connect.";
			
			break;

		case ConnectionTesterStatus.LimitedNATPunchthroughSymmetric:
			testMessage = "Limited NAT punchthrough capabilities. Cannot " +
				"connect to all types of NAT servers. Running a server " +
				"is ill advised as not everyone can connect.";
		
			break;

		case ConnectionTesterStatus.NATpunchthroughAddressRestrictedCone:
		case ConnectionTesterStatus.NATpunchthroughFullCone:
			testMessage = "NAT punchthrough capable. Can connect to all " +
				"servers and receive connections from all clients. Enabling " +
				"NAT punchthrough functionality.";
			
			break;

		default:
			testMessage = "Error in test routine, got " + connectionTestResult;
			break;
		}

		//Debug.Log (testMessage);
		debugText.text = testMessage;
	}

	public double getTime(){
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		return (System.DateTime.UtcNow - epochStart).TotalSeconds;
	}

	//========================Espace dedie au changement FPS ================================================//

	/*Rejoindre partie:
	 * Permet de devenir un combattant et avoir sa couleur

	 */

	public void rejoindrePartie(){
		//on desactive les elements pouvant génés la visée fps
		ensemblePOI.SetActive (false);
		Teleportation.SetActive (false);
		PartieExperience.SetActive (false);
		combattant = true;
		// on ajoute le joueur dans la liste du game manager
		gm.PlayerJoin (nomJoueur, true);
		// si c'est le server du network on recupere la premiere couleur sinon on demande au server sa couleur
		if (Network.isServer) {
			JoueurAssocie.GetComponent<fighter> ().couleurAssocie = listColor [0];
			listColor.RemoveAt (0);
		} else {
			netview.RPC ("AskColor", RPCMode.Server);
		}

	}

	/*Requete rpc permettant de savoir ca couleur elle s'adresse au server seulement (color ne passe pas en rpc)
	 */
	[RPC]
	void AskColor (NetworkMessageInfo info)
	{
		Color c=listColor [0];
		listColor.RemoveAt (0);
        Vector3 col = (Vector4)c;
        netview.RPC ("ReceiveColor", info.sender,col);
	}

	/*retour a l'envoyeur du server avec la couleur associé pour le joueur
	 */
	[RPC]
	void ReceiveColor (Vector3 col,NetworkMessageInfo info)
	{
        Color c = (Vector4)col;
        JoueurAssocie.GetComponent<fighter> ().couleurAssocie = c;
	}

	/*Quitter partie permet de repartir à l'etat civil donc ne plus etre capable de tirée des boules colorés
	 * 
	 */
	public void quitterPartie(){
		// on reactive les interfaces
		ensemblePOI.SetActive (true);
		Teleportation.SetActive (true);
		PartieExperience.SetActive (true);
		combattant = false;
		//on enleve le joueur du game manager
		gm.PlayerLeave (nomJoueur, true);
		// on redonne au server la couleur pour un autre futur joueur
		if (Network.isServer) {
			listColor.Add(JoueurAssocie.GetComponent<fighter> ().couleurAssocie);

		} else {
			Color c = JoueurAssocie.GetComponent<fighter> ().couleurAssocie;
            Vector3 col = (Vector4)c;
            netview.RPC ("RemoveColor", RPCMode.Server,col);
		}

	}

	//l'envoi de la couleur au serveur par requette rpc
	[RPC]
	void RemoveColor (Vector3 col,NetworkMessageInfo info)
	{
        Color c = (Vector4)col;
        listColor.Add(c);
	}

	//Fonction d'appelle de requette rpc AjouterJoueurPartie depuis gameManager
	public void ajouterJoueurPartie(string nom){
		netview.RPC ("AjouterJoueurPartie",RPCMode.Others,nom);
	}

	//Fonction RPC pour les autres permettant de dire que un joueur a rejoins la partie fps en cours
	[RPC]
	void AjouterJoueurPartie (string nom,NetworkMessageInfo info)
	{
		gm.PlayerJoin (nom, false);
	}

	//Fonction d'appelle de requette rpc RetirerJoueurPartie depuis gameManager
	public void retirerJoueurPartie(string nom){
		netview.RPC ("RetirerJoueurPartie",RPCMode.Others,nom);
	}

	//Fonction RPC pour les autres permettant de dire que un joueur a quitté la partie fps en cours
	[RPC]
	void RetirerJoueurPartie (string nom,NetworkMessageInfo info)
	{
		gm.PlayerLeave (nom, false);
	}

	//Fonction d'appelle de requette rpc CreerBalle depuis fighter
	public void creerBalle(Vector3 position,Vector3 trajectoire,Vector3 col){
		netview.RPC ("CreerBalle",RPCMode.Others,position,trajectoire,col,nomJoueur);
	}

	/*Fonction rpc permettant la création de la balle pour tous les joueurs au meme endroit
	 * Vector3 position= position a laquelle on crée la balle
	 * Vector3 trajectoire= trajectoire de la balle
	 * Vector3 col=couleur de la balle
	 * string nom= nom du joueur ayant tiré la balle
	 * 
	 */
	[RPC]
	void CreerBalle (Vector3 position,Vector3 trajectoire,Vector3 col,string nom,NetworkMessageInfo info)
	{
		//on verifie que le joeur joue bien dans la partie
		if (currentLevel == 1 && combattant) {
            Color c = (Vector4)col;
            // on appelle la création de balle associe de la classe fighter
            JoueurAssocie.GetComponent<fighter> ().creerBalle (position, trajectoire,c,nom);
		}
	}

	//Fonction d'appelle de ToucheJoueur depuis GameManager
    public void toucheJoueur(string tireur, string victime)
    {
        netview.RPC("ToucheJoueur", RPCMode.Others, tireur, victime);
    }

	/*Fonction rpc permettant de signifier aux autres que l'ont à été touché par une balle
	 * Vector3 tireur= le nom de la personne ayant tiré la balle
	 * string victime= le nom de la personne touché qui envoit cette requete

	 */

    [RPC]
    void ToucheJoueur(string tireur, string victime)
    {
        gm.PlayerHit(tireur, victime, false);
    }

	/*Fonction rpc permettant à une personne venant de se connecter de mettre à jour son GameManager
	 */
	[RPC]
	void askEtatJeu (NetworkMessageInfo info)
	{
		foreach(string nom in gm.ListNomJoueur){
			if (nom == "ObjetDeplacement") {
				netview.RPC ("AjouterJoueurPartie",info.sender,nomJoueur);
			} else {
				netview.RPC ("AjouterJoueurPartie",info.sender,nom);
			}
		}
	}

	//Fonction d'appelle de CreerPersonnageExplosion depuis Balle
	public void creerPersonnageExplosion(Vector3 pos,Vector3 color){
		if (gm.partieEnCours) {
			netview.RPC ("CreerPersonnageExplosion", RPCMode.All, pos, color);
		}
	}


	/*Fonction rpc permettant de representer pour tous par une explosion de particule la mort d'un des joueur :'(
	 * Vector3 pos=position a laquelle on doit creer l'objet
	 * Vector3 col= couleur des particule emise
	 */
	[RPC]
	void CreerPersonnageExplosion(Vector3 pos,Vector3 col){
		// on load l'objet depuis resources
		GameObject nouv = Instantiate (Resources.Load ("HommeTouche")) as GameObject;
        //on met la couleur de l'attaquant dedans
        Color c = (Vector4)col;
        //main.startColor
        nouv.GetComponent<ParticleSystem> ().startColor = c;
		// on le place au bon endroit
		nouv.transform.position = pos;

		//PS: l'element crée disparaitra par lui meme au bout d'un certain temps

	}

	//Fonction d'appelle de CreerPersonnageExplosion depuis GameManager
	public void lancementPartie(string nom){
		netview.RPC ("LancementPartie",RPCMode.All,nom);
	}


	/*Fonction RPC de lancement de la partie pour tous les joueurs
	 * string nom = nom du joueur a qui on demande de respawn
	 */
	[RPC]
	void LancementPartie(string nom){
		// on passe que par les combattants
		if (combattant) {
			
			if (nom == nomJoueur) {
				// si on est la bonne personne on est alors teleporté vers un point de spawn et la partie commence
				JoueurAssocie.GetComponent<fighter> ().positionLancementPartie = JoueurAssocie.transform.position;
				JoueurAssocie.GetComponent<fighter> ().respawn ();
			}
		}
	}

	//Fonction d'appelle de MaintenantEclaterVousOuPas depuis GameManager
	public void maintenantEclaterVous(bool etat){
		netview.RPC ("MaintenantEclaterVousOuPas",RPCMode.All,etat);
	}

	/*Requete rpc qui defini le demarage de la partie pour tous les joueurs en leur donnant les points de vie/ou la fin de la partie en les rescuscitant
	 * bool etat= si vrai alors on donne les points de vie si faux alors on revive
	 */
	[RPC]
	void MaintenantEclaterVousOuPas(bool etat){
		
		if (combattant) {
			//appelle de revive qui va remettre les gens a leur position pre spawn et visible des autres
			if (!etat) {
				
				JoueurAssocie.GetComponent<fighter> ().revive ();
                gm.ResetAll();
			} else {
				//on donne des points de vie aux joueurs qui leur permettront donc d'etre touche
				JoueurAssocie.GetComponent<fighter> ().vie = gm.viedebase;
			}
			//on lance la partie pour tous 
			gm.partieEnCours = etat;
		}

	}

	//Fonction d'appelle de HeyLesgarsJeSuisMort depuis fighter
	public void heyLesgarsJeSuisMort(){
		netview.RPC ("HeyLesgarsJeSuisMort",RPCMode.All);
	}

	/*Fonction pour signifier à tous les joueur qu'on est mort le lanceur de la partie prendra en compte cette mort  pour verifier si la partie doit durer
	 * 
	 * 
	 */
	[RPC]
	void HeyLesgarsJeSuisMort(){
		if (combattant) {
			gm.ajouterMort ();
		}

	}
	//========================FIN Espace dedie au changement FPS ================================================//
}