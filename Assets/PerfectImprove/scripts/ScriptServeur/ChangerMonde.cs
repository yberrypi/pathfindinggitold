﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangerMonde : MonoBehaviour {
	public int valeurScene;

    public static bool Active
    {
        get
        {
            return col.enabled;
        }

        set
        {
            col.enabled = value;
        }
    }

    private static Collider col;

    private void Awake()
    {
        col = GetComponent<Collider>();
    }

    public void OnClick(){
		
		NetworkManager.instance.ChangeLevel(valeurScene);
	
	}
}
