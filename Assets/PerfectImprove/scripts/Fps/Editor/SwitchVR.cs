﻿using UnityEditor;
using UnityEditorInternal.VR;

public class SwitchVR
{

    [MenuItem("Platform/Windows", false)]
    private static void Windows()
    {
        SetWindows();
    }

    [MenuItem("Platform/Android", false)]
    private static void Android()
    {
        SetAndroid();
    }

    private static void SetWindows()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
        VREditor.SetVREnabledOnTargetGroup(BuildTargetGroup.Standalone, true);
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Standalone, new string[] { "OpenVR" });
    }

    private static void SetAndroid()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        VREditor.SetVREnabledOnTargetGroup(BuildTargetGroup.Android, true);
        VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Android, new string[] { "Oculus" });
    }
}