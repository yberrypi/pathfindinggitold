﻿using UnityEditor;

[CustomEditor(typeof(CurvedImage))]
public class CurvedImageEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}
