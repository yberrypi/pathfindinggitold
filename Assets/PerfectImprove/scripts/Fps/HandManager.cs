﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Hand))]
public class HandManager : MonoBehaviour {

    public fighter fight;
    public LayerMask mask;
    public Transform laser;

    private Hand hand;
    private Vector3 scale;
    private UIElement currentUI = null;

    private void Awake()
    {
        hand = GetComponent<Hand>();
        scale = laser.localScale;
    }

    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 200f, mask))
        {
            UIElement ui = hit.collider.GetComponent<UIElement>();

            if (ui)
            {
                laser.position = Vector3.Lerp(transform.position, hit.point, 0.5f);
                scale.z = hit.distance;
                laser.localScale = scale;
                laser.gameObject.SetActive(true);
                if (currentUI == ui)
                {
                    currentUI.SendMessage("HandHoverUpdate", hand, SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    if (currentUI)
                    {
                        currentUI.SendMessage("OnHandHoverEnd", hand, SendMessageOptions.DontRequireReceiver);
                    }
                    ui.SendMessage("OnHandHoverBegin", hand, SendMessageOptions.DontRequireReceiver);
                    currentUI = ui;
                }
                
            }
            else
            {
                HoverEnd();
            }
        }
        else
        {
            HoverEnd();
        }

        if (hand.GetStandardInteractionButtonDown() && fight.nm.combattant && fight.vivant)
        {
            fight.creerBalle(transform.position, transform.forward, fight.couleurAssocie, fight.nm.nomJoueur);
            Vector3 col = (Vector4)fight.couleurAssocie;
            fight.nm.creerBalle(transform.position, transform.forward, col);
        }
    }

    private void HoverEnd()
    {
        if (currentUI)
        {
            currentUI.SendMessage("OnHandHoverEnd", hand, SendMessageOptions.DontRequireReceiver);
            currentUI = null;
        }
        laser.gameObject.SetActive(false);
    }
}
