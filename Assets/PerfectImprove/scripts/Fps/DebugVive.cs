﻿using UnityEngine;
using UnityEngine.UI;

public class DebugVive : MonoBehaviour {

    public Transform panel;
    public Text prefabText;
    public Text fpsText;

    private float time = 0f;
    private const float limit = 1f;

    private void Update()
    {
        time += Time.deltaTime;
        if (time >= limit)
        {
            time = 0f;
            float fps = Mathf.Round(1f / Time.deltaTime);
            fpsText.text = fps.ToString() + " FPS";
        }
    }

    private void OnEnable()
    {
        Application.logMessageReceived += GetLog;
    }

    private void OnDisable()
    {
        Application.logMessageReceived -= GetLog;
    }

    private void GetLog(string condition, string stackTrace, LogType type)
    {
        if (type == LogType.Log)
        {
            Text trace = Instantiate(prefabText, panel);
            trace.text = stackTrace;
            trace.transform.SetAsFirstSibling();

            Text cond = Instantiate(prefabText, panel);
            cond.text = condition;
            cond.fontStyle = FontStyle.Bold;
            cond.transform.SetAsFirstSibling();
        }
    }
}
