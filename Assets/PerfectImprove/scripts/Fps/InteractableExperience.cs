﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class InteractableExperience : MonoBehaviour {

    private ObjetInteractible oi;

    private void Start()
    {
        oi = GetComponent<ObjetInteractible>();
    }

    private void HandHoverUpdate(Hand hand)
    {
        if (hand.GetStandardInteractionButtonDown())
        {
            oi.ObjectValidate();

            if (SceneManagerExperience.viveCanPick)
            {
                Transform tr = SceneManagerExperience.currentMovingObject.transform;
                tr.SetParent(hand.transform);
                tr.localPosition = Vector3.zero;
                tr.localRotation = Quaternion.identity;
            }
        }
    }
}
