﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*Classe permettant de passer de "civil" à "combattant" et vice versa

 * Variable:
 * LaunchGame: bouton pour lancer la partie une fois joueur
 * Gm: Objet non destructible GameManager doit avoir un nom unique
 */
public class ChangerEtatPersonnage : MonoBehaviour {
	public GameObject LaunchGame;

	private GameManager gm;
    private Button[] buttons;
    private BoxCollider[] colliders;
    private Text fighterText;

	// on cherche a l'initialisation l'objet gameManager unique
	void Start(){
        gm = GameManager.instance;

        buttons = new Button[2];
        buttons[0] = GetComponent<Button>();
        buttons[1] = LaunchGame.GetComponent<Button>();

        colliders = new BoxCollider[2];
        colliders[0] = GetComponent<BoxCollider>();
        colliders[1] = LaunchGame.GetComponent<BoxCollider>();

        fighterText = buttons[0].GetComponentInChildren<Text>();
    }

	//Update nous sert juste a savoir si on peut interagir avec les boutons
	void Update(){

        bool active = !gm.partieEnCours;

        foreach (Button button in buttons)
        {
            button.interactable = active;
        }
        foreach (BoxCollider col in colliders)
        {
            col.enabled = active;
        }
	}

	//Quand on clique sur devenir combattant (premier bouton)
	public void onClick(){
        NetworkManager nm = NetworkManager.instance;
		if (nm.combattant) {
			LaunchGame.SetActive (false);
			nm.quitterPartie ();
            fighterText.text = localization.GetTextWithKey("BECOMEFIGHTER_TEXT");
		} else {
			LaunchGame.SetActive (true);
			nm.rejoindrePartie ();
            fighterText.text = localization.GetTextWithKey("RETURNCIVILIAN_TEXT");
        }
	}
}
