﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CurvedText : Text {

    public bool curved;
    public float offsetAngle = 0f;
    public float scaleFactor = 1f;

    private float Ray
    {
        get
        {
            return rectTransform.rect.width * 0.5f;
        }
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.OnValidate();
        RAZ();
    }
#endif

    protected override void OnPopulateMesh(VertexHelper toFill)
    {
        base.OnPopulateMesh(toFill);

        if (!curved)
        {
            return;
        }

        List<UIVertex> stream = new List<UIVertex>();

        toFill.GetUIVertexStream(stream);

        float ext = Ray;

        float pivot = 1f - 2f * rectTransform.pivot.x;

        float startAngle = pivot - offsetAngle * Mathf.Deg2Rad;

        switch ((int)alignment % 3)
        {
            case 0: //Left
                startAngle += Mathf.PI - 1f;
                break;

            case 1: //Center
                startAngle += Mathf.PI * 0.5f;
                break;

            case 2: //Right
                startAngle += 1f;
                break;
        }

        for (int i = 0; i < stream.Count; i++)
        {
            UIVertex v = stream[i];

            float angle = startAngle - v.position.x / ext;

            float x = ext * (pivot + Mathf.Cos(angle));
            float z = ext * Mathf.Sin(angle);

            v.position = scaleFactor * new Vector3(x, v.position.y, z);

            stream[i] = v;
        }

        toFill.Clear();
        toFill.AddUIVertexTriangleStream(stream);
    }

    private void Update()
    {
        RAZ();
    }

    private void RAZ()
    {
        if (scaleFactor < 0f)
        {
            scaleFactor = 0f;
        }
    }
}
