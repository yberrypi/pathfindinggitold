﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestYanis : MonoBehaviour {

    /// <summary>
    /// Classe de stockage des scores et de leur affichage
    /// </summary>
    public class AffichageJoueur
    {
        public string joueur;
        public int pv;
        public int score;
        public Text affichage;

        /// <summary>
        /// Vérifie si les PV du joueur sont supérieurs à 0
        /// </summary>
        public bool Vivant
        {
            get
            {
                return pv > 0;
            }
        }

        public AffichageJoueur(string jr, Text aff)
        {
            joueur = jr;
            pv = 10;
            score = 0;
            affichage = aff;
        }

        /// <summary>
        /// Affiche les statistiques du joueur
        /// </summary>
        public void Affiche()
        {
            if (pv > 0)
            {
                affichage.fontStyle = FontStyle.Normal;
                affichage.color = Color.black;
            }
            else
            {
                affichage.fontStyle = FontStyle.Italic;
                affichage.color = Color.gray;
            }

            affichage.text = ToString();
        }

        /// <summary>
        /// Ajoute 1 point à celui qui a touché un ennemi
        /// </summary>
        public void AddScore()
        {
            score++;
        }

        /// <summary>
        /// Retire 1 PV à celui qui a été touché
        /// </summary>
        /// <returns>TRUE si les PV tombent à zéro, FALSE sinon</returns>
        public bool MinusPV()
        {
            if (pv > 0)
            {
                pv--;
            }
            return pv == 0;
        }

        public override string ToString()
        {
            return joueur + " : " + pv.ToString() + " PV, " + score.ToString() + " pt" + (score > 1 ? "s" : "");
        }
    }

    [Header("Ajout")]
    public InputField inputAdd;
    public Button buttonAdd;

    [Header("Suppression")]
    public Dropdown dropDelete;
    public Button buttonDelete;

    [Header("Contrôles")]
    public Dropdown dropJ1;
    public Dropdown dropJ2;
    public Button buttonHit;

    [Header("Affichage")]
    public RectTransform players;
    public Text prefabPlayer;

    private List<AffichageJoueur> affichages = new List<AffichageJoueur>();
    private List<int> order = new List<int>();

	// Use this for initialization
	void Start () {
        inputAdd.onValueChanged.AddListener(ActivateAdd);
        buttonAdd.onClick.AddListener(AddJoueur);

        dropDelete.ClearOptions();
        dropDelete.options.Add(new Dropdown.OptionData("Joueur"));
        dropDelete.onValueChanged.AddListener(ActivateDelete);
        dropDelete.captionText.text = "Joueur";
        buttonDelete.onClick.AddListener(DeleteJoueur);

        dropJ1.ClearOptions();
        dropJ1.options.Add(new Dropdown.OptionData("Tireur"));
        dropJ2.ClearOptions();
        dropJ2.options.Add(new Dropdown.OptionData("Victime"));
        dropJ1.onValueChanged.AddListener(i => ActivateHit());
        dropJ2.onValueChanged.AddListener(i => ActivateHit());
        dropJ1.captionText.text = "Tireur";
        dropJ2.captionText.text = "Victime";
        buttonHit.onClick.AddListener(HitJoueur);
    }
	
	void AfficheTout()
    {
        order.Sort(SortScores);
        for (int i = 0; i < affichages.Count; i++)
        {
            int index = order[i];
            affichages[index].affichage.transform.SetSiblingIndex(i);
            affichages[index].Affiche();
        }
    }

    void ActivateAdd(string nom)
    {
        buttonAdd.interactable = nom != "" && affichages.TrueForAll(aj => aj.joueur != nom);
    }

    void AddJoueur()
    {
        string add = inputAdd.text.Trim();

        Text affichage = Instantiate(prefabPlayer, players);
        affichages.Add(new AffichageJoueur(add, affichage));
        order.Add(order.Count);

        dropDelete.options.Add(new Dropdown.OptionData(add));
        dropJ1.options.Add(new Dropdown.OptionData(add));
        dropJ2.options.Add(new Dropdown.OptionData(add));

        inputAdd.text = "";
        AfficheTout();
    }

    void ActivateDelete(int index)
    {
        buttonDelete.interactable = index > 0;
    }

    void DeleteJoueur()
    {
        int index = dropDelete.value;

        dropDelete.options.RemoveAt(index);
        dropDelete.value = 0;

        dropJ1.options.RemoveAt(index);
        dropJ1.value = 0;

        dropJ2.options.RemoveAt(index);
        dropJ2.value = 0;

        Destroy(affichages[index - 1].affichage.gameObject);
        affichages.RemoveAt(index - 1);
        order.Remove(index - 1);
        for (int i = 0; i < order.Count; i++)
        {
            if (order[i] >= index)
            {
                order[i]--;
            }
        }

        AfficheTout();
    }

    void ActivateHit()
    {
        int i1 = dropJ1.value;
        int i2 = dropJ2.value;
        buttonHit.interactable = i1 > 0 && i2 > 0 && i1 != i2;
    }

    void HitJoueur()
    {
        AffichageJoueur tireur = affichages[dropJ1.value - 1];
        AffichageJoueur victime = affichages[dropJ2.value - 1];

        if (tireur.Vivant && victime.Vivant)
        {
            tireur.AddScore();
            victime.MinusPV();

            AfficheTout();
        }
    }

    /// <summary>
    /// Fonction de tri des Scores
    /// </summary>
    /// <param name="aj1">Score 1</param>
    /// <param name="aj2">Score 2</param>
    /// <returns>Comparaison</returns>
    public int SortScores(int i1, int i2)
    {
        AffichageJoueur aj1 = affichages[i1];
        AffichageJoueur aj2 = affichages[i2];

        if (aj1.Vivant != aj2.Vivant)
        {
            return aj1.Vivant ? -1 : 1;
        }

        int score = aj2.score.CompareTo(aj1.score);

        if (score != 0)
        {
            return score;
        }

        int pv = aj2.pv.CompareTo(aj1.pv);

        if (pv != 0)
        {
            return pv;
        }

        return aj1.joueur.CompareTo(aj2.joueur);
    }
}
