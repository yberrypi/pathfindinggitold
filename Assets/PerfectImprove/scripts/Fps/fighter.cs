﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Class fighter: permet d'effectuer toutes les actions d'un combattant
 * public int vie=Vie actuelle du joueur
 * public bool vivant= savoir si le joueur est encore actif dans la partie
 * public bool invincible= booleen permettant de savoir si un joueur peut etre touché
 * public NetworkManager nm= network manager associe
 * GameManager gm= gamemanager associe
 * float tempsEntreClick= temps apres un click
 * public float tempsMaxEntreClick=pour simuler un tir on a besoin de parametrer un temps entre deux clicks
 * bool firstClick= bool si un click à été effectué
 * public Color couleurAssocie= couleur des balles et lunettes du combattant
 * public Vector3 positionLancementPartie= tricky, position au lancement de la partie qui est repris a la fin de la partie pour etre sur que les gens vont pas se bloquer dans le decor
 * 
 */
public class fighter : MonoBehaviour {
	public int vie;
	public bool vivant;
	public bool invincible;
	public NetworkManager nm;
	GameManager gm;
	float tempsEntreClick;
	public float tempsMaxEntreClick;
	bool firstClick;
	public Color couleurAssocie;
	public Vector3 positionLancementPartie;


	void Start(){
		
		vivant = true;
        gm = GameManager.instance;
        nm = NetworkManager.instance;
	}
	// Update is called once per frame
	void Update () {
		if (nm.combattant) {
			//simulation double click
			if (firstClick) {
				if (Input.GetMouseButtonDown (0)) {
					//trop de temps entre les 2 click
					if (tempsEntreClick > tempsMaxEntreClick) {
						tempsEntreClick = 0;
					} 
					//on effectue un tir et on reinitialise
					else {
						tir ();
						tempsEntreClick = 0;
						firstClick = false;
					}
				} else {
					tempsEntreClick += Time.deltaTime;
				}
			} else {
				if (Input.GetMouseButtonDown (0)) {
					firstClick = true;
				} 
			}



		}
	}

	/* Fonction de reaction apres s'etre pris une balle
	 * string Attaquant=nom de la personne ayant tiré la balle
	 */
	public void touche(string Attaquant){
		// si la partie est lancé et le joueur est toujours vivant
		if (gm.partieEnCours && vivant) {
			Debug.Log ("touche");
			// on enleve une vie
			vie -= 1;
			// on signale au game manager et aux autres qu'on a été touché
			gm.PlayerHit(Attaquant, nm.nomJoueur, true);
			// on test si on est mort si oui on devient invisible sinon on respawn sur la map
			if (vie < 1) {
				mort ();
			} else {
				respawn ();
			}
		}
	}

	/* Fonction de mort quand le nombre de pv atteint 0 en jeu
	 */
	public void mort(){
		
		vivant = false;
		// on lui permet d'etre un ghost
		GetComponent<CapsuleCollider> ().enabled = false;
		// on dit aux autres qu'on est mort
		nm.heyLesgarsJeSuisMort ();
	}

	/* Fonction de revive quand la partie est fini
	 */
	public void revive(){
		// on lui redonne la position
		transform.position = positionLancementPartie;
		// on le reactive et lui rend son collider
		vivant = true;
		GetComponent<CapsuleCollider> ().enabled = true;
		// on lui redonne des pvs
		vie =gm.viedebase;
	}

	/*action de tir apres un double click
	 */
	public void tir(){
		if (vivant) {
			Debug.Log ("tir effectue");
			//on creer une balle avec les bonnes informations
			creerBalle (transform.position, transform.GetChild (0).forward, couleurAssocie, nm.nomJoueur);
            //on donne aux autres l'ordre de créer la meme balle
            Vector3 col = (Vector4)couleurAssocie;
			nm.creerBalle (transform.position, transform.GetChild (0).forward, col);
		}
	}

	/*fonction pour creer une balle peut etre appeller par requette rpc ou directement depusi fighter
	 * Vector3 position=position de la balle
	 * Vector3 trajectoire= trajectoire de la balle
	 * Color couleur=couleur de la balle
	 * string nom= nom du tireur de la balle
	 */
	public void creerBalle(Vector3 position,Vector3 trajectoire,Color couleur,string nom){
		// on instancie le prefab depuis ressources
		GameObject nouv = Instantiate (Resources.Load ("Balle"))as GameObject;
		balle b = nouv.GetComponent<balle> ();
        // on lance le bruitage
        b.Feu();
		b.nomTireur = nom;
		Vector3 forw =trajectoire ;
		b.trajectoire =forw ;

		nouv.GetComponent<Renderer> ().material.color = couleur;
		nouv.GetComponent<TrailRenderer> ().material.color = couleur;
		nouv.GetComponent<Light> ().color = couleur;
		// on s'assure que la balle ne vas pas collider avec le tireu directement
		nouv.transform.position = position + forw;
	}

	/* Fonction de respawn dans l'un des spawn du jeu
	 */
	public void respawn(){
		transform.position = gm.getSpawn ();
	}
}
