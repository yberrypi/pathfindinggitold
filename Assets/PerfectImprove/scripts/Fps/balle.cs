﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balle : MonoBehaviour {

	//vitesse de deplacement de la balle dans un sens rectiligne uniforme a changer dans le prefab
	public float vitesse;
	//trajectoire de la balle initialisé a son apparition
	public Vector3 trajectoire;
	//parametre pour le score pour savoir qui touche qui
	public string nomTireur;
	//son associés pour impact de balle dans un collider ou pour un passage proche (à ameliorer)
	public AudioClip ImpactBalle;

    private AudioSource source;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame on s'en sert juste pour faire avancer la balle
    void Update () {
		//Debug.Log (BruitFeu.isPlaying);
		transform.position += vitesse * trajectoire * Time.deltaTime;
	}


	// se declenche au moment d'une colision avec un box collider
	void OnCollisionEnter(Collision collision){
		StartCoroutine(onCollision (collision));
		//collision.collider.name;

	}

	//Coroutine lancer apres la collision gere le son, si l'objet touché est le joueur ("objet deplacement de la scene") 
	//on effectue l'action d'etre touché et on crée un objet d'animation le tout connecté au network


	IEnumerator onCollision(Collision collision){
		source.Stop ();
        Light light = GetComponent<Light>();
		GetComponent<MeshRenderer> ().enabled = false;
		light.enabled = false;
		GetComponent<TrailRenderer> ().enabled = false;
        source.PlayOneShot(ImpactBalle);

        fighter fight = collision.gameObject.GetComponent<fighter>();

        if (collision.gameObject.name == "ObjetDeplacement" && fight.nm.nomJoueur != nomTireur) {
			Color c = light.color;
            Vector3 col = (Vector4)c;
            
            fight.nm.creerPersonnageExplosion(collision.contacts[0].point, col);
			fight.touche (nomTireur);
		}
		while (source.isPlaying) {
			yield return null;
		}
		Destroy (gameObject);

	}

    public void Feu()
    {
        source.Play();
    }
}
