﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*DestroyAfterXSecond est une classe permettant qu'un objet se detruise apres un temps x données (surtout utilisé pour le prefab homme touche)
 * float TempsDestroy= la durée d'existence de l'objet
 * float runningTime= le temps depuis que l'objet existe
 */
public class DestroyAfterXSecond : MonoBehaviour {
	
	public float TempsDestroy;
	float runningTime = 0;
	// Use this for initialization
	void Start () {
		runningTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		runningTime += Time.deltaTime;
		if (runningTime > TempsDestroy) {
			Destroy (gameObject);
		}
	}
}
