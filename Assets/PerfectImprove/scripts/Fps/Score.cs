﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    /// <summary>
    /// Classe de stockage des scores et de leur affichage
    /// </summary>
    public class AffichageJoueur
    {
        public string joueur;
        public int pv;
        public int score;
        public Text affichage;

        /// <summary>
        /// Vérifie si les PV du joueur sont supérieurs à 0
        /// </summary>
        public bool Vivant
        {
            get
            {
                return pv > 0;
            }
        }

        public AffichageJoueur(string jr, int life, Text aff)
        {
            joueur = jr;
            pv = life;
            score = 0;
            affichage = aff;
        }

        /// <summary>
        /// Affiche les statistiques du joueur
        /// </summary>
        /// <param name="rank">Numéro de rang à afficher (rien si égal à zéro)</param>
        public void Affiche(int rank = 0)
        {
            if (pv > 0)
            {
                affichage.fontStyle = FontStyle.Normal;
                affichage.color = Color.black;
            }
            else
            {
                affichage.fontStyle = FontStyle.Italic;
                affichage.color = Color.gray;
            }

            string r = rank.ToString() + (rank == 1 ? "er" : "ème");

            affichage.text = (rank > 0 ? r + " - " : "") + ToString();
        }

        /// <summary>
        /// Ajoute 1 point à celui qui a touché un ennemi
        /// </summary>
        public void AddScore()
        {
            score++;
        }

        /// <summary>
        /// Retire 1 PV à celui qui a été touché
        /// </summary>
        /// <returns>TRUE si les PV tombent à zéro, FALSE sinon</returns>
        public bool MinusPV()
        {
            if (pv > 0)
            {
                pv--;
            }
            return pv == 0;
        }

        public override string ToString()
        {
            return joueur + " : " + pv.ToString() + " PV, " + score.ToString() + " pt" + (score > 1 ? "s" : "") ;
        }
    }

    private static List<Score> _instances = new List<Score>();

    public static List<Score> instances
    {
        get
        {
            if ( _instances.Count == 0)
            {
                GameObject obj = new GameObject("Score");

                Score sc = obj.AddComponent<Score>();

                _instances.Add(sc);
            }

            return _instances;
        }
    }

    public GameManager gm;
    public GameObject panel;
    public RectTransform canvasJoueurs;
    public Text affTime;

    public Text prefabScore;

    private List<AffichageJoueur> affichagesJoueurs = new List<AffichageJoueur>();

    private void Awake()
    {
        if (!_instances.Contains(this))
        {
            _instances.Add(this);
            DontDestroyOnLoad(gameObject);
        }

        if (!gm)
        {
            gm = GameManager.instance;
        }
    }

    private void Update()
    {
        //Affichage du temps
        int time = Mathf.CeilToInt(gm.TempsRestant);
        affTime.color = time > 9 ? Color.black : Color.red;
        affTime.text = "Temps : " + time.ToString();
        affTime.gameObject.SetActive(gm.partieEnCours);
    }

    /// <summary>
    /// Mise à jour des scores lors d'un tir réussi
    /// </summary>
    /// <param name="attaquant">Attaquant</param>
    /// <param name="blesse">Attaqué</param>
    public void ChangeScore(string attaquant, string blesse)
    {
        int shooter = affichagesJoueurs.FindIndex(aj => aj.joueur == attaquant);
        int victim = affichagesJoueurs.FindIndex(aj => aj.joueur == blesse);

        if (shooter != -1 && victim != -1 && affichagesJoueurs[victim].Vivant)
        {
            affichagesJoueurs[shooter].AddScore();
            affichagesJoueurs[victim].MinusPV();
        }

        AfficherScore();
    }

    /// <summary>
    /// Affiche tous les scores
    /// </summary>
    public void AfficherScore()
    {
        panel.SetActive(affichagesJoueurs.Count > 0);

        affichagesJoueurs.Sort(SortScores);
        int rank = 1;
        for (int i = 0; i < affichagesJoueurs.Count; i++)
        {
            AffichageJoueur current = affichagesJoueurs[i];
            if (i > 0)
            {
                AffichageJoueur previous = affichagesJoueurs[i - 1];
                if (!previous.Vivant || !current.Vivant || previous.pv != current.pv || previous.score != current.score)
                {
                    rank = i + 1;
                }
            }

            current.affichage.transform.SetSiblingIndex(i);
            current.Affiche(rank);
        }
    }

    /// <summary>
    /// Ajout d'un joueur
    /// </summary>
    /// <param name="nomJoueur">Nom du joueur</param>
    public void AddPlayer(string nomJoueur)
    {
		Text aff = Instantiate(prefabScore, canvasJoueurs);
        affichagesJoueurs.Add(new AffichageJoueur(nomJoueur, gm.viedebase, aff));

        AfficherScore();
    }

    /// <summary>
    /// Suppression du score d'un joueur
    /// </summary>
    /// <param name="nomJoueur">Nom du Joueur</param>
    /// <returns>TRUE si un score a été supprimé, FALSE sinon</returns>
    public bool RemovePlayer(string nomJoueur)
    {
        int index = affichagesJoueurs.FindIndex(aj => aj.joueur == nomJoueur);

        if (index != -1)
        {
            Destroy(affichagesJoueurs[index].affichage.gameObject);
            affichagesJoueurs.RemoveAt(index);
            AfficherScore();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Efface tous les scores
    /// </summary>
    public void ResetAll()
    {
        foreach (AffichageJoueur aj in affichagesJoueurs)
        {
            Destroy(aj.affichage.gameObject);
        }

        affichagesJoueurs.Clear();

        AfficherScore();
    }

    /// <summary>
    /// Fonction de tri des Scores
    /// </summary>
    /// <param name="aj1">Score 1</param>
    /// <param name="aj2">Score 2</param>
    /// <returns>Comparaison</returns>
    protected virtual int SortScores(AffichageJoueur aj1, AffichageJoueur aj2)
    {
        if (aj1.Vivant != aj2.Vivant)
        {
            return aj1.Vivant ? -1 : 1;
        }

        int score = aj2.score.CompareTo(aj1.score);

        if (score != 0)
        {
            return score;
        }

        int pv = aj2.pv.CompareTo(aj1.pv);

        if (pv != 0)
        {
            return pv;
        }

        return aj1.joueur.CompareTo(aj2.joueur);
    }
}
