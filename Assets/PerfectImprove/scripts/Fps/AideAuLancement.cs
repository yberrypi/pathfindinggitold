﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AideAuLancement : MonoBehaviour {
	
	// Use this for initialization
	//probably useless
	public void OnClick(){
		GameManager.instance.LancerPartie ();
	}

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
