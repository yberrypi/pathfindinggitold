﻿using UnityEngine;
using UnityEngine.VR;

public class ManageVive : MonoBehaviour {

    public GameObject prefabOculus;
    public GameObject prefabVive;

    public static bool IsVivePresent
    {
        get
        {
            return VRSettings.loadedDeviceName == "OpenVR";
        }
    }

	void Awake () {
#if UNITY_ANDROID
        GameObject player = Instantiate(prefabOculus, transform);
#else
        GameObject player = Instantiate(IsVivePresent ? prefabVive : prefabOculus, transform);
#endif
        player.name = "ObjetDeplacement";
        NetworkManager.instance.JoueurAssocie = player;
    }
}
