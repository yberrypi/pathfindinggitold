﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*GameManager comme son nom l'indique il permet de faire fonctionner le jeu entre les joeurs et l'environnement
 * private static GameManager _instance=instance de l'objet qui doit etre unique
 * public bool partieEnCours= savoir si une partie est lancé
 * public int nbJoueurs = nbr joueur dans la partie
 *  public float tempsPartie = durée d'une parti, sauf si les joueurs meurt avant
 * public int viedebase=vie qui sera attribué a chaque joueur en debut de partie
 * public float nbrMort= nombre de mort si en cours de partie celui ci tombe à nombre de joueur -1 alors la partie s'arrete
 *float runningTime = temps decompte par rapport a un event (ici pour calculer quand la partie s'arrete)
 *public Score scoreAssocie=Score associé pour l'affichage des scores aux joueurs
 *public List<GameObject> ListJoueur= liste des différents objet jouant aux jeux
 *public List<string> ListNomJoueur= le nom de ces joueurs
 *public NetworkManager nm = permet d'appeler plus rapidement le network manager
 *public List<Transform> ListSpawn = list de spawn possible pour les joueurs
 * 
 * 
 */

public class GameManager : MonoBehaviour {

    private static GameManager _instance = null;

    public static GameManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject obj = new GameObject("GameManager");

                _instance = obj.AddComponent<GameManager>();

                DontDestroyOnLoad(obj);
            }

            return _instance;
        }
    }

	public bool partieEnCours;
    public int nbJoueurs = 0;
    public float tempsPartie = 300f;
	public int viedebase;
	public float nbrMort;
	float runningTime = 0f;
	public List<GameObject> ListJoueur=new List<GameObject>();
	public List<string> ListNomJoueur=new List<string>();
	public NetworkManager nm;

	//spawn 
	public List<Transform> ListSpawn;

	/*Permet de calculer le temps restant dans une partie pour le score
	 */
    public float TempsRestant
    {
        get
        {
            return tempsPartie - runningTime;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start(){
        
		partieEnCours = false;
	}

	void Update(){
		if (partieEnCours)
        {
			runningTime += Time.deltaTime;
			if (runningTime > tempsPartie || nbrMort == nbJoueurs - 1)
            {
				FinPartie ();
			}
		}
	}


	/*fonction LancerPartie activer quand un joueur selectionne le bouton
	 */
    public void LancerPartie()
    {
		// si le nombre de joueur n'es pas supérieur a 1 la partie s'arrete
        if (ListNomJoueur.Count > 1)
        {
			// on appelle une coroutine associé pour pas ralentir l'effet de spawn
            StartCoroutine(DeroulementSpawn());
        }
    }

	/*fonction fin partie activer quand un seul joeur est vivant ou que le temps vient de s'achever
	 */
	public void FinPartie()
	{
		//on reset les données
        partieEnCours = false;
		runningTime = 0f;
		// on demande a tout le monde de revive
		nm.maintenantEclaterVous (false);
		nbrMort = 0;
	}

	/*Enumerator du lancement de la partie
	 */
	IEnumerator DeroulementSpawn(){

		//on parcours la liste de nom
		foreach (string st in ListNomJoueur) {
			Debug.Log (st);
			//si c'est nous on respawn et on garde a jour notre position sinon on envoie une requette rpc pour lancer le respawn du joueurs
			if (st == "ObjetDeplacement") {
				GameObject.Find ("ObjetDeplacement").GetComponent<fighter> ().positionLancementPartie =	GameObject.Find ("ObjetDeplacement").transform.position;
				GameObject.Find("ObjetDeplacement").GetComponent<fighter>().respawn();
			} else {
				//le nom contenu s'ecris "joueur_nomjoueur" on enleve donc la premiere partie
				string tmp = st.Substring (8);
				nm.lancementPartie (tmp);
			}
			// on ralenti les demandes pour eviter d'avoir des spawn au meme endroit
			yield return new WaitForSeconds (0.1f);
		}

		yield return new WaitForSeconds (0.1f);
		// petite astuce tricky permet de se definir en tant que manager de la partie c'est a dire qu'on sera les seuls capable d'appeler la fin de la
		runningTime = 0f;
		//on dit a tous le monde que la partie peux commencer
		nm.maintenantEclaterVous (true);
		yield return null;
	}

	/*Fonction appeller quand un joueur deviens combattant
	 * string nomJoueur= le nom du joueur qui a rejoins
	 * bool estJoueur=savoir si c'est nous ou un autres qui rejoint
	 */
	public void PlayerJoin(string nomJoueur,bool estJoueur)
    {
        foreach (Score score in Score.instances)
        {
            score.AddPlayer(nomJoueur);
        }

		// on augmente le nombre de joueur
        nbJoueurs++;
		//si c'est nous on ajoute ObjetDeplacement sinon on ajoute le nom de l'objet autre joueur
		if (estJoueur) {

			ListNomJoueur.Add ("ObjetDeplacement");
			nm.ajouterJoueurPartie (nomJoueur);

		} else {
			ListNomJoueur.Add ("joueur: " + nomJoueur);

		}
    }

	/*Fonction appeller quand un joueur deviens civil
	 * string nomJoueur= le nom du joueur qui a quitté
	 * bool estJoueur=savoir si c'est nous ou un autres qui quitté
	 * cf au dessus mais a l'opposé quoi
	 */
	public void PlayerLeave(string nomJoueur,bool estJoueur)
    {
        foreach (Score score in Score.instances)
        {
            score.RemovePlayer(nomJoueur);
        }

        if (estJoueur) {
			ListNomJoueur.Remove ("ObjetDeplacement");
			nm.retirerJoueurPartie (nomJoueur);
		} else {
			ListNomJoueur.Remove ("joueur: " + nomJoueur);
		}
    }

	/*Fonction donnant l'information qu'un joueur à été touché aux score
	 * string shooter=personne ayant tiré
	 * string victim=personne ayant été touché
	 * bool estJoueur= si c'est nous qui avont été touché et du coup qui envois l'info
	 */
    public void PlayerHit(string shooter, string victim, bool estJoueur)
    {
        // on modifie le score
        foreach (Score score in Score.instances)
        {
            score.ChangeScore(shooter, victim);
        }
		// si on est la victime on l'annonce aux autres
        if (estJoueur)
        {
            nm.toucheJoueur(shooter, victim);
        }
    }

	/*fonction de remise à zero de gameManager et du score associe
	 * 
	 */
    public void ResetAll()
    {
        nbJoueurs = 0;

        runningTime = 0f;
        foreach (Score score in Score.instances)
        {
            score.ResetAll();
        }
    }

	/*Fonction appele par fighter pour avoir sa nouvelle position
	 */
	public Vector3 getSpawn(){
		ListJoueur = new List<GameObject> ();
		// on parcours la liste des joeurs pour creer les différent gameobject en jeu
		foreach (string nom in ListNomJoueur) {
			if (nom != "ObjetDeplacement") {
				ListJoueur.Add (GameObject.Find (nom));
			}
		}
		// on initialise une distance max qu'on veux evaluer
		float maxDist = 0;
		// variable de stockage de retour
		Vector3 vec = Vector3.zero;
		// on parcours la liste des spawn
		foreach(Transform spawn in ListSpawn){
			// on initialise une distance min
			float distMin = Mathf.Infinity;
			//on reparcours les joueurs
			foreach (GameObject joueur in ListJoueur) {
				float dist = Vector3.Distance (spawn.position, joueur.transform.position);
				//on change de distance min si la position des joueurs et du spawn est plus faible
				if ( dist < distMin) {
					distMin = dist;
				}
			}
			// on obtient donc la plus petite distance entre les joueur et un spawn, si cette distance est plus grande que notre max actuel 
			//ce spawn devient notre spawn et ainsi de suite
			if (distMin > maxDist) {
				maxDist = distMin;
				vec = spawn.position;
			}

		}
		return vec;
	}

	/*Fonction d'initialisation des spawn
	 */
	public void getListSpawn(){
		//Les spawn sont stocké dans un game object (on peut en rajouter depusi la scene voulu)
		GameObject conteneurSpawn = GameObject.Find ("SpawnPoint");
		ListSpawn = new List<Transform> ();
		//on ajoute tout les game objet de notre spawn conteneur
		foreach (Transform t in conteneurSpawn.transform) {
			ListSpawn.Add (t);
		}
	}

	/*Fonction appelé pour ajouter un mort apres qu'un joueur est dit qu'il est mort aux autres
	 */
	public void ajouterMort(){
		// on ne rajoute que dans le cas ou on est le manager de la partie c'est a dire si on est en train de calculer le temps
		if (runningTime > -1) {
			nbrMort += 1;
		}
	}

}
