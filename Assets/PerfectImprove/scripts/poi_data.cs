﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.UI;

[Serializable]
public class poi_addr 
{
    public string Address = "";
    protected piserver _pis = null;

    public poi_addr()
    {
        _pis = piserver.Instance;
    } 
}


[Serializable]
public class poi_float : poi_addr {

    public float Value
    {
        get 
        { 
            return _pis.GetTagFloat(Address);
        }
    }
}

[Serializable]
public class poi_string : poi_addr {

    public string Value
    {
        get 
        { 
            return _pis.GetTagString(Address);
        }
    }
}

[Serializable]
public class poi_date : poi_addr {

    public DateTime Value
    {
        get 
        { 
            return _pis.GetTagDate(Address);
        }
    }
}  

[Serializable]
public class poi_offset{
    public poi_float OffestValue = new poi_float();
    public poi_float OffestThreshold = new poi_float();
    public poi_float OffestTarget = new poi_float();
}

[Serializable]
public class poi_tag{
    public poi_string Lib = new poi_string();
    public poi_float Threshold  = new poi_float();
    public poi_float Target  = new poi_float();

    public poi_date Date = new poi_date();
    public poi_float Value  = new poi_float();

    public poi_offset LastValueOffset = new poi_offset();

    // Getter / Setter
    public bool Error
    {
        get 
        {
            bool bErr = (Value.Value < Threshold.Value);

            return bErr;
        }
    }

    public string ErrorMessage
    {
        get 
        {
            if(  Value.Value < Threshold.Value )
                return Lib.Value + ": " + "Valeur basse";
            return "";
        }
    }

    public float ValueScaleCarre
    {
        get 
        {
            float sca = 0;

            if( Target.Value > 0 )
                sca = Value.Value / Target.Value;
            else
                sca = Value.Value / 100;
                

            if (sca < 0.5f)
                sca = 0.5f;
            else if (sca > 1f)
                sca = 1f;

            return sca;
        }
    }

    public float ValueScaleJauge
    {
        get 
        {
            //-520 a 0
            // 0 -> -520
            // 100 (ou target) -> 0

            float sca = 0;
 
            if( Target.Value > 0 )
                sca = Value.Value * 520 / Target.Value;
            else
                sca = Value.Value * 520 / 100;

            if (sca < 0)
                sca = 0;
            else if (sca > 520)
                sca = 520;

            return sca - 520;
        }
    }

    public float LastValueRotation
    {
        get 
        {
            float rot = 0; // va de -90 a + 90

            //if( Target.Value <= 0 )
                rot = (LastValueOffset.OffestValue.Value + 100) * 180 / 200;
            //else
            //    rot = (LastValueOffset.OffestValue.Value + Target.Value) * 180 / (2 * Target.Value);

            if (rot < 0)
                rot = 0;
            else if (rot > 180)
                rot = 180;
            
            return rot - 90;
        }
    }

}
 
[Serializable]
public class poi_info {

    public poi_tag DO = new poi_tag();
    public poi_tag TP = new poi_tag();
    public poi_tag TQ = new poi_tag();
    public poi_tag TRS = new poi_tag();
    public poi_tag GoodUnitsNumber = new poi_tag();
    public poi_tag LineRun = new poi_tag();
    public poi_tag LineDefaut = new poi_tag();

    // Getter / Setter
    public bool Error
    {
        get 
        {
            bool bErr = (DO.Error || TP.Error || TQ.Error || TRS.Error || GoodUnitsNumber.Error );

            return bErr;
        }
    }

    public string ErrorMessage
    {
        get 
        {
            if (DO.Error)
                return DO.ErrorMessage;
            
            else if (TP.Error)
                return TP.ErrorMessage;
            
            else if (TQ.Error)
                return TQ.ErrorMessage;
            
            else if (TRS.Error)
                return TRS.ErrorMessage;
            
            else if (GoodUnitsNumber.Error)
                return GoodUnitsNumber.ErrorMessage;
            else
                return "";
        }
    }
}

[Serializable]
public class poi_stat {
    public string Name = "";
    public string CodeStat = "";
    public string CodeSample = ""; 
    public string CodeGraph = "";

    public poi_stat( string name )
    {
        Name = name;
    }
}

[Serializable]
public class poi_spawn
{
    public Vector3 CamPosOrig = new Vector3(0, 0, 0);
    public Vector3 CamRotaOrig = new Vector3(0, 0, 0);

    public poi_spawn(Vector3 camPos, Vector3 camRota )
    {
        CamPosOrig = new Vector3(camPos.x, camPos.y, camPos.z);
        CamRotaOrig = new Vector3(camRota.x, camRota.y, camRota.z);
    }
}

// ATTENTION : Surtout ne pas rendre Serializable cette class car sinon essaye de la charger avant
// que les init necessaires ont été fait
//[Serializable]
public class poi_config {
    
    private string _goName = "";
    private string _scName = "";

    // Variable du fichier de Config
    public string Name = "";
    public poi_info RealTime = new poi_info();
    public poi_info DayTime = new poi_info();
    public List<poi_stat> ListStats = new List<poi_stat>(); 
    public poi_spawn Spawn = new poi_spawn(new Vector3(0, 0, 0), new Vector3(0, 0, 0));


    // Constructor
    public poi_config( string scName, string goName, Vector3 camPos, Vector3 camRota ) 
    {
        _scName = scName;
        _goName = goName;
        Name = goName;
        Spawn = new poi_spawn(camPos, camRota);


        for (int i = 1; i < 10; i++)
        {
            ListStats.Add( new poi_stat( "STAT"+i.ToString() ) );
        }
    }

    // Getter / Setter
    public bool Error
    {
        get 
        {
            bool bErr = (RealTime.Error || DayTime.Error || (RealTime.LineDefaut.Value.Value != 0) );

            if (DayTime.GoodUnitsNumber.Value.Value <= 0) // Si Pas de piece on Mets un pouce vert (c'est la ligne ne va pas fonctionner)
                bErr = false;

            return bErr;
        }
    }

    public string ErrorMessage
    {
        get 
        {
            if (DayTime.GoodUnitsNumber.Value.Value <= 0)  // Si Pas de piece on Mets un pouce vert (c'est la ligne ne va pas fonctionner)
                return "";
            
            else if (RealTime.LineDefaut.Value.Value != 0)
                return "Ligne en défaut";
            
            else if (RealTime.Error)
                return RealTime.ErrorMessage;
            
            else if (DayTime.Error)
                return DayTime.ErrorMessage;
            
            else
                return "";
        }
    }
        


    // Methodes
    public void LoadConfig()
    {
        if( (_goName != null) && (_goName.Length > 0) )
        {
            try
            {
                string strJson = null;

                strJson = File.ReadAllText(Application.persistentDataPath + "/pidata/" + _scName + "_" + _goName + ".json");

                JsonUtility.FromJsonOverwrite(strJson, this);
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }
        }

    }

    public void SaveConfig(Vector3 camPos, Vector3 camRota)
    {
        #if UNITY_EDITOR
        if( (_goName != null) && (_goName.Length > 0) )
        {
            try
            {
                string strJson = null;

                if( (Spawn != null)  && 
                    ((camPos.x  != 0) || (camPos.y  != 0) || (camPos.z  != 0)) &&
                    ((camRota.x != 0) || (camRota.y != 0) || (camRota.z != 0)) )
                {
                    Spawn.CamPosOrig = camPos;
                    Spawn.CamRotaOrig = camRota;
                }

                strJson = JsonUtility.ToJson(this, true);

                //File.WriteAllText(Application.persistentDataPath + "/pidata/" + _scName + "_" + _goName + ".json", strJson);
                File.WriteAllText(Application.streamingAssetsPath + "/pidata/" + _scName + "_" + _goName + ".json", strJson);
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }
        }
        #endif

    }

    public void SaveConfig()
    {
        SaveConfig(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
    }
}
  

public class poi_data : MonoBehaviour, IComparable {

    public poi_config TagsConfig = null;
    public bool TagsConfigInitOK = false;
    public fullscreen poi_fullscreen = null;
    public statscreen poi_fullscreen_stat = null;

    public Color colorGoodValue;
    public Color colorWarnValue;
    public Color colorBadValue;
    public Texture2D iconGoodValue;
    public Texture2D iconBadValue;
    private PIServerTask taskStat = null;
    private string currentStatCode = "";
    public DateTime dateDebStat = DateTime.MinValue;
    public DateTime dateFinStat = DateTime.MinValue;

    private float _timeLoopMajValConfig = 1f;
     


    Text _namemachine = null;
    Text _statdate = null;
    Image _topbar = null;
    Text _warndescription = null;
    Text _numberunits = null;
    Image _linerun = null;
    Image _linedefaut = null;
    Text _txtdatedebut = null;
    Text _txtdatefin = null;
    Image _inicon = null;
    Image _jauge = null;
    RectTransform _jaugeRect = null;
    Image _intopbarRT = null;
    Image _intopbarDT = null;
    Text _panelhour = null;
    Image _imgStat = null;

    GameObject _warningicon = null;

    private class BtnStat{
        public GameObject goBtnStat = null;
        public Text txtStat;
        public stat_button btnStat = null;
        public Image imgStat = null;

        public BtnStat( GameObject go )
        {
            goBtnStat = go;
            txtStat = go.FindInChildren("Text").GetComponent<Text>();
            btnStat = go.GetComponent<stat_button>();
            imgStat = go.GetComponent<Image>();
        }
    }

    private class StatElem{
        public GameObject goRT = null;
        public GameObject goDT = null;
        public Text itemnameRT;
        public Text percentageRT;
        public Image percentpanelRT;
        public Image tendanceRT;

        public Text objectifpercentageDT;
        public Text percentageDT;
        public Image percentpanelDT;
        public Image tendanceDT;

        public StatElem( GameObject goST_RT, GameObject goST_DT )
        {
            goRT = goST_RT;
            itemnameRT = goST_RT.FindInChildren("itemname").GetComponent<Text>();
            percentageRT = goST_RT.FindInChildren("percentage").GetComponent<Text>();
            percentpanelRT = goST_RT.FindInChildren("percentpanel").GetComponent<Image>();
            tendanceRT = goST_RT.FindInChildren("tendance").GetComponent<Image>();

            goDT = goST_DT;
            objectifpercentageDT = goST_DT.FindInChildren("objectifpercentage").GetComponent<Text>();
            percentageDT = goST_DT.FindInChildren("percentage").GetComponent<Text>();
            percentpanelDT = goST_DT.FindInChildren("percentpanel").GetComponent<Image>();
            tendanceDT = goST_DT.FindInChildren("tendance").GetComponent<Image>();
        }
    }


    List<BtnStat> _lstBtnStat = null;
    List<StatElem> _lstStatElem = null;

    void Init()
    {
        GameObject cam = GameObject.Find ("3D Camera");

        poi_fullscreen = gameObject.FindInChildren("CanvasFullscreen").FindInChildren("fullscreen").GetComponent<fullscreen>();
        poi_fullscreen_stat = gameObject.FindInChildren("CanvasFullscreen").FindInChildren("fullscreen_stat").GetComponent<statscreen>();

        GameObject goWIN_Stat = gameObject.FindInChildren("fullscreen_stat");
        GameObject goWIN_Etat = gameObject.FindInChildren("fullscreen_tempsreel");

        GameObject goImg_Stat = goWIN_Stat.FindInChildren("imgstat");
        GameObject goPOI = gameObject.FindInChildren("poi_window");

        GameObject goWIN_RT = goWIN_Etat.FindInChildren("window_lasthour");
        GameObject goWIN_DT = goWIN_Etat.FindInChildren("window_day");

         _imgStat = goImg_Stat.GetComponent<Image>();

        _namemachine = goWIN_Etat.FindInChildren("namemachine").GetComponent<Text>();
        _statdate = goWIN_Etat.FindInChildren("statdate").GetComponent<Text>();

        _topbar = goWIN_Etat.FindInChildren("topbar").GetComponent<Image>();
        _warningicon = goWIN_Etat.FindInChildren("warningicon");

        _warndescription = goWIN_Etat.FindInChildren("warndescription").GetComponent<Text>();

        _numberunits = goWIN_Etat.FindInChildren("numberunits").GetComponent<Text>(); 

        _linerun = goWIN_Etat.FindInChildren("linerun").GetComponent<Image>();

        _linedefaut = goWIN_Etat.FindInChildren("linedefaut").GetComponent<Image>();

        _txtdatedebut = goWIN_Stat.FindInChildren("txtdatedebut").GetComponent<Text>();
        _txtdatefin = goWIN_Stat.FindInChildren("txtdatefin").GetComponent<Text>();


        _inicon = goPOI.FindInChildren("inicon").GetComponent<Image>();

        _jaugeRect = goPOI.FindInChildren("jauge").GetComponent<RectTransform>();
        _jauge = goPOI.FindInChildren("jauge").GetComponent<Image>();


        _intopbarRT = goWIN_RT.FindInChildren("intopbar").GetComponent<Image>();
        _intopbarDT = goWIN_DT.FindInChildren("intopbar").GetComponent<Image>();

        _panelhour = goWIN_RT.FindInChildren("panelhour").GetComponent<Text>();


        _lstBtnStat = new List<BtnStat>();
        for (int i = 0; i < 5; i++)
        {
            //GameObject go = goWIN_Stat.FindInChildren("bttn_btn" + i+gameObject.name);
            GameObject go = goWIN_Stat.FindInChildren("bttn_btn" + i);

            if (go != null)
            {
                _lstBtnStat.Add(new BtnStat(go));
            }

        }

        _lstStatElem = new List<StatElem>();
        for (int i = 0; i < 4; i++)
        {
            GameObject goST_RT = goWIN_RT.FindInChildren("statelement" + i);
            GameObject goST_DT = goWIN_DT.FindInChildren("statelement" + i);

            if ((goST_RT != null) && (goST_DT != null))
            {
                _lstStatElem.Add(new StatElem(goST_RT, goST_DT));
            }

        }

        if (cam != null)
            TagsConfig = new poi_config( common.CurrentScene(), gameObject.name, cam.transform.localPosition, cam.transform.localEulerAngles );
        else
            TagsConfig = new poi_config( common.CurrentScene(), gameObject.name, new Vector3(0, 0, 0), new Vector3(0, 0, 0) );
      

        if (TagsConfig != null)
        {
            TagsConfig.LoadConfig();
            TagsConfig.SaveConfig();
            //TagsConfig.SaveConfig(cam.transform.localPosition, cam.transform.localEulerAngles);
        }  

        TagsConfigInitOK = true;
    }

	// Use this for initialization
    IEnumerator Start () {

        TagsConfigInitOK = false;

        while (!downloadmanager.AllDownloaded)
            yield return null;

        while (!piserver.Ready)
            yield return null;
        
        Init();

        StartCoroutine(MajValConfig());
	}
	
    // Update is called once per frame
    void Update () {
        
        //StartCoroutine(MajValConfig());

        MajTaskStat();
    }
        
	IEnumerator MajValConfig () {

        bool bFirstInit = true;
		
        // Infinite loop executed every "frenquency" secondes.
        while( true )
        {
            if (TagsConfig != null)
            {    
				
                Texture2D tex = null;
              
                _namemachine.text = TagsConfig.Name;

                if( bFirstInit )
                    _statdate.text = showdate.FormatDate( DateTime.Now, true, false, false, false ); // Mis a jour plus bas avec la date réel de la stat

                _topbar.color = (TagsConfig.Error ? colorBadValue : colorGoodValue);
                _warningicon.SetActive( TagsConfig.Error );
                _warndescription.text = TagsConfig.ErrorMessage;

                _numberunits.text = TagsConfig.DayTime.GoodUnitsNumber.Value.Value.ToString();

                _linerun.color = (TagsConfig.RealTime.LineRun.Value.Value != 0 ? colorGoodValue : Color.white);
                _linedefaut.color = (TagsConfig.RealTime.LineDefaut.Value.Value != 0 ? colorBadValue : Color.white);
			

                // Mise a jour des date de selection
                if( ((dateDebStat == DateTime.MinValue) ||  (dateDebStat == DateTime.MinValue)) && 
                     (TagsConfig.DayTime.TRS.Date.Value > DateTime.MinValue) )
                {
                    DateTime date = TagsConfig.DayTime.TRS.Date.Value;
                    dateDebStat = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00, 000);
                    dateFinStat = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
                }
                _txtdatedebut.text = dateDebStat.ToString( "dd/MM/yyyy" );
                _txtdatefin.text = dateFinStat.ToString( "dd/MM/yyyy" );

                // Mise a jour text bouton des stats
                for (int i = 0, j=0; i < _lstBtnStat.Count; i++)
                {
					
                    BtnStat btn = _lstBtnStat[i];

                    while( j < TagsConfig.ListStats.Count )
                    {
                        if (TagsConfig.ListStats[j].CodeStat.Length > 0)
                            break;
                        else
                            j++;
                    }
                    if (j < TagsConfig.ListStats.Count)
                    {
                        btn.goBtnStat.SetActive( true );
                        btn.txtStat.text = TagsConfig.ListStats[j].Name;
                        btn.btnStat.stat = TagsConfig.ListStats[j];

                        if( TagsConfig.ListStats[j].CodeStat + TagsConfig.ListStats[j].CodeGraph == currentStatCode )
                            btn.imgStat.color =  colorGoodValue;
                        else
                            btn.imgStat.color =  Color.white;
                            
                        j++;
                    }
                    else
                    {
                        btn.goBtnStat.SetActive( false );
                    }
                }

                // Mise a jour pointeur
                tex = (TagsConfig.Error ? iconBadValue : iconGoodValue);
                _inicon.sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 ());
                _inicon.color = (TagsConfig.Error ? colorBadValue : colorGoodValue);

                //-520 a 0
                _jaugeRect.anchoredPosition3D = new Vector3( 0, TagsConfig.DayTime.TRS.ValueScaleJauge, 0 );
                _jauge.color = (TagsConfig.DayTime.TRS.Error ? colorBadValue : colorGoodValue);

                // Traitement des Temps Reel
                _intopbarRT.color = (TagsConfig.RealTime.Error ? colorBadValue : colorGoodValue);

                // Traitement des valeurs jours
                _intopbarDT.color = (TagsConfig.DayTime.Error ? colorBadValue : colorGoodValue);

                for (int i = 0; i < _lstStatElem.Count; i++)
                {
                    poi_tag tag_rt;
                    poi_tag tag_dt;

                    StatElem elm = _lstStatElem[i];

                    yield return null;

                    switch (i)
                    {
                        case 0:
                        default:
                            tag_rt = TagsConfig.RealTime.DO;
                            tag_dt = TagsConfig.DayTime.DO;
                            break;

                        case 1:
                            tag_rt = TagsConfig.RealTime.TP;
                            tag_dt = TagsConfig.DayTime.TP;
                            break;

                        case 2:
                            tag_rt = TagsConfig.RealTime.TQ;
                            tag_dt = TagsConfig.DayTime.TQ;
                            break;

                        case 3:
                            tag_rt = TagsConfig.RealTime.TRS;
                            tag_dt = TagsConfig.DayTime.TRS;
                            break;
                    }

                    // Traitement des Temps Reel
                    if ((tag_rt != null) && (tag_dt != null))
                    {
                        if( tag_rt.Date.Value > DateTime.MinValue )
                            _panelhour.text = showdate.FormatDate( tag_rt.Date.Value, false, false, false, true );

                       // elm.itemnameRT.text = tag_rt.Lib.Value;
						elm.itemnameRT.GetComponent<showtext>().mykey = tag_rt.Lib.Value;
                        elm.percentageRT.text = tag_rt.Value.Value + "%";

                        if (tag_rt.Error)
                        {
                            elm.percentpanelRT.color = colorBadValue;
                            elm.percentageRT.color = Color.white;

                            if( tag_rt.LastValueRotation >= 0 )
                                elm.tendanceRT.color = colorGoodValue;
                            else
                                elm.tendanceRT.color = Color.white;
                        }
                        else if (tag_rt.Value.Value < tag_rt.Target.Value)
                        {
                            elm.percentpanelRT.color = colorGoodValue;                               
                            elm.percentageRT.color = Color.white;

                            if( tag_rt.LastValueRotation >= 0 )
                                elm.tendanceRT.color = Color.white;
                            else
                                elm.tendanceRT.color = colorBadValue;
                        }
                        else
                        {
                            elm.percentpanelRT.color = Color.white;
                            elm.percentageRT.color = colorGoodValue;

                            if( tag_rt.LastValueRotation >= 0 )
                                elm.tendanceRT.color = colorGoodValue;
                            else
                                elm.tendanceRT.color = colorBadValue;
                        }
                          
                        elm.percentpanelRT.transform.localScale = new Vector3(tag_rt.ValueScaleCarre, tag_rt.ValueScaleCarre, 0);
                        elm.tendanceRT.transform.localEulerAngles = new Vector3(0, 0, tag_rt.LastValueRotation);
                    }

                    // Traitement des valeurs jours
                    if(tag_dt != null)
                    {
                        if (tag_dt.Date.Value > DateTime.MinValue)
                        {
                            _statdate.text = showdate.FormatDate( tag_dt.Date.Value, true, false, false, false );
                        }

                        elm.percentageDT.text = tag_dt.Value.Value + "%";
                        elm.objectifpercentageDT.text = tag_dt.Target.Value + "%";

                        if (tag_dt.Error)
                        {
                            elm.percentpanelDT.color = colorBadValue;
                            elm.percentageDT.color = Color.white;

                            if( tag_dt.LastValueRotation >= 0 )
                                elm.tendanceDT.color = colorGoodValue;
                            else
                                elm.tendanceDT.color = Color.white;
                        }
                        else if (tag_dt.Value.Value < tag_dt.Target.Value)
                        {
                            elm.percentpanelDT.color = colorGoodValue;
                            elm.percentageDT.color = Color.white;

                            if( tag_dt.LastValueRotation >= 0 )
                                elm.tendanceDT.color = Color.white;
                            else
                                elm.tendanceDT.color = colorBadValue;
                        }
                        else
                        {
                            elm.percentpanelDT.color = Color.white;   
                            elm.percentageDT.color = colorGoodValue;

                            if( tag_dt.LastValueRotation >= 0 )
                                elm.tendanceDT.color = colorGoodValue;
                            else
                                elm.tendanceDT.color = colorBadValue;
                        }

                        elm.percentpanelDT.transform.localScale = new Vector3(tag_dt.ValueScaleCarre, tag_dt.ValueScaleCarre, 0);
                        elm.tendanceDT.transform.localEulerAngles = new Vector3(0, 0, tag_dt.LastValueRotation);

                    }

                }
            }

            yield return new WaitForSeconds( _timeLoopMajValConfig );

            bFirstInit = false;
        }

	}


    void MajTaskStat()
    {
        if( (taskStat != null ) && (taskStat.IsFinished) )
        {

            try
            {
                Texture2D tex = new Texture2D( taskStat.Width, taskStat.Height);

                if (taskStat.Image != null)
                {
                    tex.LoadImage(taskStat.Image);
                }

                Image picture = _imgStat;

                if( picture != null )
                    picture.sprite = Sprite.Create (tex, new Rect (0, 0, taskStat.Width, taskStat.Height), new Vector2 ());

                currentStatCode = taskStat.Codes[0] + taskStat.Codes[2];

                for (int i = 0, j=0; i < _lstBtnStat.Count; i++)
                {
                    BtnStat btn = _lstBtnStat[i];

                    while( j < TagsConfig.ListStats.Count )
                    {
                        if (TagsConfig.ListStats[j].CodeStat.Length > 0)
                            break;
                        else
                            j++;
                    }
                    if (j < TagsConfig.ListStats.Count)
                    {
                        if( TagsConfig.ListStats[j].CodeStat + TagsConfig.ListStats[j].CodeGraph == currentStatCode )
                            btn.imgStat.color =  colorGoodValue;
                        else
                            btn.imgStat.color =  Color.white;

                        j++;
                    }
                }
            }
            catch( Exception ex )
            {
                logger.LogException(ex.Message);
            }


            taskStat = null;
        }
    }


    public void StartTaskStat(poi_stat stat) {

        if (stat != null)
        {
            Image picture = _imgStat;
            string codeSample = stat.CodeSample;


            if (picture != null)
            {

                if (taskStat != null)
                    taskStat.IsCanceled = true;

                int width = (int)picture.rectTransform.rect.width * 2;   // x2 pour avoir une meilleur resolution
                int height = (int)picture.rectTransform.rect.height * 2; // x2 pour avoir une meilleur resolution

                // Mise a jour des date de selection
                if( ((dateDebStat == DateTime.MinValue) ||  (dateDebStat == DateTime.MinValue)) && 
                    (TagsConfig.DayTime.TRS.Date.Value > DateTime.MinValue) )
                {
                    DateTime date = TagsConfig.DayTime.TRS.Date.Value;
                    dateDebStat = new DateTime(date.Year, date.Month, date.Day, 00, 00, 00, 000);
                    dateFinStat = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
                }

                if (stat.CodeSample.Length > 0)
                {
                    // si un code sample est indiqué dans le fichier de config c'est le code sample minimum et par defaut de la stat
                    // cependant suivant la plage de date selectionnée on adpate le type de données a prendre 
                    double nbJ = (dateFinStat - dateDebStat).TotalDays; 
                    string codeStatPrefered = "";

                    if (nbJ <= 1)
                        codeStatPrefered = "0"; // Sample
                    
                    else if (nbJ <= 7)
                        codeStatPrefered = "1"; // Heure
                    
                    else if (nbJ <= 6*30)
                        codeStatPrefered = "2"; // Jour

                    else if (nbJ <= 2*365)
                        codeStatPrefered = "3"; // Mois
                    
                    else
                        codeStatPrefered = "4"; // Année


                    if (codeStatPrefered.CompareTo(codeSample) > 0)
                        codeSample = codeStatPrefered;
                    
                        
                }

                taskStat = piserver.StatRequest(stat.CodeStat, codeSample, stat.CodeGraph, dateDebStat, dateFinStat, width, height);
            }
        }
    }

    public void StartTaskStat() {
        poi_stat stat = null;

        // Recherche la 1er Stat valide
        for( int i = 0; i < TagsConfig.ListStats.Count; i++ )
        {
            if (TagsConfig.ListStats[i].CodeStat.Length > 0)
            {
                stat = TagsConfig.ListStats[i];
                break;
            }
        }

        if (stat != null)
            StartTaskStat(stat);
    }


    /*
    public int Compare(object x, object y)  
    {
        poi_data poi_x = (poi_data)x;
        poi_data poi_y = (poi_data)y;
        
        return poi_x.TagsConfig.Name.CompareTo( poi_y.TagsConfig.Name );
    }
    */

    public int CompareTo(object y)  
    {
        poi_data poi_x = this;
        poi_data poi_y = (poi_data)y;

        //return poi_x.TagsConfig.Name.CompareTo( poi_y.TagsConfig.Name );
        return poi_x.name.CompareTo( poi_y.name );
    }

    public static poi_data[] GetAllDevice()
    {
        poi_data[] alldevices  = GameObject.FindObjectsOfType<poi_data>();

        if( alldevices != null )
        {
            Array.Sort(alldevices);
        }

        return alldevices;
    }

}
