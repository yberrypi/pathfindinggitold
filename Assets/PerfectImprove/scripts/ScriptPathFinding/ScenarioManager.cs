﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioManager : MonoBehaviour {
	public List<Operateurs> lScenario;
	public static int numCurrentScen;
	public static bool ScenarioEncours;
	public float tempsChangement;
    public float tempsStatEtLogo;
	public float tempsLogo;
	float runningTime;
	bool stat;
	public TuileGenerator tg;
	static bool launchFirst;
	public ScenarioAffichageStat EcranScenario; 
	public EcranOperateurs EcranOperateur;
	public static bool OPvisible;
	public Text ChronoEcran1;
	public Dropdown dropScen;

	// Use this for initialization
	void Start () {
		stat = false;
		runningTime = 0f;
		EcranScenario.destroyData ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ChronoEcran1.enabled) {
			ChronoEcran1.text = runningTime.ToString ("F2");
		}
		OPvisible = lScenario [numCurrentScen].OperateurVisibe;
		if (launchFirst) {
			launchFirst = false;
			launchFrstScen ();
		}
		if (ScenarioEncours) {
			runningTime += Time.deltaTime;

            float tpsChgt = tempsChangement;

			if (stat)
                tpsChgt = tempsStatEtLogo;
            
            if (runningTime > tpsChgt) {
				next ();
				runningTime = 0f;
			} else if (stat) {
                float tempsrestant = tpsChgt - runningTime;
				if (tempsrestant < tempsLogo) {
					EcranScenario.affichageLogo (tempsrestant / tempsLogo);
				}
			}
		} else {
			runningTime = 0f;
		}
	}

	void next(){

		if (numCurrentScen == lScenario.Count - 1 && !stat) {
			afficherEcranStat ();
			lScenario [numCurrentScen].gameObject.SetActive (false);
			dropScen.captionText.text = "Results";
			stat = true;
		}
		else if (stat){
			EcranScenario.ContentHorizontal.gameObject.SetActive (true);
			EcranScenario.enleverLogo ();
			closeStat ();
			stat = false;
			lScenario [numCurrentScen].gameObject.SetActive (true);
			nextScenario ();
		}
		else {
			EcranScenario.gameObject.SetActive (true);
			EcranScenario.AddScenario (lScenario [numCurrentScen]);
			EcranScenario.gameObject.SetActive (false);
			nextScenario ();
		}


	}

	void afficherEcranStat(){ 
		EcranScenario.gameObject.SetActive (true);
		EcranScenario.AddScenario (lScenario [numCurrentScen]);
	}

	void closeStat(){
		EcranScenario.destroyData ();
		EcranScenario.gameObject.SetActive (false);
	}

	void nextScenario (){
		
		
		lScenario [numCurrentScen].reset ();

		tg.reset ();
		lScenario [numCurrentScen].gameObject.SetActive(false);
		if (numCurrentScen < lScenario.Count-1) {
			numCurrentScen++;
		} else {
			numCurrentScen = 0;
			EcranScenario.destroyData ();
		}
		lScenario [numCurrentScen].gameObject.SetActive(true);
		EcranOperateur.ops = lScenario [numCurrentScen];
		EcranOperateur.CreateList ();
		dropScen.captionText.text = lScenario [numCurrentScen].name;

	}

	public static void StartSimul(){
		numCurrentScen = 0;
		launchFirst = true;
	}



	public void launchFrstScen(){
		int i = 0;

		foreach (Operateurs scen in lScenario) {
			if (i == 0) {
				scen.gameObject.SetActive (true);
				scen.reset ();
			} else {
				scen.gameObject.SetActive (false);
			}
			i++;
		}
		tg.reset ();
		EcranOperateur.ops = lScenario [numCurrentScen].GetComponent<Operateurs> ();
		EcranOperateur.CreateList ();
	}
}
