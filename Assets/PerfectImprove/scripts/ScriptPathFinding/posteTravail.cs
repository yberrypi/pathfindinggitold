﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posteTravail : MonoBehaviour {
	public bool needStock;
	public float tempsTache;
	public bool operateurDispo;
	public float tempsPasse=0;

	
	// Update is called once per frame
	void Update () {
		if (operateurDispo && !needStock) {
			tempsPasse += Time.deltaTime;
			if (tempsPasse > tempsTache) {
				tempsPasse = 0;
				needStock = true;

			} 
		}
	}
}
