﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioAffichageStat : MonoBehaviour {
	public Transform ContentHorizontal;
	public GameObject logo;
	public TuileGenerator tg;
	public ScenarioManager scm;

	public void AddScenario(Operateurs scenario){
		Transform nouv = Instantiate (Resources.Load<Transform>("ContentVerticalScenario"), ContentHorizontal);

		nouv.name = scenario.name;
		nouv.GetChild (0).GetChild (0).GetComponent<Text>().text = scenario.name;
		nouv.GetChild (0).GetChild (1).GetComponent<Text>().text = scm.tempsChangement.ToString ("F2");
        nouv.localScale = new Vector3(1, 1, 1);

		bool pair = true;
		foreach (Operateur op in scenario.lOperateur) {
			Transform nouvOperateur;
			if (pair) {
				nouvOperateur = Instantiate (Resources.Load<Transform> ("ScenarioOperateur1"), nouv);
			}
			else {
				nouvOperateur = Instantiate (Resources.Load<Transform> ("ScenarioOperateur2"), nouv);
			}
			pair = !pair;
			nouvOperateur.GetChild (0).GetComponent<Text>().text = op.nom;
			nouvOperateur.GetChild (1).GetComponent<Text>().text = op.distanceParcouru.ToString("F2");
			nouvOperateur.GetChild (2).GetComponent<Text>().text = op.pieceFaite.ToString();
            nouvOperateur.GetChild (3).GetComponent<Image>().color = op.couleurAssocie;
            nouvOperateur.localScale=new Vector3 (1, 1, 1);
		}

		Transform image = Instantiate (Resources.Load<Transform> ("ImageAffichageMapScenario"), nouv);

		image.name = scenario.name + "_imageAssocie";
		tg.GenerateImage (image.GetComponent<RawImage> ());
		image.localScale = new Vector3 (1, 1, 1);
	}

	public void destroyData(){
		foreach (Transform t in ContentHorizontal){
			Destroy (t.gameObject);
		}
	}

	public void affichageLogo(float pourcent){
		logo.SetActive (true);
		if (pourcent < 0.33f) {
			logo.GetComponent<Image> ().color = new Color (1, 1, 1, pourcent * 3);
			logo.transform.GetChild(0).GetComponent<Image> ().color = new Color (1, 1, 1, pourcent * 3);

		} else if (pourcent < 0.66f) {
			ContentHorizontal.gameObject.SetActive (false);
			logo.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
			logo.transform.GetChild(0).GetComponent<Image> ().color = new Color (1, 1, 1, 1);

		} else {
			logo.GetComponent<Image> ().color = new Color (1, 1, 1, (1 - pourcent) * 3);
			logo.transform.GetChild(0).GetComponent<Image> ().color = new Color (1, 1, 1, (1 - pourcent) * 3);

		}

	}

	public void enleverLogo(){
		logo.SetActive (false);
	}

}
