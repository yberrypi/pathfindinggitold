﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Operateurs : MonoBehaviour {
	
	public List<Operateur> lOperateur;
	public bool OperateurVisibe;

	// Use this for initialization
	void Start () {
		OperateurVisibe = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void testVisibilite(){
		OperateurVisibe = false;
		foreach(Operateur op in lOperateur){
			if (op.visible) {
				OperateurVisibe = true;
                break;
			}
		}
	}

	public void reset(){
		foreach (Operateur op in lOperateur) {
			op.reset ();
		}
	}
}
