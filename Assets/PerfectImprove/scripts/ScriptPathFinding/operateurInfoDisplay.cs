﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class operateurInfoDisplay : MonoBehaviour {
	public Operateur op;

    [Header("Sous-élements")]
    public Text nom;
    public Text distance;
    public Toggle visible;
    public Image image;
	
	// Update is called once per frame
	void Update () {
        distance.text = op.distanceParcouru.ToString ("F2");
	}

	public void CreerData(){
        nom.text = op.nom;
        distance.text = op.distanceParcouru.ToString ("F2");
        visible.isOn = op.visible;
        image.color = op.couleurAssocie;
    }

	public void UpdateData(){
		
		op.visible = visible.isOn;
	}
}
