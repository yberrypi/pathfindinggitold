﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWindows : MonoBehaviour {
	bool open=true;
	public GameObject moins;
	public GameObject plus;
	public RectTransform fond;
	public Transform PartieDroite;
	public RectTransform Logo;

	
	public void OnClick(){
        Vector3 vec = PartieDroite.localPosition;
        if (open) {
			moins.SetActive (false);
			plus.SetActive (true);
			vec.x += fond.rect.width - Logo.rect.width;
		} else {
			moins.SetActive (true);
			plus.SetActive (false);
			vec.x -= fond.rect.width - Logo.rect.width;
		}
        PartieDroite.localPosition = vec;
        open = !open;
	}
}
