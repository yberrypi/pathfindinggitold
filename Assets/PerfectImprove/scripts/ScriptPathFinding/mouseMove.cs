﻿using UnityEngine;
using System.Collections;
//change
using System.Collections.Generic;


public class mouseMove : MonoBehaviour {

	#if UNITY_STANDALONE_WIN || UNITY_EDITOR || UNITY_STANDALONE_OSX
	
	public static bool stopEditorMovement;
	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationX = 0F;
	float rotationY = 0F;

	private List<float> rotArrayX = new List<float>();
	float rotAverageX = 0F;	

	private List<float> rotArrayY = new List<float>(); 
	float rotAverageY = 0F;

	public int frameCounter = 20;

	Quaternion originalRotation;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
        {
            rb.freezeRotation = true;
        }
        originalRotation = transform.localRotation;
    }

    void Update ()
	{
		if (!stopEditorMovement) {
			if (axes == RotationAxes.MouseXAndY) {			
				rotAverageY = 0f;
				rotAverageX = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;
				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				rotArrayY.Add (rotationY);
				rotArrayX.Add (rotationX);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}

				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}

				rotAverageY /= rotArrayY.Count;
				rotAverageX /= rotArrayX.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);
				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);

				transform.localRotation = originalRotation * xQuaternion * yQuaternion;
			} else if (axes == RotationAxes.MouseX) {			
				rotAverageX = 0f;

				rotationX += Input.GetAxis ("Mouse X") * sensitivityX;

				rotArrayX.Add (rotationX);

				if (rotArrayX.Count >= frameCounter) {
					rotArrayX.RemoveAt (0);
				}
				for (int i = 0; i < rotArrayX.Count; i++) {
					rotAverageX += rotArrayX [i];
				}
				rotAverageX /= rotArrayX.Count;

				rotAverageX = ClampAngle (rotAverageX, minimumX, maximumX);

				Quaternion xQuaternion = Quaternion.AngleAxis (rotAverageX, Vector3.up);
				transform.localRotation = originalRotation * xQuaternion;			
			} else {			
				rotAverageY = 0f;

				rotationY += Input.GetAxis ("Mouse Y") * sensitivityY;

				rotArrayY.Add (rotationY);

				if (rotArrayY.Count >= frameCounter) {
					rotArrayY.RemoveAt (0);
				}
				for (int j = 0; j < rotArrayY.Count; j++) {
					rotAverageY += rotArrayY [j];
				}
				rotAverageY /= rotArrayY.Count;

				rotAverageY = ClampAngle (rotAverageY, minimumY, maximumY);

				Quaternion yQuaternion = Quaternion.AngleAxis (rotAverageY, Vector3.left);
				transform.localRotation = originalRotation * yQuaternion;
			}
		}
	}

	public static float ClampAngle (float angle, float min, float max)
	{
		angle = angle % 360;
		if ((angle >= -360F) && (angle <= 360F)) {
			if (angle < -360F) {
				angle += 360F;
			}
			if (angle > 360F) {
				angle -= 360F;
			}			
		}
		return Mathf.Clamp (angle, min, max);
	}


	/*Vector3 mousePosini;
	// Use this for initialization
	void Start () {
		mousePosini = Input.mousePosition;
	}
	
	// Update is called once per frame
	void Update () {
		//transform.position=new Vector3(transform.position.x, 0.685f,transform.position.z);
		Vector3 vec = Input.mousePosition;

		float diffx = (vec.x - mousePosini.x)*10*Time.deltaTime;
		float diffy = (vec.y - mousePosini.y)*10*Time.deltaTime;

		transform.eulerAngles = new Vector3 (transform.eulerAngles.x- diffy, transform.eulerAngles.y + diffx, transform.eulerAngles.z);

		mousePosini = vec;
		if (Input.touchCount == 1) {
			Touch t=Input.GetTouch(0);
			Vector2 vec2=t.deltaPosition;
			float diffx = (vec2.x - mousePosini.x)*10*Time.deltaTime;
			float diffy = (vec2.y - mousePosini.y)*10*Time.deltaTime;
			
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x- diffy, transform.eulerAngles.y + diffx, transform.eulerAngles.z);
		}
		else if (Input.touchCount == 2) {
			// Store both touches.
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			transform.position*=(1+(deltaMagnitudeDiff*0.015f));
	}
	}*/
	#endif

}
