﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EcranOperateurs : MonoBehaviour {
	public Operateurs ops;
	public Transform content;

	public void CreateList(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}
		bool pair= true;
		foreach (Operateur op in ops.lOperateur) {
            operateurInfoDisplay nouv;
			if (pair) {
				 nouv = Instantiate(Resources.Load<operateurInfoDisplay>("OperateurInfo1"), content);
			} else {
				 nouv = Instantiate(Resources.Load<operateurInfoDisplay>("OperateurInfo2"), content);
			}
			pair = !pair;
			nouv.name = "Info: " + op.nom;
			nouv.op=op;
			nouv.CreerData();
			nouv.transform.localScale = Vector3.one;
		}

	}
}
