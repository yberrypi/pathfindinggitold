﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Operateur : MonoBehaviour {
	public string nom;
	public int maxTuile = 0;
	public posteTravail posteAssocie;
	public List<pointAccesStock> LPA;
	pointAccesStock currentPointAcces;
	int numCurrentPA;
	NavMeshAgent navAgent;
	public Animator anim;
	public Vector3 anglePrecedent;
	public float distanceParcouru = 0f;
	Vector3 anciennePosition;
	public Text textDist;

	public bool visible;
	public Color couleurAssocie; 
	public int pieceFaite;
	public Vector3 positionDepart;
	public Quaternion rotationDepart;

	// Update is called once per frame

	void Start () {
		visible = true;
		//positionDepart = transform.position;
		rotationDepart = transform.rotation;
		anciennePosition = transform.position;
		numCurrentPA = 0;
		navAgent = GetComponent<NavMeshAgent> ();
		currentPointAcces = LPA [0];
		if (anim != null) {
			anim.Play ("HumanoidIdle");
		}
		anglePrecedent = transform.localEulerAngles;
	}

	void Update () {
		
		Vector3 nouvPos = transform.position;
		nouvPos.y = 0f;
		transform.position = nouvPos;
		distanceParcouru += Vector3.Distance (nouvPos, anciennePosition);
		anciennePosition = nouvPos;
		textDist.text = "Distance parcouru: " + distanceParcouru.ToString ("F2");
		if (anim != null) {
			float angle = anglePrecedent.y - transform.localEulerAngles.y;
			if (Mathf.Abs (angle * 10f) > 5f) {
				
					if (angle < 0f) {
						anim.Play ("HumanoidWalkRight");
					} else {
						anim.Play ("HumanoidWalkLeft");
					}
			



			} else if (navAgent.remainingDistance > 0.1f) {
				anim.Play ("HumanoidWalk");
			} else {
				anim.Play ("HumanoidIdle");
			}
			anglePrecedent = transform.localEulerAngles;
		}
		if (posteAssocie.needStock) {
			posteAssocie.needStock = false;
			posteAssocie.operateurDispo = false;
			navAgent.isStopped = false;
			navAgent.destination = LPA [0].transform.position;
			if (anim != null) {
				anim.Play ("HumanoidWalk");
			}
			pieceFaite++;
		} else {
			float dist = navAgent.remainingDistance;
			if (!posteAssocie.operateurDispo && dist != Mathf.Infinity && navAgent.pathStatus == NavMeshPathStatus.PathComplete && dist < 0.1f && Vector3.Distance(navAgent.destination,transform.position) < 2f) {
				

				if (numCurrentPA == LPA.Count) {
					
					posteAssocie.operateurDispo = true;
					numCurrentPA = 0;
					currentPointAcces = LPA [numCurrentPA];
					navAgent.isStopped = true;
					if (anim != null) {
						anim.Play ("HumanoidIdle");
					}
				} else {
					
					currentPointAcces.operateurDispo = true;
					navAgent.isStopped = true;
					if (anim != null) {
						anim.Play ("HumanoidIdle");
					}
				}

			}
			if (currentPointAcces.TacheFini) {
				next ();
			}
		}
	}

	void next(){
		numCurrentPA++;

		if (numCurrentPA == LPA.Count) {

			currentPointAcces.operateurDispo = false;
			currentPointAcces.TacheFini = false;
			navAgent.isStopped = false;
			navAgent.destination = posteAssocie.transform.position;
			if (anim != null) {
				anim.Play ("HumanoidWalk");
			}
		} else {
			
			currentPointAcces.operateurDispo = false;
			currentPointAcces.TacheFini = false;
			currentPointAcces = LPA [numCurrentPA];
			navAgent.isStopped = false;
			navAgent.destination = currentPointAcces.transform.position;
			if (anim != null) {
				anim.Play ("HumanoidWalk");
			}
		}
	}

	public void changeMax(int count){
		if (count > maxTuile) {
			maxTuile = count;
		}
	}

	public void reset(){
		
		anim.Play ("HumanoidIdle");
		pieceFaite = 0;
		distanceParcouru = 0;
		navAgent.enabled = false;


		transform.position = posteAssocie.transform.position;
		transform.rotation = rotationDepart;
		anciennePosition = transform.position;

		posteAssocie.operateurDispo = true;
		posteAssocie.needStock = false;
		posteAssocie.tempsPasse = 0f;
		numCurrentPA = 0;
		currentPointAcces= LPA [0];
		foreach (pointAccesStock pas in LPA) {
			pas.TacheFini = false;
			pas.operateurDispo = false;
			pas.tempsPasse = 0f;
		}
		navAgent.enabled = true;
		navAgent.isStopped = true;
	}
}
