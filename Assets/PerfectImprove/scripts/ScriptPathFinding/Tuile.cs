﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuile : MonoBehaviour {

	public class operateurCount{
		public Operateur op;
		public int count;

		public operateurCount(Operateur ope){
			op=ope;
			count=1;
		}

		public void reset(){
			count=0;
		}

		public void delete(){
			count=0;
			op = null;
		
		}
	}

	public Color couleurDepart;
	public Color couleurArrive;
	public int count;
	GameObject lastFocus;
	float tempsPasseApresExit;
	public bool exit=true;
	List<operateurCount> lOpc=new List<operateurCount>();
	public ScenarioManager ScenMan;
	// Use this for initialization
	void Start () {
		ColorieTuile ();
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}

	public void ColorieTuile(){
		float ratio = 0f;
		Color c = new Color ();
        Renderer renderer = GetComponent<Renderer>();

		if (ScenarioManager.OPvisible) {
			operateurCount tmp = FindMaxOpeVisible ();
			if (tmp != null) {
				GetComponent<Renderer> ().enabled = true;
				//Debug.Log (tmp.op.visible);
				if (tmp.op.maxTuile != 0) {
					ratio = (float)tmp.count / tmp.op.maxTuile;
				}
                c = (0.25f + 0.75f * ratio) * tmp.op.couleurAssocie;
                c.a = 1f;

			} else {
				c = new Color (0.35f, 0.35f, 0.35f);
                renderer.enabled = false;
			}
            renderer.material.color = c;
		} else {
			if (TuileGenerator.max > 0) {
				ratio = (float)count / TuileGenerator.max;
			}
			/*if (ratio > 0.5f) {
				c = new Color (1, 1 - (ratio-0.5f)*2, 0, 0.5f);
			} else {
				c = new Color (ratio*2, 1, 0, 0.5f);
			}

			*/

			// c = new Color (ratio, 1 - ratio, 0, 0.5f);
			//c = new Color (0.25f+0.75f*ratio,0, 0, 0.5f);
			float CalculExpo = Mathf.Log (1f + ratio * (Mathf.Exp(1f) - 1f));
            c = Color.LerpUnclamped(couleurDepart, couleurArrive, CalculExpo);
            c.a = 1f;

            renderer.material.color = c;
		}
		if (exit && lastFocus!=null) {
			tempsPasseApresExit += Time.deltaTime;
			if (tempsPasseApresExit > 0.5f) {
				lastFocus = null;
				tempsPasseApresExit = 0f;
			}

		}
	}


	void OnCollisionEnter(Collision collision)
	{
		
		tempsPasseApresExit = 0f;

		if (collision.gameObject.tag == "Operator" && collision.gameObject != lastFocus) {
			//Debug.Log (collision.gameObject.name);

			count++;
			if(count > TuileGenerator.max){
				TuileGenerator.max = count;
			}
			GetComponent<MeshRenderer> ().enabled = TuileGenerator.visible;
			lastFocus = collision.gameObject;
			Operateur op = collision.transform.parent.GetComponent<Operateur> ();
			if (op != null) {
				//Debug.Log (op.name);
				AjoutCountOp (op);
			}
			ColorieTuile ();
		}
	}

	void OnCollisionExit(Collision collision)
	{
		exit = true;
	}

	public void AjoutCountOp(Operateur p){
		 bool found = false;
		foreach (operateurCount opc in lOpc) {
			if (opc.op.name == p.name && !found) {
				opc.count += 1;
				p.changeMax (opc.count);
			}
		}

		if (!found) {
			operateurCount tmp = new operateurCount (p);
			lOpc.Add (tmp);
			p.changeMax (1);
		}

	}

	operateurCount FindMaxOpeVisible(){
		int countMax = -1;
		operateurCount opRes = null;

		foreach(operateurCount opc in lOpc){
			if (opc.op.visible && countMax < opc.count) {
				countMax = opc.count;
				opRes = opc;
			}
		}

		return opRes;
	}

	public void reset(){
		foreach (operateurCount opc in lOpc) {
			opc.reset ();
		}
		lOpc = new List<operateurCount>();
		GetComponent<MeshRenderer> ().enabled = false;
		count = 0;
		lastFocus = null;
		ColorieTuile ();
		exit = true;
	}

}
