﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TuileGenerator : MonoBehaviour {
	public static int max=0;
	public int longueur;
	public int largeur;
	public float largeurTuile;
	public Vector3 pointDeDepartLocal;
	List<GameObject> lTuile;
	public GameObject Temoin;
	public static bool visible=false;
	public RawImage imageAfichage;
	public bool ecranFull;
	public GameObject pleinEcran;
	public RawImage imagePlein;
	public Operateurs ops;
	// Use this for initialization


	//tableau

	public GameObject tableau;

	void Start () {
		visible = true;
		generateTuile ();
		visible = true;
		ecranFull = false;
		pleinEcran.SetActive (false);
		ScenarioManager.ScenarioEncours = !ScenarioManager.ScenarioEncours;

		ScenarioManager.StartSimul ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!ScenarioManager.ScenarioEncours) {
			if (Input.GetKeyDown (KeyCode.E)) {
				visible = !visible;
				afficherTuile ();
			}
			if (Input.GetKeyDown (KeyCode.R)) {
				GenerateImage (imageAfichage);
			}

			/*if (Input.GetKeyDown ("t")) {
				afficherTableau ();


			}*/
			if (Input.GetKeyDown (KeyCode.Y)) {
				ecranFull = !ecranFull;
				pleinEcran.SetActive (ecranFull);

			}
		} 
		if (Input.GetKeyDown (KeyCode.S)) {
			visible = true;
			ecranFull = false;
			pleinEcran.SetActive (false);
			ScenarioManager.ScenarioEncours = !ScenarioManager.ScenarioEncours;

			ScenarioManager.StartSimul ();


		}

		if (ecranFull) {
			GenerateImage (imagePlein);
		}
	}

	void generateTuile(){
		lTuile = new List<GameObject> ();
		for (int i = 0; i < longueur + 1; i++) {
			for (int j = 0; j < largeur + 1; j++) {
				GameObject nouv = Instantiate (Temoin);
				nouv.transform.SetParent (Temoin.transform.parent);
				nouv.name = "Tuile" + i.ToString () + j.ToString ();
				nouv.transform.localScale = new Vector3(largeurTuile, largeurTuile, largeurTuile);
				Vector3 vec = pointDeDepartLocal;
				vec.x += largeurTuile * j * 10;
				vec.z += largeurTuile * i * 10;
				nouv.transform.localPosition = vec;
				nouv.GetComponent<Renderer>().material = new Material(Shader.Find("Transparent/Diffuse"));
				nouv.SetActive (true);
				nouv.GetComponent<MeshRenderer>().enabled = false;
				lTuile.Add (nouv);

			}
		}
	}

	void afficherTuile(){
		foreach (GameObject g in lTuile) {
			if (g.GetComponent<Tuile>().count > 0){
				g.GetComponent<MeshRenderer>().enabled = visible;
			}
		}
	}

	public void GenerateImage(RawImage im){

		Texture2D tex = new Texture2D (longueur,largeur);
		for (int i = 0; i < longueur + 1; i++) {
			for (int j = 0; j < largeur + 1; j++) {
				Color c = new Color ();
			
				c = lTuile [i*(largeur+1)+j].GetComponent<Renderer> ().material.color;
				//
				c.a = 1f;
				tex.SetPixel (i, j, c);
			
			}
		}
		tex.Apply ();
		im.texture = tex;


	}

	void afficherTableau(){
		if (tableau.activeSelf) {
			ops.testVisibilite ();
			tableau.SetActive (false);
			foreach (GameObject g in lTuile) {
				g.GetComponent<Tuile> ().ColorieTuile ();
			}
		} else {
			tableau.SetActive (true);
			tableau.GetComponent<EcranOperateurs> ().CreateList ();
		}
	}

	public void reset(){
		max = 0;
		foreach (GameObject t in lTuile) {
			t.GetComponent<Tuile>().reset ();
		}
	}

	public void ChangeAspect(){
		
		ops.testVisibilite ();
		foreach (GameObject g in lTuile) {
			g.GetComponent<Tuile> ().ColorieTuile ();
		}
	}

}
