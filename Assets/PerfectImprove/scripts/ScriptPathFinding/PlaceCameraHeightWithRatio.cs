﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PlaceCameraHeightWithRatio : MonoBehaviour {
	public float width;
	public float height;

    private Camera cam;

	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
		repositionCamera ();
	}
	
	// Update is called once per frame
	void Update () {
		repositionCamera ();
	}

	void repositionCamera(){
		Vector3 vec = transform.position;
		vec.y = calculHauteur ();
		transform.position = vec;
	}

	float calculHauteur(){

        float angle = cam.fieldOfView * 0.5f * Mathf.Deg2Rad;
		//Debug.Log (((float)Screen.width) / ((float)Screen.height));
		float tanAngleH = Mathf.Tan(angle) * Screen.width / Screen.height;
		float hauteurPourVertical = height * 0.5f / Mathf.Tan (angle);
		float hauteurPourHorizontal = width * 0.5f / tanAngleH;
		//Debug.Log (hauteurPourVertical);
		//Debug.Log (hauteurPourHorizontal);
		return Mathf.Max(hauteurPourVertical, hauteurPourHorizontal);
	}
}
