﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointAccesStock : MonoBehaviour {
	public bool TacheFini;
	public float tempsTache;
	public bool operateurDispo;
	public float tempsPasse=0;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		
		if (operateurDispo) {
			tempsPasse += Time.deltaTime;
			if (tempsPasse > tempsTache) {
				tempsPasse = 0;
				TacheFini = true;

			} 
		}
	}
}
