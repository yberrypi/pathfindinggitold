﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fullscreen : MonoBehaviour
{
	public bool onscreen = false;
	bool lastonscreen = false;
	RectTransform rect;
	float		screenposition;
	Vector2		vec;
	GameObject fullscreen_tempsreel;
	GameObject fullscreen_stat;

	void Awake ()
	{
		rect = gameObject.GetComponent<RectTransform>();
		screenposition = rect.anchoredPosition.y;
		vec = new Vector2(rect.anchoredPosition.x,-600.0f);
		rect.anchoredPosition = vec;
	}
	
	void Update ()
	{
		if (onscreen && (!lastonscreen))
		{
			scroll4items.ResetScroll();
		}
		lastonscreen = onscreen;

		if (!onscreen)
		{
			if (vec.y > -600.0f)
				vec.y -= (1200.0f * Time.fixedDeltaTime);
			if (vec.y < -600.0f)
				vec.y = -600.0f;

            if (vec.y == -600.0f)
            {
                gameObject.SetActive(false);
            }
                
		}
		else
		{
			if (vec.y < screenposition)
				vec.y += (1200.0f * Time.fixedDeltaTime);
			if (vec.y > screenposition)
				vec.y = screenposition;
		}
		rect.anchoredPosition = vec;

	}
}
