﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scroll4items : MonoBehaviour
{
	public static float yscrollposition = -1000.0f;
	public static float currentyscrollposition;
	public static float time = 0.0f;
	public static int scrollstate = 0;
	RectTransform myrect;

	public static void ResetScroll()
	{
		currentyscrollposition = yscrollposition;
		time = Time.time + 5.0f;
		scrollstate = 0;

	}


	void Awake ()
	{
        enabled = false;

        myrect = transform as RectTransform;

		if (yscrollposition == -1000.0f)
		{
			yscrollposition = myrect.anchoredPosition.y;
			currentyscrollposition = yscrollposition;
		}
		if (time == 0.0f)
			time = Time.time + 5.0f;
	}


	void Update ()
	{
		switch (scrollstate)
		{
		case 0 :
			if (time < Time.time)
			{
				scrollstate = 1;
			}
			break;
		case 1 :
			currentyscrollposition += 0.5f;
			if (currentyscrollposition > yscrollposition + 70.0f)
			{
				currentyscrollposition = yscrollposition + 70.0f;
				time = Time.time + 5.0f;
				scrollstate = 2;
			}
			break;
		case 2 :
			if (time < Time.time)
			{
				scrollstate = 3;
			}
			break;
		case 3 :
			currentyscrollposition -= 0.5f;
			if (currentyscrollposition < yscrollposition)
			{
				currentyscrollposition = yscrollposition;
				time = Time.time + 5.0f;
				scrollstate = 0;
			}
			break;
		}

		myrect.anchoredPosition = new Vector2(myrect.anchoredPosition.x,currentyscrollposition);
	}
}
