﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class statscreen : MonoBehaviour
{
	public bool onscreen = false;
	bool lastonscreen = false;
	RectTransform rect;
	float		screenposition;
	Vector2		vec;
	GameObject fullscreen_tempsreel;
	GameObject fullscreen_stat;
	float positionshift = 342.0f;
	GameObject arrow = null;
//	

	void Start ()
	{
		rect = gameObject.GetComponent<RectTransform>();
		screenposition = rect.anchoredPosition.y;
		positionshift = rect.rect.height - 25;
		vec = new Vector2(rect.anchoredPosition.x,-positionshift);
		rect.anchoredPosition = vec;
		arrow = gameObject.FindInChildren("bttn_button_back").FindInChildren("icon");
	}
	
	void Update ()
	{
		if (onscreen && (!lastonscreen))
		{
		}
		lastonscreen = onscreen;

		if (!onscreen)
		{
			arrow.transform.localEulerAngles = new Vector3 (0,0,180);
			if (vec.y > screenposition-positionshift)
				vec.y -= (positionshift * Time.fixedDeltaTime * 60.0f / 30.0f);
			if (vec.y < screenposition-positionshift)
				vec.y = screenposition-positionshift;
		}
		else
		{
			arrow.transform.localEulerAngles = new Vector3 (0,0,0);
			if (vec.y < screenposition)
				vec.y += (positionshift * Time.fixedDeltaTime * 60.0f/ 30.0f);
			if (vec.y > screenposition)
				vec.y = screenposition;
		}
		rect.anchoredPosition = vec;

	}
}
