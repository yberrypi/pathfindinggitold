﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class piservertests : MonoBehaviour
{
    public Text NbPieces = null;
    public Text NbPiecesDate = null;
    public Text NumStat = null;
    public Text NumMail = null;

    private int _cptMail = 0;
    private int _cptStat = 0;
    PIServerTask taskMail = null; 
    PIServerTask taskStats = null;
    private float _timeLoop = 0;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if( (taskMail != null ) && (taskMail.IsFinished) )
        {
            Debug.Log("Fin Task Mail");

            taskMail = null;
        }

        if( (taskStats != null ) && (taskStats.IsFinished) )
        {
            Texture2D tex = new Texture2D( taskStats.Width, taskStats.Height);

            if (taskStats.Image != null)
            {
                tex.LoadImage(taskStats.Image);
            }

            Image pictureInScene = null;
            GameObject go = GameObject.Find("ImgTestPiServer");

            if( go != null )
                pictureInScene = go.GetComponent<Image>();

            if( pictureInScene != null )
                pictureInScene.sprite = Sprite.Create (tex, new Rect (0, 0, taskStats.Width, taskStats.Height), new Vector2 ());//set the Rect with p

            Debug.Log("Fin Task Stat");

            taskStats = null;
        }


        _timeLoop += Time.deltaTime;
        if( _timeLoop >= 1 ) // 1 seconde
        {
            _timeLoop = 0;

            GameObject go = GameObject.Find("Idec");
            poi_data pidata = null;

            if( go != null )
                pidata = go.GetComponent<poi_data>();

            if (pidata != null)
            {
                if (NbPieces != null)
                    NbPieces.text = pidata.TagsConfig.DayTime.GoodUnitsNumber.Value.Value.ToString();

                if (NbPiecesDate != null)
                    //NbPiecesDate.text = pidata.TagsConfig.DayTime.GoodUnitsNumber.Date.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    NbPiecesDate.text = pidata.TagsConfig.DayTime.GoodUnitsNumber.Date.Value.ToString("dd/MM/yyyy");
                
            }
        }
            
        if (NumStat != null)
            NumStat.text = _cptStat.ToString();

        if (NumMail != null)
            NumMail.text = _cptMail.ToString();
	}


    public void TestPIServer()
    {
        DateTime dateQuery = DateTime.Now;
        GameObject go = GameObject.Find("Idec");
        poi_data pidata = null;

        if( go != null )
            pidata = go.GetComponent<poi_data>();

        if (pidata != null)
        {
            dateQuery = pidata.TagsConfig.DayTime.GoodUnitsNumber.Date.Value;
        }



        if (taskMail == null)
        {
            _cptMail++;
            taskMail = piserver.MailMessage("piservertests", "TestPIServer", "Ceci est un test à partir d'android\r\néè@à\r\nMail N°" + _cptMail);
        }

        if (taskStats != null)
            taskStats.IsCanceled = true;
     
        switch (_cptStat)
        {
            case 1:
                taskStats = piserver.StatRequest("Idec_PiecesMauvaises", "2", "1", dateQuery, dateQuery, 640, 480);
                break;
               
            case 2:
                taskStats = piserver.StatRequest("Idec_Tendance", "1", "1", dateQuery, dateQuery, 640, 480);
                break;

            case 3:
                taskStats = piserver.StatRequest("Idec_Tendance", "1", "4", dateQuery, dateQuery, 640, 480);
                break;

            default:
                taskStats = piserver.StatRequest("Idec_PiecesMauvaises", "2", "3", dateQuery, dateQuery, 640, 480);
                _cptStat = 0;
                break;
        }
        _cptStat++;
    }
}
