﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System.IO;

public class json_parser : MonoBehaviour
{
	static fullscreen fulls = null;

	string jsondata = "[" +
		"{" +
		"\"machinename\":\"cirmeca\"," +
		"\"dayvalues\":" +
		"{" +
		"\"trs\":20," +
		"\"cadence\":10," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":20," +
		"\"cadence_tendance\":-10," +
		"\"rendement\":30," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":40," +
		"\"rendement_tendance\":10," +
		"\"quality\":50," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":60," +
		"\"quality_tendance\":-20" +
		"}," +
		"\"lasthour\":" +
		"{" +
		"\"trs\":30," +
		"\"cadence\":11," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":21," +
		"\"cadence_tendance\":20," +
		"\"rendement\":31," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":41," +
		"\"rendement_tendance\":-30," +
		"\"quality\":51," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":61," +
		"\"quality_tendance\":30" +
		"}" +
		"}," +
		"{" +
		"\"machinename\":\"robot\"," +
		"\"dayvalues\":" +
		"{" +
		"\"trs\":40," +
		"\"cadence\":12," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":22," +
		"\"cadence_tendance\":-40," +
		"\"rendement\":32," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":42," +
		"\"rendement_tendance\":40," +
		"\"quality\":52," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":62," +
		"\"quality_tendance\":-50" +
		"}," +
		"\"lasthour\":" +
		"{" +
		"\"trs\":50," +
		"\"cadence\":13," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":23," +
		"\"cadence_tendance\":50," +
		"\"rendement\":33," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":43," +
		"\"rendement_tendance\":-60," +
		"\"quality\":53," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":63," +
		"\"quality_tendance\":60" +
		"}" +
		"}," +
		"{" +
		"\"machinename\":\"idec\"," +
		"\"dayvalues\":" +
		"{" +
		"\"trs\":60," +
		"\"cadence\":14," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":24," +
		"\"cadence_tendance\":-70," +
		"\"rendement\":34," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":44," +
		"\"rendement_tendance\":70," +
		"\"quality\":54," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":64," +
		"\"quality_tendance\":-80" +
		"}," +
		"\"lasthour\":" +
		"{" +
		"\"trs\":70," +
		"\"cadence\":15," +
		"\"cadence_name\":\"Cadence %\"," +
		"\"cadence_objectif\":25," +
		"\"cadence_tendance\":80," +
		"\"rendement\":35," +
		"\"rendement_name\":\"Rendement %\"," +
		"\"rendement_objectif\":45," +
		"\"rendement_tendance\":-100," +
		"\"quality\":55," +
		"\"quality_name\":\"Quality %\"," +
		"\"quality_objectif\":65," +
		"\"quality_tendance\":100" +
		"}" +
		"}" +
		"]";

	static IDictionary configjson = null;
	static List <object> config = null;
	static List <object> injson;

	void Awake ()
	{
//		Debug.Log(jsondata);
		if (fulls == null)		fulls = GameObject.Find("CanvasScreens").FindInChildren("fullscreen").GetComponent<fullscreen>();


		injson = (List <object>)Json.Deserialize(jsondata);


	}

	IEnumerator Start ()
	{
		GameObject poi_window = GameObject.Find("poi_window0");

        while (!downloadmanager.AllDownloaded)
			yield return null;
/*		
		string cfile = File.ReadAllText(Application.persistentDataPath + "/pidata/config.txt");
		configjson = (IDictionary)Json.Deserialize(cfile);
		config = (List <object>)configjson["POI"];

		for (int i=1;i<GetNrMachine();i++)
		{
			GameObject obj = GameObject.Instantiate(poi_window);
			obj.name = "poi_window"+i;
			obj.transform.parent = poi_window.transform.parent;

			poi_attach objproc = obj.GetComponent<poi_attach>();
			objproc.id = i;
			obj.transform.localScale = Vector3.one;
			obj.SetActive(true);
		}
		poi_attach objproc2 = poi_window.GetComponent<poi_attach>();
		objproc2.id = 0;
		poi_window.SetActive(true);
		*/
	}

	void Update2 ()
	{
		if (config != null)
			FillMachine(0);
        
	}


	public static int GetNrMachine()
	{
		return(config.Count);
	}

	public static string GetMachineName(int id)
	{
		IDictionary json = (IDictionary)injson[id];

		return((string)json["machinename"]);
	}




	static void FillOne(IDictionary vals,GameObject obj)
	{
		Image colbar = obj.FindInChildren("mask").FindInChildren("intopbar").GetComponent<Image>();

		for (int i=0;i<3;i++)
		{
			string name = "cadence";
			switch(i)
			{
			case 0 :
				name = "cadence";
				break;
			case 1 :
				name = "rendement";
				break;
			case 2 :
				name = "quality";
				break;
			}
			GameObject elm = obj.FindInChildren("mask").FindInChildren("fillpanel").FindInChildren("statelement"+i);
			Text tm = elm.FindInChildren("percentage").GetComponent<Text>();
			tm.text = (long)vals[name] + "%";
			tm = elm.FindInChildren("objectifpercentage").GetComponent<Text>();
			tm.text = (long)vals[name+"_objectif"] + "%";
			tm = elm.FindInChildren("itemnam").GetComponent<Text>();
			tm.text = (string)vals[name+"_name"];

			long turnval = (long)vals[name+"_tendance"];
			float angleval = ((float)turnval) * 90.0f / 100.0f;
			GameObject turn = elm.FindInChildren("tendance");
			turn.transform.localEulerAngles = new Vector3(0,0,angleval);
			Image colorarrow = turn.GetComponent<Image>();
			if (turnval < 0)
				colorarrow.color = Color.red;
			else
				colorarrow.color = Color.green;

		}
/*
		"\"trs\":20," +
		"\"cadence_tendance\":-10," +
		"\"rendement\":30," +
		"\"rendement_objectif\":40," +
		"\"rendement_tendance\":10," +
		"\"quality\":50," +
		"\"quality_objectif\":60," +
		"\"quality_tendance\":-20" +
		*/
	}

	public static void FillMachine(int id)
	{
		for (int i=0;i<GetNrMachine();i++)
		{
			IDictionary json = (IDictionary)injson[i];
			IDictionary dayvalues = (IDictionary)json["dayvalues"];

			GameObject poi_window = GameObject.Find("poi_window"+i);
			GameObject poi_bar = poi_window.FindInChildren("fillbar").FindInChildren("intopbar");
			RectTransform rt = poi_bar.GetComponent<RectTransform>();

			long trs = (long)dayvalues["trs"];
			float ttrs = ((float)trs) * 1040.0f / 100.0f;
			rt.sizeDelta = new Vector2(170.0f,ttrs);
		}

		if (fulls.onscreen)
		{
			IDictionary json = (IDictionary)injson[id];

			IDictionary dayvalues = (IDictionary)json["dayvalues"];
			GameObject obj = GameObject.Find("CanvasScreens").FindInChildren("window_left");
			FillOne(dayvalues,obj);

			dayvalues = (IDictionary)json["lasthour"];
			obj = GameObject.Find("CanvasScreens").FindInChildren("window_right");
			FillOne(dayvalues,obj);
		}

	}

}
