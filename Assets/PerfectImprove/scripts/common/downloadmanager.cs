﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;


public class FileDL
{
    public string fileName = "";
    public bool onlyIfNotExist = false;

    public FileDL( string fname, bool ifNotExist)
    {
        fileName = fname;
        onlyIfNotExist = ifNotExist;
    }
}

public class downloadmanager : MonoBehaviour
{
    private static volatile downloadmanager _instance;
    private static object _syncRoot = new System.Object();

    private bool _allFinished = false;
    private bool _downloadFileOK = false;

    List<FileDL> FilesToDownload = new List<FileDL>();

    private downloadmanager()
    {
        _allFinished = false;
    }

    public static downloadmanager Instance
    {
        get 
        {
            if (_instance == null) 
            {
                // ATTENTION Ligne non superfulx : On Créé l'instance de Debug avant de passer dans le constructeur de cette class
                Debug.Log("Init downloadmanager ...");

                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<downloadmanager>();

                        singleton.name = "downloadmanager";

                        DontDestroyOnLoad(singleton);
                    }
                }

                Debug.Log("downloadmanager OK !");
            }

            return _instance;
        }
    }

    // Use this for initialization
    IEnumerator Start ()
    {
        bool deleteAll = true;
        if (File.Exists(Application.persistentDataPath + "/pidata/version.txt"))
        {
            string oldVersion = File.ReadAllText(Application.persistentDataPath + "/pidata/version.txt");
            string newVersion = "";

            //string fromPath = Application.streamingAssetsPath + "/pidata/version.txt";
            string fromPath = "file://"+Application.streamingAssetsPath+"/pidata/version.txt";
            #if UNITY_EDITOR
            fromPath = "file://"+Application.streamingAssetsPath+"/pidata/version.txt";
            #elif UNITY_ANDROID
            fromPath = "jar:file://" + Application.dataPath + "!/assets/pidata/version.txt";
            #endif

            WWW reader = new WWW(fromPath);
            while (!reader.isDone) 
            {
                yield return new WaitForSeconds(0.03f);
            }

            if (string.IsNullOrEmpty(reader.error))
            {
                newVersion = reader.text;
            }


            deleteAll = oldVersion != newVersion;
        }

        if (deleteAll)
        {
            try
            {
                Directory.Delete(Application.persistentDataPath + "/pidata", true);
            }
            catch( Exception )
            {
            }
        }

        #if UNITY_EDITOR 
        CreatePIDATAList();
        #endif

        IEnumerator edir = DownloadFile( new FileDL( "pidata/dir.txt", false ) );
        while( edir.MoveNext() )
            yield return new WaitForSeconds(0.03f);

        if( _downloadFileOK )
        {
            string[] allfiles = null;

            try
            {
                allfiles = File.ReadAllText(Application.persistentDataPath+"/pidata/dir.txt").Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
            }
            catch( Exception) 
            {
            }

            if (allfiles != null)
            {
                foreach (string fname in allfiles)
                {
                    bool bOnlyIfNotExist = false;

                    // A revoir pour ne pas ecraser certain fichier si deja existant
                    if (FicCompare(fname, "clientsetting.json"))
                        #if UNITY_EDITOR
                        bOnlyIfNotExist = false;
                        #else
                        bOnlyIfNotExist = false;
                        #endif
                    else if (FicCompare(fname, "Langue.csv"))
                        bOnlyIfNotExist = false;
                    else if (FicCompare(fname, ".png"))
                        bOnlyIfNotExist = true;
                    else if (FicCompare(fname, ".jpg"))
                        bOnlyIfNotExist = true;
                    else if (FicCompare(fname, ".json"))
                        bOnlyIfNotExist = false;
                    else if (FicCompare(fname, ".txt"))
                        bOnlyIfNotExist = false;

                    FilesToDownload.Add(new FileDL(fname, bOnlyIfNotExist));
                }
            }   
        }

        foreach(FileDL file in FilesToDownload)
        {
            IEnumerator efile = DownloadFile( file );
            while( efile.MoveNext() )
                yield return new WaitForSeconds(0.03f);
        }

        lock (_syncRoot)
        {
            _allFinished = true;
        }
    }

    // Update is called once per frame
    void Update () {

    }

    // Getter / Setter
    public bool AllFinished
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _allFinished;
            }
        }
    }

    private bool FicCompare( string fullname, string endname )
    {
        bool bRet = false;
        string fullname_parse = fullname.ToLower();
        string endname_parse = endname.ToLower();

        if (fullname_parse.Length >= endname_parse.Length)
        {
            bRet = (fullname_parse.Substring(fullname_parse.Length - endname_parse.Length) == endname_parse);
        }



        return bRet;
    }

    private void CreatePIDATAList()
    {
        try
        {
            string stpath = Application.streamingAssetsPath + "/pidata";

            string [] testlist = System.IO.Directory.GetFiles(stpath,"*.*",SearchOption.AllDirectories);
            string finalsave = "";
            for (int i=0;i<testlist.Length;i++)
            {
                testlist[i] = testlist[i].Replace("\\","/");
                testlist[i] = testlist[i].Substring(testlist[i].LastIndexOf("/pidata")+1);

                if( FicCompare( testlist[i], ".json" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], ".csv" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], ".png" ) )
                    finalsave += testlist[i] + ";";
                else if( FicCompare( testlist[i], ".jpg" ) )
                    finalsave += testlist[i] + ";";
                else if (FicCompare(testlist[i], ".txt"))
                    finalsave += testlist[i] + ";";
            }

            File.WriteAllText(Application.streamingAssetsPath + "/pidata/dir.txt",finalsave);
        }
        catch( Exception) 
        {
        }
    }

    private IEnumerator DownloadFile( FileDL fic)
    {
        _downloadFileOK = false;


        if( !File.Exists(Application.persistentDataPath + "/" + fic.fileName) ||
            !fic.onlyIfNotExist )
        {
            string fromPath = "file://"+Application.streamingAssetsPath+"/"+fic.fileName;
            #if UNITY_EDITOR
            fromPath = "file://"+Application.streamingAssetsPath+"/"+fic.fileName;
            #elif UNITY_ANDROID
            fromPath = "jar:file://" + Application.dataPath + "!/assets/"+fic.fileName;
            #endif
            WWW mywww = new WWW(fromPath);
            yield return mywww;

            while (!mywww.isDone)
                yield return null;

            try
            {
                string toPath = Application.persistentDataPath + "/" + fic.fileName;
                string toPathDir = Path.GetDirectoryName( toPath );


                if(string.IsNullOrEmpty(mywww.error))
                {
                    byte [] data = mywww.bytes;

                    if (!Directory.Exists(toPathDir))
                        Directory.CreateDirectory(toPathDir);

                    File.WriteAllBytes(toPath,data);
                    data = null;
                    _downloadFileOK = true;
                }
            }
            catch( Exception) 
            {
            }
        }
        else
            _downloadFileOK = true;

    }


    // Methode / Getter / Setter Static
    public static bool AllDownloaded
    {
        get 
        { 
            downloadmanager dlmanager = downloadmanager.Instance;
            return dlmanager.AllFinished;
        }
    }   

	public void restartManager(){
		//Start ();
	}


}