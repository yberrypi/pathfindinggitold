using System.Linq;
using System.Threading;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
//using System.Drawing;
//using System.Drawing.Imaging;

public static class Helper
{
	public static void DebugAwake(string name,GameObject me=null)
	{
		/*
		if (me == null)
			Debug.Log(Application.loadedLevelName+" "+name+":"+DateTime.Now.Minute+" "+DateTime.Now.Second+" "+DateTime.Now.Millisecond);
		else
			Debug.Log(Application.loadedLevelName+" "+name+":"+DateTime.Now.Minute+" "+DateTime.Now.Second+" "+DateTime.Now.Millisecond+"  "+me.name);
		*/
	}


	public static bool IsDestroyed(this GameObject gameObject)
	{
		return gameObject == null && !ReferenceEquals(gameObject, null);
	}

	public static void SetState(this GameObject go, bool act)
	{
		if (go.activeSelf != act)			go.SetActive(act);
	}

	public static GameObject FindInChildren(this GameObject go, string name)
    {
		foreach (Transform child in go.GetComponentsInChildren<Transform>(true))
	    {
			if (child.gameObject.name == name)		return (child.gameObject);
//    	   Debug.Log (list.name);
    	}
		return null;
    }

    public static GameObject FindInCanvas(this GameObject go, string name)
    {
        foreach (Canvas can in GameObject.FindObjectsOfType<Canvas>())
        {
            GameObject obj = can.gameObject.FindInChildren(name);

            if (obj != null)
                return obj;  
        }

        return null;
    }


    public static  T FindInChildrenObjectOfType<T>(this GameObject go) where T : class
    {
        T objT = go.GetComponentInChildren<T>(true);

        return objT;
    }

    public static  T FindInCanvasObjectOfType<T>(this GameObject go) where T : class
    {
        T objT = null;

        foreach (Canvas can in GameObject.FindObjectsOfType<Canvas>())
        {
            objT = can.gameObject.FindInChildrenObjectOfType<T>();

            if (objT != null)
                break;  
        }

        return objT;

    }



	public static void SetVisible(this GameObject go, bool bVisible)
	{
        Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
		
		foreach (Renderer child in renderers)
		{
			child.enabled = bVisible;
		}
		
		if (go.GetComponent<Renderer>() != null)
		{
			go.GetComponent<Renderer>().enabled = bVisible;
		}
            
	}

	static string [] challengemapbuttonunlocked = new string[]
	{
		"ChallengeMapButtonUnLocked"
	};



	public enum ImageFilterMode : int {
		Nearest = 0,
		Biliner = 1,
		Average = 2
	}
	/*
	private static ImageCodecInfo GetEncoder(ImageFormat format)
	{

		ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

		foreach (ImageCodecInfo codec in codecs)
		{
			if (codec.FormatID == format.Guid)
			{
				return codec;
			}
		}
		return null;
	}

	public static Texture2D ResizeTextureDrawing()
	{
		
			Image img = Image.FromFile(@"C:\Users\Antoine\Desktop\photostreamtest\original.png");
			Bitmap bmp1 = new Bitmap(img, new Size((int)((float)img.Width/(float)img.Height*1080f), 1080));
			ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

			System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
			EncoderParameters myEncoderParameters = new EncoderParameters(1);

			//for(int i =0 ; i < 16;i++){
			EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 80L);//(i*25/4));
			myEncoderParameters.Param[0] = myEncoderParameter;
			bmp1.Save(@"C:\Users\Antoine\Desktop\photostreamtest\RESIZED"+80.ToString()+".jpg", jpgEncoder, myEncoderParameters);
		return (null);
	}
*/
	public static Texture2D ResizeTexture(Texture2D pSource, ImageFilterMode pFilterMode, float pScale)
	{
		//*** Variables
		int i;

		//*** Get All the source pixels
		Color[] aSourceColor = pSource.GetPixels(0);
		Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);

		//*** Calculate New Size
		float xWidth = Mathf.RoundToInt((float)pSource.width * pScale);                     
		float xHeight = Mathf.RoundToInt((float)pSource.height * pScale);

		//*** Make New
		Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);

		//*** Make destination array
		int xLength = (int)xWidth * (int)xHeight;
		Color[] aColor = new Color[xLength];

		Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);

		//*** Loop through destination pixels and process
		Vector2 vCenter = new Vector2();
		for(i=0; i<xLength; i++){

			//*** Figure out x&y
			float xX = (float)i % xWidth;
			float xY = Mathf.Floor((float)i / xWidth);

			//*** Calculate Center
			vCenter.x = (xX / xWidth) * vSourceSize.x;
			vCenter.y = (xY / xHeight) * vSourceSize.y;

			//*** Do Based on mode
			//*** Nearest neighbour (testing)
			if(pFilterMode == ImageFilterMode.Nearest){

				//*** Nearest neighbour (testing)
				vCenter.x = Mathf.Round(vCenter.x);
				vCenter.y = Mathf.Round(vCenter.y);

				//*** Calculate source index
				int xSourceIndex = (int)((vCenter.y * vSourceSize.x) + vCenter.x);

				//*** Copy Pixel
				aColor[i] = aSourceColor[xSourceIndex];
			}

			//*** Bilinear
			else if(pFilterMode == ImageFilterMode.Biliner){

				//*** Get Ratios
				float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
				float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);

				//*** Get Pixel index's
				int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
				int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
				int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
				int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));

				//*** Calculate Color
				aColor[i] = Color.Lerp(
					Color.Lerp(aSourceColor[xIndexTL], aSourceColor[xIndexTR], xRatioX),
					Color.Lerp(aSourceColor[xIndexBL], aSourceColor[xIndexBR], xRatioX),
					xRatioY
				);
			}

			//*** Average
			else if(pFilterMode == ImageFilterMode.Average){

				//*** Calculate grid around point
				int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
				int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
				int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
				int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);

				//*** Loop and accumulate
				Vector4 oColorTotal = new Vector4();
				Color oColorTemp = new Color();
				float xGridCount = 0;
				for(int iy = xYFrom; iy < xYTo; iy++){
					for(int ix = xXFrom; ix < xXTo; ix++){

						//*** Get Color
						oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];

						//*** Sum
						xGridCount++;
					}
				}

				//*** Average Color
				aColor[i] = oColorTemp / (float)xGridCount;
			}
		}

		//*** Set Pixels
		oNewTex.SetPixels(aColor);
		oNewTex.Apply();

		//*** Return
		return oNewTex;
	}

	public class ThreadData
	{
		public int start;
		public int end;
		public ThreadData (int s, int e) {
			start = s;
			end = e;
		}
	}

		private static Color[] texColors;
		private static Color[] newColors;
		private static int w;
		private static float ratioX;
		private static float ratioY;
		private static int w2;
		private static int finishCount;
		private static Mutex mutex;

		public static void Point (Texture2D tex, int newWidth, int newHeight)
		{
			ThreadedScale (tex, newWidth, newHeight, false);
		}

		public static void Bilinear (Texture2D tex, int newWidth, int newHeight)
		{
			ThreadedScale (tex, newWidth, newHeight, true);
		}

		private static void ThreadedScale (Texture2D tex, int newWidth, int newHeight, bool useBilinear)
		{
			texColors = tex.GetPixels();
			newColors = new Color[newWidth * newHeight];
			if (useBilinear)
			{
				ratioX = 1.0f / ((float)newWidth / (tex.width-1));
				ratioY = 1.0f / ((float)newHeight / (tex.height-1));
			}
			else {
				ratioX = ((float)tex.width) / newWidth;
				ratioY = ((float)tex.height) / newHeight;
			}
			w = tex.width;
			w2 = newWidth;
			var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
			var slice = newHeight/cores;

			finishCount = 0;
			if (mutex == null) {
				mutex = new Mutex(false);
			}
			if (cores > 1)
			{
				int i = 0;
				ThreadData threadData;
				for (i = 0; i < cores-1; i++) {
					threadData = new ThreadData(slice * i, slice * (i + 1));
					ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
					Thread thread = new Thread(ts);
					thread.Start(threadData);
				}
				threadData = new ThreadData(slice*i, newHeight);
				if (useBilinear)
				{
					BilinearScale(threadData);
				}
				else
				{
					PointScale(threadData);
				}
				while (finishCount < cores)
				{
					Thread.Sleep(1);
				}
			}
			else
			{
				ThreadData threadData = new ThreadData(0, newHeight);
				if (useBilinear)
				{
					BilinearScale(threadData);
				}
				else
				{
					PointScale(threadData);
				}
			}

			tex.Resize(newWidth, newHeight);
			tex.SetPixels(newColors);
			tex.Apply();
		}

		public static void BilinearScale (System.Object obj)
		{
			ThreadData threadData = (ThreadData) obj;
			for (var y = threadData.start; y < threadData.end; y++)
			{
				int yFloor = (int)Mathf.Floor(y * ratioY);
				var y1 = yFloor * w;
				var y2 = (yFloor+1) * w;
				var yw = y * w2;

				for (var x = 0; x < w2; x++) {
					int xFloor = (int)Mathf.Floor(x * ratioX);
					var xLerp = x * ratioX-xFloor;
					newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor+1], xLerp),
						ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor+1], xLerp),
						y*ratioY-yFloor);
				}
			}

			mutex.WaitOne();
			finishCount++;
			mutex.ReleaseMutex();
		}

		public static void PointScale (System.Object obj)
		{
			ThreadData threadData = (ThreadData) obj;
			for (var y = threadData.start; y < threadData.end; y++)
			{
				var thisY = (int)(ratioY * y) * w;
				var yw = y * w2;
				for (var x = 0; x < w2; x++) {
					newColors[yw + x] = texColors[(int)(thisY + ratioX*x)];
				}
			}

			mutex.WaitOne();
			finishCount++;
			mutex.ReleaseMutex();
		}

		private static Color ColorLerpUnclamped (Color c1, Color c2, float value)
		{
			return new Color (c1.r + (c2.r - c1.r)*value, 
				c1.g + (c2.g - c1.g)*value, 
				c1.b + (c2.b - c1.b)*value, 
				c1.a + (c2.a - c1.a)*value);
		}




	public static string CamelHubMe(string initialfilepath)
	{
		if (System.IO.File.Exists(initialfilepath))		return (initialfilepath);
// compose in 3 items
//		Debug.Log (initialfilepath);
		int extentionpos = initialfilepath.LastIndexOf('.');
		string extension = initialfilepath.Substring(extentionpos+1);
		string strippedtext = initialfilepath.Substring(0,extentionpos);
		extentionpos = strippedtext.LastIndexOf('/');
		string filename = strippedtext.Substring(extentionpos+1);
		strippedtext = strippedtext.Substring(0,extentionpos);

		string testfilename = filename.ToLower();
		string [] testme = null;
		if (testfilename == "challengemapbuttonunlocked")			testme = challengemapbuttonunlocked;
		if (testme == null)		return (initialfilepath);
		foreach(string newfn in testme)
		{
//			Debug.Log (strippedtext+ "/" + newfn + "." + extension);

			if (System.IO.File.Exists(strippedtext+ "/" + newfn + "." + extension))
				return (strippedtext+ "/" + newfn + "." + extension);
		}
		return ("");
	}
	
	public static Quaternion QuaternionFromMatrix(Matrix4x4 m) 
	{
		// Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		Quaternion q = new Quaternion();
		q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2; 
		q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2; 
		q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2; 
		q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2; 
		q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
		q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
		q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );
		return q;
	}

	public static double GetMiliseconds(DateTime mytime)
	{
		DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0); //.ToLocalTime();
		TimeSpan span = (mytime - epoch);
		return span.TotalMilliseconds;
	}

	public static  float ElasticeaseOut (float t, float b, float c, float d, float a, float p)
	{
		if (t==0f) 		return b;  
		if ((t/=d)==1) 	return b+c;  
		if (p==0f) p=d*0.3f;
		float s = 0f;
		if ((a==0f) || a < Mathf.Abs(c)) 
		{ 
			a=c; 
			s=p/4; 
		}
		else
		{
			s = p/(2*Mathf.PI) * Mathf.Asin (c/a);
		}
		return (a*Mathf.Pow(2,-10*t) * Mathf.Sin( (t*d-s)*(2*Mathf.PI)/p ) + c + b);
	}
	
#if UNITY_EDITOR
	static public bool DEBUG = false;
	

	static public void DebugTrace(string debugString)
	{
		Debug.Log(debugString);
	}
	
#else
	static public bool DEBUG = false;
	//#define if (Helper.DEBUG) Debug.Log	
	static public void DebugTrace(string debugString)
	{
		
	}
	
#endif
}