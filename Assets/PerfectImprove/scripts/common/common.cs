﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System;
using System.IO;
using MiniJSON;

public class common : MonoBehaviour {

    public static string clientid  = "0CC68B2D-D434_";
    public static string clientkey = "CC68B2D-D434-4EDXX";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public static string GetString(string key)
    {
        string newkey = Encrypt(key);
        string decrypt = PlayerPrefs.GetString(newkey, "");
        if (decrypt == null || decrypt == "") return "";
        return (Decrypt(decrypt));
    }
    public static void SetString(string key,string value)
    {
        string newkey = Encrypt(key);
        string newvalue = Encrypt(value);
        PlayerPrefs.SetString(newkey,newvalue);
        PlayerPrefs.Save();
    }
    public static bool HasKey(string key)
    {
        string newkey = Encrypt(key);
        return(PlayerPrefs.HasKey(newkey));
    }
    public static void    DeleteKeyQuick(string key)
    {
        string newkey = Encrypt(key);
        PlayerPrefs.DeleteKey(newkey);
    }
    public static void    DeleteKey(string key)
    {
        string newkey = Encrypt(key);
        PlayerPrefs.DeleteKey(newkey);
        PlayerPrefs.Save();
    }
    public static int GetInt(string key, int i = 0)
    {
        string newkey = Encrypt(key);
        if (PlayerPrefs.HasKey(newkey)) return (PlayerPrefs.GetInt(newkey));

        return i;   
    }
    public static void SetInt(string key,int value)
    {
        string newkey = Encrypt(key);
        PlayerPrefs.SetInt(newkey,value);
        PlayerPrefs.Save();
    }
    public static float GetFloat(string key)
    {
        string newkey = Encrypt(key);
        return (PlayerPrefs.GetFloat(newkey));
    }
    public static void SetFloat(string key,float value)
    {
        string newkey = Encrypt(key);
        PlayerPrefs.SetFloat(newkey,value);
        PlayerPrefs.Save();
    }
    public static void Save() 
    {
        PlayerPrefs.Save();
    }

    public static string Encrypt (string toEncrypt)
    {
        string createkey = clientkey + clientid;
        string  mykey = createkey;
        if (createkey.Length > 32)      mykey = createkey.Substring(0,32);

        byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
        byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes (toEncrypt);

        RijndaelManaged rDel = new RijndaelManaged ();
        rDel.Key = keyArray;
        rDel.Mode = CipherMode.ECB;
        rDel.Padding = PaddingMode.PKCS7;
        // better lang support
        ICryptoTransform cTransform = rDel.CreateEncryptor ();
        byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
        return Convert.ToBase64String (resultArray, 0, resultArray.Length);
    }

    public static string Decrypt (string toDecrypt)
    {
        string createkey = clientkey + clientid;
        string  mykey = createkey;
        if (createkey.Length > 32)      mykey = createkey.Substring(0,32);

        byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
        byte[] toEncryptArray = Convert.FromBase64String (toDecrypt);
        RijndaelManaged rDel = new RijndaelManaged ();
        rDel.Key = keyArray;
        rDel.Mode = CipherMode.ECB;
        rDel.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = rDel.CreateDecryptor ();
        byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
        return UTF8Encoding.UTF8.GetString (resultArray);

    }

    public static void LoadLevel(string name)
    {
        GameObject obj = GameObject.Find ("ObjectManager");
        Destroy (obj);
        //Application.LoadLevel(name);
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }

    public static void LoadScene(string name)
    {
        LoadLevel(name);
    }

    public static string CurrentScene()
    {
        string name = "";

        UnityEngine.SceneManagement.Scene sc = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

        name = sc.name;

        return name;
    }

    // Attention le lock doit etre pris par l'appelant
    public static byte[] LoadDataFile(string dirname, string filename)
    {
        byte[] data = null;
        string full_filename = dirname + "/" + filename;

        try
        {
            Directory.CreateDirectory(dirname);

            if (File.Exists(full_filename))
            {
                data = File.ReadAllBytes(full_filename);
            }

        }
        catch( Exception ex )
        {
            data = null;
            logger.LogException(ex.Message);
        }

        return data;
    }

    public static bool SaveDataFile(string dirname, string filename, byte[] data)
    {
        bool bRet = false;
        string full_filename = dirname + "/" + filename;

        if (data != null)
        {
            try
            {
                Directory.CreateDirectory(dirname);

                File.WriteAllBytes(full_filename, data);

                bRet = true;
            }
            catch (Exception ex)
            {
                data = null;
                logger.LogException(ex.Message);
            }
        }

        return bRet;
    }


    public static string parseTXT( byte[] byteStrTXT )
    {
        string str = "";

        if (byteStrTXT != null)
        {
            str = Encoding.ASCII.GetString(byteStrTXT);
            //str = Encoding.UTF8.GetString(byteStrTXT);
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //str = enc.GetString(byteStrTXT);
        }

        return str;
    }

    public static List<string[]> parseCSV( byte[] byteStrCSV )
    {

        List<string[]> lst = new List<string[]>();


        if (byteStrCSV != null)
        {
            string str = "";
            //str = Encoding.ASCII.GetString(byteStrCSV);
            //str = Encoding.UTF8.GetString(byteStrCSV);
            Encoding enc = Encoding.GetEncoding("iso-8859-1");
            str = enc.GetString(byteStrCSV);

            str = str.Replace("\r", "\n").Replace("\n\n", "\n");
            bool yaCot = false;
            string sep = ";";

            string c_sav = "";
            string[] chps = null;
            int no_chp = 0;
            int max_chp = 0;


            string[] ligs = str.Split('\n');

            if (ligs != null)
            {
                foreach (string lig in ligs)
                {
                    if (lig != null)
                    {
                        string ligParse = lig.Replace ( "(@@$sautdeligne$@@)","\n").Trim();

                        if (ligParse == "sep=;")
                            sep = ";";
                        else if (ligParse == "sep=,")
                            sep = ",";
                        else if (ligParse == "sep=\t")
                            sep = "\t";
                        else
                        {
                            if (yaCot)
                                ligParse = "\n" + ligParse;

                            for (int i = 0; i < ligParse.Length; i++)
                            {
                                string c = ligParse.Substring(i, 1);

                                if (c == "\"")
                                {
                                    yaCot = !yaCot;
                                    if ( (c_sav == "\"") && yaCot )
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] += c;
                                    }
                                }
                                else if (c == sep)
                                {
                                    if (yaCot)
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] += c;
                                    }
                                    else
                                    {
                                        if ((chps == null) || (chps.Length < (no_chp + 1)))
                                        {
                                            Array.Resize(ref chps, no_chp + 1);
                                            chps[no_chp] = "";
                                        }

                                        chps[no_chp] = chps[no_chp].Trim();
                                        no_chp++;
                                    }
                                }
                                else
                                {
                                    if ((chps == null) || (chps.Length < (no_chp + 1)))
                                    {
                                        Array.Resize(ref chps, no_chp + 1);
                                        chps[no_chp] = "";
                                    }

                                    chps[no_chp] += c;
                                }


                                c_sav = c;
                            }

                            if( (chps != null) && !yaCot )
                            {
                                // La premiere ligne est l'entete CSV : Elle determine le nombre de champs
                                if (lst.Count <= 0)
                                {
                                    max_chp = chps.Length;
                                    while (max_chp > 0)
                                    {
                                        if (chps[max_chp - 1].Length > 0)
                                            break;
                                        else
                                            max_chp--;
                                    }
                                }

                                Array.Resize(ref chps, max_chp);

                                // NULL-> ""
                                for (int i = 0; i < max_chp; i++)
                                {
                                    if (chps[i] == null)
                                        chps[i] = "";
                                }

                                lst.Add(chps);
                                chps = null;
                                no_chp = 0;
                                c_sav = "";
                            } 
                        }  
                    }
                }
            }

            if (chps != null) // Si on passe ici c'est que le fichier est non conforme (probleme de cot ouverte non fermé)
            {
                // La premiere ligne est l'entete CSV : Elle determine le nombre de champs
                if (lst.Count <= 0)
                {
                    max_chp = chps.Length;
                    while (max_chp > 0)
                    {
                        if (chps[max_chp - 1].Length > 0)
                            break;
                        else
                            max_chp--;
                    }
                }

                Array.Resize(ref chps, max_chp);

                // NULL-> ""
                for (int i = 0; i < max_chp; i++)
                {
                    if (chps[i] == null)
                        chps[i] = "";
                }

                lst.Add(chps);
            }
        }

        return lst;
    }

    public static byte[] parseCSV( List<string[]> lstStrCSV)
    {
        byte[] byteStrCSV = null;

        if (lstStrCSV != null)
        {
            StringBuilder str = new StringBuilder();
            string sep = ";";


            foreach( string[] lig in lstStrCSV )
            {
                string lig_csv = "";

                foreach (string col in lig)
                {
                    if (lig_csv.Length > 0)
                        lig_csv += sep;

                    lig_csv += "\"" + col.Replace( "\"", "\"\"" ) + "\""; 
                }

                str.AppendFormat("{0}\r\n", lig_csv);
            }



            //byteStrCSV = Encoding.ASCII.GetBytes(str);
            //byteStrCSV = Encoding.UTF8.GetBytes(str);
            Encoding enc = Encoding.GetEncoding("iso-8859-1");
            byteStrCSV = enc.GetBytes(str.ToString());
        }

        return byteStrCSV;
    }

    public static IDictionary parseJSON( byte[] byteStrJSON )
    {
        IDictionary dictJSON = new Dictionary<string,string>();

        if (byteStrJSON != null)
        {
            string str = "";
            str = Encoding.ASCII.GetString(byteStrJSON);
            //str = Encoding.UTF8.GetString(byteStrJSON);
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //str = enc.GetString(byteStrJSON);

            try
            {
                IDictionary objlist = (IDictionary)Json.Deserialize(str);
                if( objlist != null )
                    dictJSON = objlist;

            }
            catch( Exception ex )
            {
                logger.LogException(ex.Message);
            }
        }

        return dictJSON;
    }


    public static byte[] parseJSON( IDictionary dictStrJSON )
    {
        byte[] byteStrJSON = null;

        if (dictStrJSON != null)
        {
            StringBuilder str = new StringBuilder();
            string sep = ",";
            bool bfirst = true;

            str.Append("{");

            IDictionaryEnumerator dicEnum = dictStrJSON.GetEnumerator();

            if (dicEnum != null)
            {

                dicEnum.Reset();
                while (dicEnum.MoveNext())
                {
                    string keyObj = dicEnum.Key as string;

                    if( (keyObj != null) && (keyObj.Length > 0) && (dicEnum.Value != null) )
                    {
                        string lig_json = "";
                        Type ValueType = dicEnum.Value.GetType();

                        if( ValueType == typeof(string) )
                            lig_json = "\"" + keyObj + "\" : \"" + (string)dicEnum.Value + "\"";

                        else if( (ValueType == typeof(int)) || ( ValueType == typeof(long)) ) 
                            lig_json = "\"" + keyObj + "\" : " + dicEnum.Value.ToString();

                        if (lig_json.Length > 0)
                        {
                            if( bfirst )
                                str.AppendFormat("\r\n   {0}",lig_json );
                            else
                                str.AppendFormat("{1}\r\n   {0}",lig_json, sep);
                        }
                    }

                }
            }

            str.Append("\r\n}\r\n");

            byteStrJSON = Encoding.ASCII.GetBytes(str.ToString());
            //byteStrJSON = Encoding.UTF8.GetBytes(str.ToString());
            //Encoding enc = Encoding.GetEncoding("iso-8859-1");
            //byteStrJSON = enc.GetBytes(str.ToString());
        }

        return byteStrJSON;
    }

    public static GameObject FindInChildren(GameObject go, string name)
    {
        foreach (Transform child in go.GetComponentsInChildren<Transform>(true))
        {
            if (child.gameObject.name == name)      return (child.gameObject);
            //         Debug.Log (list.name);
        }
        return null;
        /*
//      Debug.Log("***** Looking for a " + name + " in "+ go.name);
        //      if (Helper.DEBUG) Debug.Log (go.name+ " find "+name);
        return (from x in go.GetComponentsInChildren<Transform>()

            where x.gameObject.name == name

            select x.gameObject).First();
        */
    }
}
