﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PIMonoBehaviour : MonoBehaviour {

    private static object _syncRoot = new System.Object();
    private static List<WeakReference> _listPIMono = null;

    public void Awake()
    {
        lock( _syncRoot )
        {
            if (_listPIMono == null)
                _listPIMono = new List<WeakReference>();

            if (_listPIMono != null)
                _listPIMono.Add(new WeakReference(this));
        }

        SendMessage("PIAwake");
    }
       
    // Methodes
    public T PIFindObjectOfType<T>() where T : class
    {
        T objT = null;

        lock( _syncRoot )
        {
            if (_listPIMono != null)
            {
                GC.Collect();

                int i = 0;
                while (i < _listPIMono.Count)
                {
                    if (_listPIMono[i].IsAlive)
                    {
                        if( objT == null )
                            objT = _listPIMono[i].Target as T;
                            
                        i++;
                    }
                    else
                        _listPIMono.RemoveAt(i);
                        
                }    
            } 
        }

        return objT;

    }
        
    public T[] PIFindObjectsOfType<T>() where T : class
    {
        List<T> listT = new List<T>();

        lock( _syncRoot )
        {
            if( (_listPIMono != null) && (listT != null) )
            {
                GC.Collect();

                int i = 0;
                while (i < _listPIMono.Count)
                {
                    if (_listPIMono[i].IsAlive)
                    {
                        T objT = _listPIMono[i].Target as T;

                        if (objT != null)
                            listT.Add(objT);

                        i++;
                    }
                    else
                        _listPIMono.RemoveAt(i);

                }    
            } 
        }

        if ((listT != null) && (listT.Count > 0))
            return listT.ToArray();
        else     
            return null;
    }
}
