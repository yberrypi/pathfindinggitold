using UnityEngine;
using System.Collections;

public class facecamera : MonoBehaviour
{
	Camera mycam;

	void Awake ()
	{
		GameObject cam = GameObject.Find("3D Camera");
		mycam = cam.GetComponent<Camera>();
	}
	
	void Update ()
	{
		transform.LookAt(mycam.transform.position);
	}
}
