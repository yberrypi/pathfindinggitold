﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

public class IncrementVersionNumber {

    [MenuItem("Build/Increment version")]
    public static void IncrementVersion()
    {
        string version = PlayerSettings.bundleVersion;
        string[] parts = version.Split('.');
        int lastNum = int.Parse(parts[parts.Length - 1]) + 1;

        parts[parts.Length - 1] = lastNum.ToString();
        string newVersion = string.Join(".", parts);

        PlayerSettings.bundleVersion = newVersion;
        PlayerSettings.Android.bundleVersionCode = lastNum;

        File.WriteAllText(Application.streamingAssetsPath + "/pidata/version.txt", newVersion);
    }

    [PostProcessBuild(1080)]
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {
        IncrementVersion();
    }
}
