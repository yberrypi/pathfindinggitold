﻿using UnityEngine.UI;
using UnityEditor;

public static class ExtensionsAspectRatioFitter
{
	[MenuItem("CONTEXT/AspectRatioFitter/FitImageAspect")]
	private static void FitImageAspect(MenuCommand menuCommand)
	{
		AspectRatioFitter f = menuCommand.context as AspectRatioFitter;
		if (f != null)
		{
			Image img = f.GetComponent<Image>();
			if (img == null && f.transform.parent != null)
				img = f.transform.parent.GetComponent<Image>();
			if (img != null && img.sprite != null)
			{
				Undo.RecordObject(f, "FitImageAspect");

				f.aspectRatio = (float)img.sprite.texture.width / img.sprite.texture.height;
				EditorUtility.SetDirty(f);
			}
		}
	}
}
