﻿using UnityEngine;
using System.Collections;

public class outils_button : MonoBehaviour {

	GameObject DeviceSelection;

	// Use this for initialization
	void Awake () {
		DeviceSelection = GameObject.Find("DeviceSelection");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PressPicker()
	{
		DeviceSelection.SetActive(true);
	}

	public void PressPickerQuit()
	{
		DeviceSelection.SetActive(false);
	}
}
