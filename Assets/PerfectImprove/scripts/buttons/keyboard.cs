﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class keyboard : MonoBehaviour
{
	public List <fullscreen> fullslist = null;
	//GameObject yesno_obj = null;
	yesno yesno_class = null;
    SNAILRUSHDATEPICKER DatePickerGUI = null;
    calendar DatePickerUI = null;

    // Ne Pas utiliser Awake mais PIAwake pour l'init
    void Awake ()
	{
		//yesno_obj = GameObject.Find("CanvasScreen").FindInChildren("yesno");
		//yesno_class = yesno_obj.GetComponent<yesno>();
		//yesno_obj.SetActive(false);
		fullslist = new List<fullscreen>();

        //DatePickerGUI = PIFindObjectOfType<SNAILRUSHDATEPICKER>();
        //DatePickerUI = PIFindObjectOfType<calendar>();
	}

    void Start()
    {
        //DatePickerGUI = PIFindObjectOfType<SNAILRUSHDATEPICKER>();
        //DatePickerUI = PIFindObjectOfType<calendar>();

        DatePickerGUI = gameObject.FindInCanvasObjectOfType<SNAILRUSHDATEPICKER>();
        DatePickerUI = gameObject.FindInCanvasObjectOfType<calendar>();
    }

	public void AddPopupScreen(fullscreen scrn)
	{
        RemoveAllPopupScreen();
		fullslist.Add(scrn);
        scrn.gameObject.SetActive(true);
	}
        
    public void RemoveAllPopupScreen()
    {
        while(fullslist.Count > 0)
        {
            fullscreen retfull = fullslist[0];
            retfull.onscreen = false;
            RemovePopupScreen(retfull);
        }
    }

    public void RemovePopupScreen(fullscreen scrn)
    {
        fullslist.Remove(scrn);
    }

	void Update ()
	{
		/*
		if (
			((Application.platform == RuntimePlatform.Android) || ()) &&

			(Input.GetKeyUp(KeyCode.Escape)))))
			*/
		if (Input.GetKeyUp(KeyCode.Escape))
		{
            TraitementEscape();
		}
		if (Input.GetKeyUp(KeyCode.C))
		{
			GameObject cam = GameObject.Find ("3D Camera");
			clientsetting client = GameObject.FindObjectOfType<clientsetting>();
            poi_data[] alldevices = poi_data.GetAllDevice();
            bool AppInitOK = true;

            if ((cam == null) || (client == null) || !client.PlayerConfigInitOK)
                AppInitOK = false;
                
            if (alldevices != null)
            {
                for (int i = 0; i < alldevices.Length; i++)
                    if (!alldevices[i].TagsConfigInitOK)
                    {
                        AppInitOK = false;
                        break;
                    }
            }
            else
                AppInitOK = false;


            if( AppInitOK )
            {
                string area = client.PlayerConfig.Area;
                bool bTrouve = false;

                for (int i = 0; i < alldevices.Length; i++)
                {
                    if (alldevices[i].TagsConfig.Name == area)
                    {
                        alldevices[i].TagsConfig.SaveConfig(cam.transform.localPosition, cam.transform.localEulerAngles);
                        bTrouve = true;
                        break;
                    }
                }

                if( !bTrouve )
                    client.PlayerConfig.SaveConfig(cam.transform.localPosition, cam.transform.localEulerAngles);
            }
		}	
	}


    public void TraitementEscape()
    {
        if ((DatePickerUI != null) && (DatePickerUI.CalendarIsShow()))
            DatePickerUI.HideCalendar();

        else if ((DatePickerGUI != null) && (DatePickerGUI.CalendarIsShow()))
            DatePickerGUI.HideCalendar();

      //  else if (yesno_obj.activeInHierarchy)
        //{
          //  yesno_obj.SetActive(false);
       // }
        else
        {
            if (fullslist.Count > 0)
            {
                fullscreen retfull = fullslist[0];
                retfull.onscreen = false;
                RemovePopupScreen(retfull);
            }
            else
            {
                //yesno_class.yesnotype = 0;      // quit
            //    yesno_obj.SetActive(true);
            }
        }
    }
}
