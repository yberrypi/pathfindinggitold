﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;


public class stat_button : MonoBehaviour {

    public poi_stat stat =  null;
    private poi_data pd = null;
    private bool waitDatePicker = false;
    private bool modeFinDatePicker = false;
    GameObject goWIN_Stat = null;
    SNAILRUSHDATEPICKER DatePickerGUI = null;
    calendar DatePickerUI = null;


    // Ne Pas utiliser Awake mais PIAwake pour l'init
    void Awake ()
    {
        GameObject father = gameObject.transform.parent.gameObject;
        pd = father.GetComponent<poi_data>();
        while (pd == null)
        {
            father = father.transform.parent.gameObject;
            pd = father.GetComponent<poi_data>();
            goWIN_Stat = father.FindInChildren("fullscreen_stat");
        } 
        //DatePickerGUI = PIFindObjectOfType<SNAILRUSHDATEPICKER>();
        //DatePickerUI = PIFindObjectOfType<calendar>();
    }

    void Start()
    {
        //DatePickerGUI = PIFindObjectOfType<SNAILRUSHDATEPICKER>();
        //DatePickerUI = PIFindObjectOfType<calendar>();
        DatePickerGUI = gameObject.FindInCanvasObjectOfType<SNAILRUSHDATEPICKER>();
        DatePickerUI = gameObject.FindInCanvasObjectOfType<calendar>();
            
    }

    // Update is called once per frame
    void Update () {

        DateTime dateSelect = DateTime.MinValue;

        if (waitDatePicker )
        {
            bool dateSelected = false;

            if (DatePickerUI != null)
                dateSelected = DatePickerUI.CalendarDateIsSelected(out dateSelect);
            else if (DatePickerGUI != null)
                dateSelected = DatePickerGUI.CalendarDateIsSelected(out dateSelect);
          
            if( dateSelected )
            {
                if ((pd != null) && (dateSelect > DateTime.MinValue))
                {
                    if (modeFinDatePicker)
                    {
                        pd.dateFinStat = new DateTime(dateSelect.Year, dateSelect.Month, dateSelect.Day, 23, 59, 59, 999);
                      
                        if (pd.dateFinStat < pd.dateDebStat)
                            pd.dateDebStat = new DateTime(pd.dateFinStat.Year, pd.dateFinStat.Month, pd.dateFinStat.Day, 00, 00, 00, 000);
                    }
                    else
                    {
                        pd.dateDebStat = new DateTime(dateSelect.Year, dateSelect.Month, dateSelect.Day, 00, 00, 00, 000);
                        if (pd.dateFinStat < pd.dateDebStat)
                            pd.dateFinStat = new DateTime(pd.dateDebStat.Year, pd.dateDebStat.Month, pd.dateDebStat.Day, 23, 59, 59, 999); 
                    }

                    if (goWIN_Stat != null)
                    {
                        goWIN_Stat.FindInChildren("txtdatefin").GetComponent<Text>().text = pd.dateFinStat.ToString("dd/MM/yyyy");
                        goWIN_Stat.FindInChildren("txtdatedebut").GetComponent<Text>().text = pd.dateDebStat.ToString("dd/MM/yyyy");
                    }
                }

				waitDatePicker = false;
            }

        }
    }

    void DatePickerOn( bool dateModeFin )
    {
        if(pd != null)
        {
            RectTransform rect = gameObject.GetComponent<RectTransform>();

            if (dateModeFin)
            {
                if (DatePickerUI != null)
                    DatePickerUI.ShowCalendar(pd.dateFinStat, rect);
                
                else if( DatePickerGUI != null)
                    DatePickerGUI.ShowCalendar(pd.dateFinStat, rect);
            }
            else
            {
                if (DatePickerUI != null)
                    DatePickerUI.ShowCalendar(pd.dateDebStat, rect);

                else if( DatePickerGUI != null )
                    DatePickerGUI.ShowCalendar(pd.dateDebStat, rect);
            }

            waitDatePicker = true;
            modeFinDatePicker = dateModeFin;
        }


    }
        
    public void ButtonPressed_ShowStat()
	{
        if (stat != null)
        {
            if( pd != null )
                pd.StartTaskStat(stat);
        }
    }

    public void ButtonPressed_DateDeb() {
        DatePickerOn(false);
    }

    public void ButtonPressed_DateFin() {
        DatePickerOn(true);
    }

}
