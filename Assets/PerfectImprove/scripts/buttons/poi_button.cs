﻿using UnityEngine;
using System.Collections;
using System;

public class poi_button : MonoBehaviour
{
	fullscreen fulls = null;
	keyboard keyboardobj = null;
	statscreen stat = null;
    private poi_data pd = null;

	void Awake ()
	{
		GameObject father = gameObject.transform.parent.gameObject;
		poi_attach at = father.GetComponent<poi_attach>();
		while (at == null)
		{
			father = father.transform.parent.gameObject;
			at = father.GetComponent<poi_attach>();

            if( pd == null )
                pd = father.GetComponent<poi_data>();
		}
                

		if (father != null)
		{
			fulls = father.FindInChildren("CanvasFullscreen").FindInChildren("fullscreen").GetComponent<fullscreen>();
			//            fulls = father.FindInChildren("CanvasStats").FindInChildren("fullscreen").GetComponent<fullscreen>();
			stat = father.FindInChildren("CanvasFullscreen").FindInChildren("fullscreen_stat").GetComponent<statscreen>();
		}

        if (fulls != null)
        {
            fulls.gameObject.SetActive(true);
        }

        if (stat != null)
        {
            stat.gameObject.SetActive(true);
        }
        
		keyboardobj = GameObject.FindObjectOfType<keyboard>();
	}
	
	public void ButtonPressed_SwapStatScreens()
	{
		if (stat.onscreen)
		{
			stat.onscreen = false;
		}
		else
		{
			stat.onscreen = true;
		}
	}

	public void ButtonPressed_SwapScreens()
	{
		if (fulls.onscreen)
		{
			keyboardobj.RemovePopupScreen(fulls);
			fulls.onscreen = false;
            if( stat != null )
                stat.onscreen = false;
		}
		else
		{
            if (pd != null)
            {
                pd.dateDebStat = DateTime.MinValue;
                pd.dateFinStat = DateTime.MinValue;
                pd.StartTaskStat();
            }
            
			keyboardobj.AddPopupScreen(fulls);
			fulls.onscreen = true;
		}
	}
}
