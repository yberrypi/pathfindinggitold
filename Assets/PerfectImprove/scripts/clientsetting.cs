﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.UI;


[Serializable]
public class player_spawn
{
    public string Resolution = "";
    public Vector3 CamPosOrig = new Vector3(0, 0, 0);
    public Vector3 CamRotaOrig = new Vector3(0, 0, 0);
    public float fResolution = 0f;
	public float fieldOfView = 86.0f;
	public player_spawn( string reso, float freso, Vector3 camPos, Vector3 camRota ,float fov = 86.0f)
    {
        Resolution = reso;
        fResolution = freso;
        CamPosOrig = new Vector3(camPos.x, camPos.y, camPos.z);
        CamRotaOrig = new Vector3(camRota.x, camRota.y, camRota.z);
		fieldOfView = fov;
    }
}

[Serializable]
public class player_web
{
    public string MainUrl = "";
    public string ProxyUrl = "";
    public string LoginUser = "";
    public string LoginPass = "";
}

// ATTENTION : Surtout ne pas rendre Serializable cette class car sinon essaye de la charger avant
// que les init necessaires ont été fait
//[Serializable]
public class player_config
{
    private string _goName = "";
    private string _scName = "";

    // Variable du fichier de Config
    public player_web Web = new player_web();
    public List<player_spawn> Spawns = new List<player_spawn>();
    public string Area = "Usine";
    public bool SlideShowActivate = true;


    // Constructor
    public player_config( string scName, string goName, Vector3 camPos, Vector3 camRota ) 
    {
        _scName = scName;
        _goName = goName;

        Spawns.Add( new player_spawn( "4:3",   4f/3f,   camPos, camRota, 86.0f ) );
        Spawns.Add( new player_spawn( "16:9",  16f/9f,  camPos, camRota, 86.0f ) );
        Spawns.Add( new player_spawn( "16:10", 16f/10f, camPos, camRota, 86.0f ) );

        Spawns.Add( new player_spawn( "3:2",   3f/2f,   camPos, camRota, 86.0f ) );
        Spawns.Add( new player_spawn( "5:4",   5f/4f,   camPos, camRota, 86.0f ) );
    }

    // Getter / Setter
    public player_spawn DefaultSpawn
    {
        get 
        { 
            player_spawn spawn = FindDefSpawn();
            return spawn;
        }
    }


    // Methodes
    private player_spawn FindDefSpawn()
    {
        player_spawn spawn = null;

        //float reso = (float)Screen.currentResolution.width / Screen.currentResolution.height;
        float reso = (float)Screen.width / Screen.height;


        // Recherche de la meilleur config
        foreach (player_spawn sp in Spawns)
        {
            if (spawn == null)
                spawn = sp;
            else if (Math.Abs(spawn.fResolution - reso) > Math.Abs(sp.fResolution - reso))
                spawn = sp;
        }

        return spawn;
    }

    public void LoadConfig()
    {
 
        try
        {
            string strJson = null;

            strJson = File.ReadAllText(Application.persistentDataPath + "/pidata/" + _scName + "_" + _goName + ".json");

            JsonUtility.FromJsonOverwrite(strJson, this);
        }
        catch (Exception ex)
        {
            logger.LogException(ex.Message);
        }

    }

    public void SaveConfig( Vector3 camPos, Vector3 camRota)
    {
       
        if( (_goName != null) && (_goName.Length > 0) )
        {
            try
            {
                string strJson = null;
                player_spawn spawn = FindDefSpawn();

                if( (spawn != null)  && 
                    ((camPos.x  != 0) || (camPos.y  != 0) || (camPos.z  != 0)) &&
                    ((camRota.x != 0) || (camRota.y != 0) || (camRota.z != 0)) )
                {
                    spawn.CamPosOrig = camPos;
                    spawn.CamRotaOrig = camRota;
                }
                    
                strJson = JsonUtility.ToJson(this, true);

                File.WriteAllText(Application.persistentDataPath + "/pidata/" + _scName + "_" + _goName + ".json", strJson);

                #if UNITY_EDITOR
                File.WriteAllText(Application.streamingAssetsPath + "/pidata/" + _scName + "_" + _goName + ".json", strJson);
                #endif
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }
        }

    }

    public void SaveConfig()
    {
        SaveConfig(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
    }
}

public class clientsetting : MonoBehaviour {
    
    public player_config PlayerConfig = null;
    public bool PlayerConfigInitOK = false;


    void Init()
    {
        GameObject cam = GameObject.Find ("3D Camera");

        if (cam != null)
            PlayerConfig = new player_config( common.CurrentScene(), "clientsetting", cam.transform.localPosition, cam.transform.localEulerAngles );
        else
            PlayerConfig = new player_config( common.CurrentScene(), "clientsetting", new Vector3(0, 0, 0), new Vector3(0, 0, 0) );

        if (PlayerConfig != null)
        {
            PlayerConfig.LoadConfig();
            PlayerConfig.SaveConfig();
            //PlayerConfig.SaveConfig(cam.transform.localPosition, cam.transform.localEulerAngles);
        }  

        PlayerConfigInitOK = true;
    }

    // Use this for initialization
    IEnumerator Start () {

        PlayerConfigInitOK = false;

        while (!downloadmanager.AllDownloaded)
            yield return null;

        Init();
    }

	
	// Update is called once per frame
	void Update () {
	
	}
}
