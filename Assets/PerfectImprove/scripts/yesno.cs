﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class yesno : MonoBehaviour
{
	public int yesnotype = 0;
	public Text yesnotitle;
	public Text yesnotext;

	void Awake()
	{
		yesnotitle = gameObject.FindInChildren("yesnotitle").GetComponent<Text>();
		yesnotext = gameObject.FindInChildren("yesnotext").GetComponent<Text>();
	}

	// Use this for initialization
	void OnEnable ()
	{
		switch (yesnotype)
		{
		case 0 :		// quit panel
			yesnotitle.text = localization.GetTextWithKey("QUIT_APP_TITLE");
			yesnotext.text = localization.GetTextWithKey("QUIT_APP_TEXT");
			break;
		}
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void PressedYes()
	{
		switch (yesnotype)
		{
		case 0 :		// quit panel
            gameObject.SetActive(false);
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
			break;
		}
	}

	public void PressedNo()
	{
		switch (yesnotype)
		{
		case 0 :		// quit panel
			gameObject.SetActive(false);
			break;
		}
	}
}
