﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showtext : MonoBehaviour
{
	public string mykey;
    public bool forceUpperCase;
	Text mytext;

    private float _vitesseRefresh = 0f;
    private string _oldText = "";

	void Awake ()
	{
		mytext = gameObject.GetComponent<Text>();
	}

	void Update ()
	{
        _vitesseRefresh += Time.deltaTime;
        if (_vitesseRefresh >= 0.5f)
        {
            _vitesseRefresh = 0f;
            string texttoshow = localization.GetTextWithKey(mykey);

            if (_oldText != texttoshow)
            {
                _oldText = texttoshow;

                texttoshow = texttoshow.Replace("<date>", showdate.TodaysDate());
                texttoshow = texttoshow.Replace("<lasthour>", "LASTHOUR");
                texttoshow = texttoshow.Replace("<good>", "PIECESBONNES");
                texttoshow = texttoshow.Replace("<starthour>", "HEUREDEMARRAGE");

                if (forceUpperCase)
                    mytext.text = texttoshow.ToUpper();
                else
                    mytext.text = texttoshow;
            }
        }
	}
}
