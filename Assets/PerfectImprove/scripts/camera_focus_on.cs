﻿using UnityEngine;
using System.Collections;

public class camera_focus_on : MonoBehaviour
{
    private bool initOK = false;

    private clientsetting client = null;
    private poi_data[] alldevices = null;
    private GameObject cam = null;

    private Vector3 camRotaOrig = new Vector3();
    private Vector3 camPosOrig = new Vector3(); 
    private string currentArea = "";



	void Awake ()
	{
	}

    private void Init()
    {
        Vector3 camRota = new Vector3();
        Vector3 camPos= new Vector3();

        cam = GameObject.Find ("3D Camera");

        if( cam != null )
        {
            camRotaOrig = cam.transform.localEulerAngles;
            camPosOrig = cam.transform.localPosition;
        }
            
        if( client != null )
        {
            player_spawn spawn = client.PlayerConfig.DefaultSpawn;
           
            if( spawn != null )
            {
                camRotaOrig = spawn.CamRotaOrig;
                camPosOrig = spawn.CamPosOrig;
            }

            currentArea = client.PlayerConfig.Area;
        }

        camRota = camRotaOrig;
        camPos = camPosOrig;

        if (alldevices != null)
        {
            for (int i = 0; i < alldevices.Length; i++)
            {
                if (alldevices[i].TagsConfig.Name == currentArea)
                {
                    camRota = alldevices[i].TagsConfig.Spawn.CamRotaOrig;
                    camPos = alldevices[i].TagsConfig.Spawn.CamPosOrig;
                    break;
                }
            }
        }
            
        initOK = true;

        StartTraveling(camPos, camRota);
    }


    // Use this for initialization
    IEnumerator Start () {

        client = GameObject.FindObjectOfType<clientsetting>();
        alldevices = poi_data.GetAllDevice();

        while (!downloadmanager.AllDownloaded)
            yield return null;

        if (client != null)
        {
            while (!client.PlayerConfigInitOK)
                yield return null;
        }

        if (alldevices != null)
        {
            bool test = false;
            while (!test)
            {
                test = true;
                for (int i = 0; i < alldevices.Length; i++)
                    if (!alldevices[i].TagsConfigInitOK)
                        test = false;
                yield return null;
            }
        }
            
        Init();
    }

	IEnumerator SpawnTo(Vector3 pos,Vector3 rot)
	{
		Vector3 startpos = cam.transform.localPosition;

		Vector3 startrot = cam.transform.localEulerAngles;
		Vector3 diff = startrot - rot;
		while (diff.x > 180.0f)			diff.x-=360.0f;
		while (diff.x < -180.0f)		diff.x+=360.0f;
		while (diff.y > 180.0f)			diff.y-=360.0f;
		while (diff.y < -180.0f)		diff.y+=360.0f;
		while (diff.z > 180.0f)			diff.z-=360.0f;
		while (diff.z < -180.0f)		diff.z+=360.0f;
		startrot = rot +diff;

		float timer = 0.0f;
		while (timer < 1.0f)
		{
			cam.transform.localPosition = Vector3.Lerp(startpos, pos, timer);
			cam.transform.localEulerAngles = Vector3.Lerp(startrot, rot, timer);
			timer += Time.deltaTime / 4.0f;		// 1.0f is fast / 10.0f is slow
			yield return null;
		}
	}

    float FloatPrecision3( float val )
    {
        int ival;

        ival = (int)(val * 1000);

        return((float)ival / 1000f);
    }
    

	void Update ()
	{
        if (initOK)
        {
            if (client != null)
            {
                if (currentArea != client.PlayerConfig.Area)
                {
                    Vector3 camRota = new Vector3();
                    Vector3 camPos= new Vector3();

                    camRota = camRotaOrig;
                    camPos = camPosOrig;

                    currentArea = client.PlayerConfig.Area;

                    if (alldevices != null)
                    {
                        for (int i = 0; i < alldevices.Length; i++)
                        {
                            if (alldevices[i].TagsConfig.Name == currentArea)
                            {
                                camRota = alldevices[i].TagsConfig.Spawn.CamRotaOrig;
                                camPos = alldevices[i].TagsConfig.Spawn.CamPosOrig;
                                break;
                            }
                        }
                    }

                    StartTraveling(camPos, camRota);
                }
            }
        }
	}

    public void StartTraveling( Vector3 pos,Vector3 rot )
    {
        if ( initOK )
        {
            StopAllCoroutines();
            StartCoroutine(SpawnTo(pos,rot));
        }
    }
}
