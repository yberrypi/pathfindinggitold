﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrFPS : MonoBehaviour {

    public Text FPSVal;


    private float accum   = 0f; // FPS accumulated over the interval
    private int   frames  = 0; // Frames drawn over the interval

    public  float frequency = 0.5F; // The update frequency of the fps

	// Use this for initialization
	void Start () {

        StartCoroutine( FPS() );
	}
	
	// Update is called once per frame
	void Update () {

        accum += Time.timeScale/ Time.deltaTime;
        ++frames;

	}

    IEnumerator FPS()
    {
        // Infinite loop executed every "frenquency" secondes.
        while( true )
        {
            // Update the FPS
            float fps = accum/frames;
            float msec = 1000.0f / fps;
            //string sFPS = fps.ToString( "f" + Mathf.Clamp( 2, 0, 10 ) );

            //Update the color
            Color color = (fps >= 30) ? Color.green : ((fps > 15) ? Color.yellow : Color.red);

            accum = 0.0F;
            frames = 0;

            if( FPSVal != null )
            {
                FPSVal.text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
                FPSVal.color = color;
            }

            yield return new WaitForSeconds( frequency );
        }
    }
}
