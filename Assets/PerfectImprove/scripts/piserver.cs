﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Globalization;
using System.IO;
using System.Net.Security;
using TcpClient = SocketEx.TcpClient;
using System.Threading;
//using BestHTTP;
using MiniJSON;

public class PIServerTask {
    public enum TypeServerTask
    {
        UNKNOW = 0,
        MAIL = 1,
        STAT = 2
    }

    private object _syncRoot = new System.Object();

    private TypeServerTask _typeTask = TypeServerTask.MAIL;
    private int _tryCount = 0;
    private DateTime _lastTimeTry = DateTime.MinValue;
    private bool _isFinished = false;
    private bool _isCanceled = false;

    private string _message = "";
    private string[] _codes = { "", "", "" };
    private DateTime _dateDebut = DateTime.MinValue;
    private DateTime _dateFin = DateTime.MinValue;
    private int _width= 0;
    private int _height = 0;
    private byte[] _image = null;



    public PIServerTask( TypeServerTask typeTask, string message, string code0, string code1, string code2, DateTime dateDebut,  DateTime dateFin, int width, int height )
    {
        _typeTask = typeTask;
        _tryCount = 0;
        _lastTimeTry = DateTime.MinValue;
        _isFinished = false;
        _isCanceled = false;

        _image = null;

        _message = message;

        _codes[0] = code0;
        _codes[1] = code1;
        _codes[2] = code2;
        _dateDebut = dateDebut;
        _dateFin = dateFin;
        _width = width;
        _height = height;
    }

    public PIServerTask( TypeServerTask typeTask, string message )
        : this(typeTask, message, "", "", "", DateTime.Now, DateTime.Now, 0, 0)
    {
    }

    public PIServerTask( TypeServerTask typeTask, string code0, string code1, string code2, DateTime dateDebut,  DateTime dateFin, int width, int height )
        : this(typeTask, "", code0, code1, code2, dateDebut,dateFin, width, height)
    {
    }

    // Getter / Setter
    public TypeServerTask TypeTask
    {
        get
        { 
            lock (_syncRoot)
            {
                return _typeTask;
            }
        }
    }
        
    public int TryCount
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _tryCount;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _tryCount = value;
            }
        }
    }

    public DateTime LastTimeTry
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _lastTimeTry;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _lastTimeTry = value;
            }
        }
    }

    public bool IsFinished
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _isFinished;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _isFinished = value;
            }
        }
    }

    public bool IsCanceled
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _isCanceled;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _isCanceled = value;
            }
        }
    }

    public byte[] Image
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _image;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _image = value;
            }
        }
    }


    public string Message
    {
        get
        { 
            lock (_syncRoot)
            {
                return _message;
            }
        }
    }

    public string[] Codes
    {
        get
        { 
            lock (_syncRoot)
            {
                return _codes;
            }
        }
    }

    public DateTime DateDebut
    {
        get
        { 
            lock (_syncRoot)
            {
                return _dateDebut;
            }
        }
    }

    public DateTime DateFin
    {
        get
        { 
            lock (_syncRoot)
            {
                return _dateFin;
            }
        }
    }

    public int Width
    {
        get
        { 
            lock (_syncRoot)
            {
                return _width;
            }
        }
    }

    public int Height
    {
        get
        { 
            lock (_syncRoot)
            {
                return _height;
            }
        }
    }
}

public class CTCPClientConnect {
    
    private object _syncRoot = new System.Object();

    private object _socket = null;
    private  ManualResetEvent _signalWaitTCPAsyncConnect = null;
    private bool _connected = false;
    private string _message = "";

    public CTCPClientConnect( TcpClient socket )
    {
        _socket = socket;
        _signalWaitTCPAsyncConnect= new ManualResetEvent(false);
        _connected = false;
        _message = "";
    }

    public CTCPClientConnect( SslStream socket )
    {
        _socket = socket;
        _signalWaitTCPAsyncConnect= new ManualResetEvent(false);
        _connected = false;
        _message = "";
    }

    // Getter / Setter
    public object Socket
    {
        get 
        { 
            return _socket;
        }
    }

    public ManualResetEvent SignalWaitTCPAsyncConnect
    {
        get 
        { 
            return _signalWaitTCPAsyncConnect;
        }
    }

    public bool Connected
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _connected;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _connected = value;
            }
        }
    }

    public string Message
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _message;
            }
        }
        set 
        { 
            lock (_syncRoot)
            {
                _message = value;
            }
        }
    }
}


public class piserver : MonoBehaviour {

    private static volatile piserver _instance;
    private static object _syncRoot = new System.Object();
    private Uri _mailURL = null;
    private Uri _getJsonURL = null;
    private Uri _phantomJSURL = null;
    private string _user = "";
    private string _pass = "";
    private string _proxyHost = "";
    private int _proxyPort = -1;
    private string _baseURL = "";
    private Thread _thServer = null;
    private AutoResetEvent _eWaitServer = null;
    private bool _bStopServer = false;
    private int _loopTime = 10000; // 10secondes
    private int _maxTaskTry = 10; 
    private List<PIServerTask> _serverTaskList = null;
    private Dictionary<string,object> _realTimeData = null;
    private string _realTimeDataFile = null;
    DateTime _realTimeDate = DateTime.MinValue;
    private clientsetting _clientSetting = null;

    private bool _initOK = false;

    private void Init()
    {
        _realTimeDataFile = Application.persistentDataPath + "/lastrealtimedata.json";
        _realTimeData = new Dictionary<string, object>();
        _realTimeDate = DateTime.MinValue;

        if (File.Exists(_realTimeDataFile))
        {
            try
            {
                string txtJson = File.ReadAllText(_realTimeDataFile);

                if( txtJson.Length > 0 )
                {
                    IDictionary dictJson =  (IDictionary)Json.Deserialize(txtJson);

                    AddAllRealTimeData( dictJson );
                }
            }
            catch( Exception ex) 
            {
                logger.LogException(ex.Message);
            }

        }

        _serverTaskList = new List<PIServerTask>();

		string proxy = "";
		Uri proxyURL = null;

        // Il Faut que downloadmanager.cs est terminé sont traitement avant la construction de cette Class

        if (_clientSetting != null)
        {
            _user = _clientSetting.PlayerConfig.Web.LoginUser;
            _pass = _clientSetting.PlayerConfig.Web.LoginPass;
            _baseURL = _clientSetting.PlayerConfig.Web.MainUrl;

            // Pour l'instant Force mode Non Proxy
            //proxy = _clientSetting.PlayerConfig.Web.ProxyUrl;

        }

        try
        {
            if( (proxy.Length >= 7) && (proxy.Substring( 0, 7).ToLower() == "http://") )
                proxyURL = new Uri(proxy);
            else if( proxy.Length > 0 )
                proxyURL = new Uri("http://" + proxy);
     
        }
        catch( Exception ex) 
        {
            logger.LogException(ex.Message);
        }

        if (proxyURL != null)
        {
            _proxyHost = proxyURL.Host;
            _proxyPort = proxyURL.Port;
        }

        try
        {
            _mailURL = new Uri(_baseURL + "mail.php");
        }
        catch( Exception ex) 
        {
            logger.LogException(ex.Message);
        }

        try
        {
            _getJsonURL = new Uri(_baseURL + "getjson.php");
        }
        catch( Exception ex) 
        {
            logger.LogException(ex.Message);
        }

        try
        {
            _phantomJSURL = new Uri(_baseURL + "phantomjs.php");
        }
        catch( Exception ex) 
        {
            logger.LogException(ex.Message);
        }
            
        _bStopServer = false;
        _eWaitServer = new AutoResetEvent(false);
        _thServer = new Thread (ServerThread);
        _thServer.Start ();

        _initOK = true;
    }





    private piserver()
    {
        

        // Deplacer le Init dans Start
        // Init()
    }

    public static piserver Instance
    {
        get 
        {
            if (_instance == null) 
            {
                // ATTENTION Ligne non superfulx : On Créé l'instance de logger avant de passer dans le constructeur de cette class
                logger instanceLogger = logger.Instance;

                lock (_syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();

                        _instance = singleton.AddComponent<piserver>();

                        singleton.name = "piserver";

                        DontDestroyOnLoad(singleton);
                    }
                }
            }

            return _instance;
        }
    }

    void OnDestroy()
    {
        _bStopServer = true;

        if (_eWaitServer != null)
            _eWaitServer.Set();

        if (_thServer != null)
        {
            _thServer.Join();
            _thServer = null;
        }
    }



    // Use this for initialization
    IEnumerator Start () {

        _clientSetting = GameObject.FindObjectOfType<clientsetting>();

        while (!downloadmanager.AllDownloaded)
            yield return null;

        if (_clientSetting != null)
        {
            while (!_clientSetting.PlayerConfigInitOK)
                yield return null;
        }

        Init();
    }

    // Update is called once per frame
    void Update () {

    }

    private void AddAllRealTimeData( IDictionary dic, string basekey )
    {
        if (dic != null)
        {
            IDictionaryEnumerator dicEnum = dic.GetEnumerator();

            if (dicEnum != null)
            {
                dicEnum.Reset();
                do
                {
                    string keyObj = dicEnum.Key as string;
                    IDictionary dictObj = dicEnum.Value as IDictionary;


                    if( keyObj != null )
                    {
                        string key = basekey + "/" + keyObj;

                        if (dictObj != null)
                        {
                            AddAllRealTimeData( dictObj, key );
                        }
                        else
                        {
                            _realTimeData.Add( key.ToLower(), dicEnum.Value );
                        }
                    }
                }
                while (dicEnum.MoveNext());

            }
        }
    }

    private void AddAllRealTimeData( IDictionary dic )
    {
        try
        {
            if( _realTimeData == null )
                _realTimeData = new Dictionary<string, object>();
            else
                _realTimeData.Clear();

            AddAllRealTimeData( dic, "" );

            _realTimeDate = DateTime.Now;

        }
        catch( Exception ex )
        {
            logger.LogException(ex.Message);
        }
    }

    string StreamReadASCIILine (Stream stream)
    {
        var line = new List<byte> ();
        while (true) {
            int c = stream.ReadByte ();
            if (c == -1) {
                break;
            }

            if ((byte)c == '\n')
                break;
            else if ((byte)c != '\r')
                line.Add ((byte)c);
        }
        var s = ASCIIEncoding.ASCII.GetString (line.ToArray ()).Trim ();
        return s;
    }
           

    private string UrlEncode( string url )
    {
        StringBuilder url_encoded = new StringBuilder("");

        if (url != null)
        {
            //char[] uc = url.ToCharArray();
            byte[] uc = Encoding.UTF8.GetBytes(url);

            for (int i = 0; i < uc.Length; i++)
            {
                if( ((uc[i] >= 'A') && (uc[i] <= 'Z')) ||
                    ((uc[i] >= 'a') && (uc[i] <= 'z')) ||
                    ((uc[i] >= '0') && (uc[i] <= '9')) ||
                    (uc[i] == '-') || (uc[i] == '_') || (uc[i] == '.') || (uc[i] == '~')
                )
                {
                    url_encoded.Append((char)uc[i]);
                }
                else
                {
                    url_encoded.AppendFormat("%{0:X02}", (byte)uc[i]);
                }
            }
        }

        return url_encoded.ToString();
    }


    private void AsyncConnectCallBack(IAsyncResult ar)
    {
        CTCPClientConnect cliConnect  = (CTCPClientConnect)ar.AsyncState;

        if (cliConnect != null)
        {
            try
            {
                TcpClient cli = cliConnect.Socket as TcpClient;
                SslStream cliSsl = cliConnect.Socket as SslStream;

                if( cli != null )
                    cli.EndConnect(ar);

                if( cliSsl != null )
                    cliSsl.EndAuthenticateAsClient(ar);
                
                cliConnect.Message = "";
                cliConnect.Connected = true;
            }
            catch (Exception ex)
            {
                cliConnect.Message = ex.Message;
                cliConnect.Connected = false;
            }

            cliConnect.SignalWaitTCPAsyncConnect.Set();
        }
    }

    private bool HTTPPost( Uri url, string proxyHost, int proxyPort, Dictionary<string, string> fields, out int status, out Stream dataStream, out Encoding dataEncoding )
    {
        TcpClient cli = null;
        Stream stream = null;
        string dataUrlEncoded = "";
        bool bModeProxy = false;
        bool bModeHTTPS = false;
        bool bRet = true;

        status = 0;
        dataStream = null; 
        dataEncoding = Encoding.ASCII;

        bModeProxy = ((proxyHost != null) && (proxyHost.Length > 0) && (proxyPort > 0));

        if ((url == null) || (url.Host == null) || (url.Host.Length <= 0) || (url.Port <= 0))
        {
            logger.LogError("BAD URL");
            bRet = false;
        }
        else
        {
            bModeHTTPS = (url.Scheme.ToLowerInvariant() == "https");
        }

        if (bRet)
        {
            //dataUrlEncoded = "login=sailiz2016_001&mdp=CC68B2D-D434-4ED&action=Envoyer&message=testéè@à";
        
            if (fields != null)
            {
                foreach (KeyValuePair<string,string> dicItem in fields)
                {
                    if( dataUrlEncoded.Length > 0 )
                        dataUrlEncoded += "&" + dicItem.Key + "=" + UrlEncode(dicItem.Value);
                    else
                        dataUrlEncoded += dicItem.Key + "=" + UrlEncode(dicItem.Value);
                }
            }
        }
            
        if (bRet)
        {
            try
            {
                cli = new TcpClient();
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

            bRet = (cli != null);
        }


        if (bRet)
        {
            try
            {
                cli.ConnectTimeout = new TimeSpan(0, 0, 5);
                cli.ReadTimeout = new TimeSpan(0, 0, 5);
                cli.WriteTimeout = new TimeSpan(0, 0, 5);
                cli.SendTimeout = 5000;
                cli.ReceiveTimeout = 5000;

                CTCPClientConnect cliConnect = new CTCPClientConnect( cli );

                if( (cliConnect != null) && (cliConnect.SignalWaitTCPAsyncConnect != null) ) // Connect Asynchone
                {
                    bool bRetWait;

                    cliConnect.SignalWaitTCPAsyncConnect.Reset();

                    if (bModeProxy)
                        cli.BeginConnect(proxyHost, proxyPort, new AsyncCallback(AsyncConnectCallBack), cliConnect );
                    else
                        cli.BeginConnect(url.Host, url.Port, new AsyncCallback(AsyncConnectCallBack), cliConnect );

                    bRetWait = cliConnect.SignalWaitTCPAsyncConnect.WaitOne(5000);

                    if( bRetWait )
                    {
                        if( !cliConnect.Connected )
                        {
                            try
                            {
                                cli.Close();
                            }
                            catch( Exception ) {}

                            cli = null;

                            logger.LogError("Error Connect " + cliConnect.Message);
                            bRet = false;
                        }
                    }
                    else
                    {
                        try
                        {
                            cli.Close();
                        }
                        catch( Exception ) {}

                        cli = null;

                        logger.LogError("Connect Timeout");
                        bRet = false;
                    }


                }
                else
                {
                    if (bModeProxy)
                        cli.Connect(proxyHost, proxyPort);
                    else
                        cli.Connect(url.Host, url.Port);
                }
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
                bRet = false;
            }
        }


        if (bRet )
        {
            try
            {
                if (bModeHTTPS && !bModeProxy)
                {
                    SslStream sslStream = new SslStream(cli.GetStream(), false, (sender, cert, chain, errors) =>
                    {
                        return true;
                    });
                            
                    sslStream.ReadTimeout = 5000;
                    sslStream.WriteTimeout = 5000;

                    if (!sslStream.IsAuthenticated)
                    {

                        CTCPClientConnect cliConnect = new CTCPClientConnect( sslStream );

                        if( (cliConnect != null) && (cliConnect.SignalWaitTCPAsyncConnect != null) ) // Connect Asynchone
                        {
                            bool bRetWait;

                            cliConnect.SignalWaitTCPAsyncConnect.Reset();

                            sslStream.BeginAuthenticateAsClient( url.Host, new AsyncCallback(AsyncConnectCallBack), cliConnect );

                            bRetWait = cliConnect.SignalWaitTCPAsyncConnect.WaitOne(5000);

                            if( bRetWait )
                            {
                                if( !cliConnect.Connected )
                                {
                                    try
                                    {
                                        cli.Close();
                                    }
                                    catch( Exception ) {}

                                    cli = null;

                                    logger.LogError("Error Connect " + cliConnect.Message);
                                    bRet = false;
                                }
                            }
                            else
                            {
                                try
                                {
                                    cli.Close();
                                }
                                catch( Exception ) {}

                                cli = null;

                                logger.LogError("Connect Timeout");
                                bRet = false;
                            }
                        }
                        else
                        {
                            sslStream.AuthenticateAsClient(url.Host);
                        }
                    }
                    
                    stream = sslStream;
                }
                else
                    stream = cli.GetStream();
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
                bRet = false;
            }
        }

        if (bRet)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                var streamW = new BinaryWriter( stream );
                Stream byteStream = null;
                long numBytesToRead = 0;
                byte[] buf;

                if( dataUrlEncoded.Length > 0 )
                {
                    byteStream = new MemoryStream(Encoding.UTF8.GetBytes(dataUrlEncoded));
                    numBytesToRead = byteStream.Length;
                }

                if (bModeProxy)
                    query.Append( "POST " + url.OriginalString + " HTTP/1.0\r\n" );
                else
                    query.Append( "POST " + url.PathAndQuery + " HTTP/1.1\r\n" );
                
                query.Append( "Host: " + url.Host + "\r\n");
                query.Append( "User-Agent: PIViewer\r\n");
                query.Append( "Accept: text/html, */*\r\n");
                query.Append( "Accept-Encoding: identity\r\n");
                query.Append( "Accept-Language: fr-FR\r\n");
                query.Append( "Cache-Control: no-cache\r\n");
                query.Append( "Pragma: no-cache\r\n");
                query.Append( "Content-Type: application/x-www-form-urlencoded; charset=UTF-8\r\n");
                query.Append( "Content-Length: " + numBytesToRead + "\r\n");


                query.Append( "\r\n");

                buf = ASCIIEncoding.ASCII.GetBytes( query.ToString() );
                    
                streamW.Write( buf );

                buf = new byte[512];
                while (numBytesToRead > 0)
                {
                    int readed = byteStream.Read(buf, 0, 512);
                    streamW.Write(buf, 0, readed);
                    numBytesToRead -= readed;
                }
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
                bRet = false;
            }
        }

        if (bRet)
        {
            // Lecture Ligne status
            string lig;
            string[] param;

            lig = StreamReadASCIILine(stream);
            param = lig.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (param.Length >= 2)
            {
                int.TryParse(param[1].Trim(), out status);
                int contentLength = 0;
                bool bChuncked = false;

                // Lecture de l'entete
                while (bRet)
                {
                    lig = StreamReadASCIILine(stream);
                    if (lig.Length <= 0) // Fin d'entete
                        break;
                    else
                    {
                        param = lig.Split(new char[] { ':' }, 2, StringSplitOptions.RemoveEmptyEntries );
                        if (param.Length == 2)
                        {
                            if (param[0].Trim().Equals("content-length", StringComparison.InvariantCultureIgnoreCase))
                            {
                                int.TryParse(param[1].Trim(), out contentLength);
                            }
                            else if (param[0].Trim().Equals("transfer-encoding", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (param[1].Trim().Equals("chunked", StringComparison.InvariantCultureIgnoreCase))
                                    bChuncked = true;
                            }
                            else if (param[0].Trim().Equals("content-type", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (param[1].IndexOf("charset=UTF-8", StringComparison.InvariantCultureIgnoreCase) >= 0)
                                    dataEncoding = Encoding.UTF8;
                            }
                        }
                        else
                        {
                            logger.LogError("BAD Read Head Line (line:" + lig + ")");
                            bRet = false;
                        }
                    }
                }

                // Lecture des data
                if (bRet && ((contentLength > 0) || bChuncked) )
                { 
                    while (bRet)
                    {
                        if (bChuncked)
                        {
                            Int32 szChunk = 0;

                            lig = StreamReadASCIILine(stream);
                            if (Int32.TryParse(lig, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out szChunk))
                            {
                                if (szChunk >= 0)
                                {
                                    if (dataStream == null)
                                    {
                                        try
                                        {
                                            dataStream = new MemoryStream();
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.LogException(ex.Message);
                                            bRet = false;
                                        }
                                    }
                                            
                                    if( bRet && (dataStream != null) )
                                    {
                                        try
                                        {
                                            byte[] buf = new byte[512];
                                            int numBytesToRead = szChunk;

                                            while (numBytesToRead > 0)
                                            {
                                                int readed, readcount;

                                                readcount = 512;
                                                if( readcount > numBytesToRead )
                                                    readcount = numBytesToRead;

                                                readed = stream.Read(buf, 0, readcount);

                                     
                                                if( readed > 0 )
                                                {
                                                    dataStream.Write(buf, 0, readed);                                                    
                                                    numBytesToRead -= readed;
                                                }
                                                else if( readed < 0 )
                                                {
                                                    logger.LogError("BAD Read for data");
                                                    bRet = false;
                                                    break;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.LogException(ex.Message);
                                            bRet = false;
                                        }

                                    }
                                    else
                                    {
                                        logger.LogError("Error alloc memory for data");
                                        bRet = false;
                                    }
                                }

                                if (bRet)
                                {
                                    // On lit le CR+LF de fin de chunk
                                    lig = StreamReadASCIILine(stream);

                                    if (szChunk <= 0)
                                    {
                                        // FIN
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                logger.LogError("BAD Read Size Chunk Line (line:" + lig + ")");
                                bRet = false;
                            }
                        }
                        else if (contentLength > 0)
                        {
                            if (dataStream == null)
                            {
                                try
                                {
                                    dataStream = new MemoryStream();
                                }
                                catch (Exception ex)
                                {
                                    logger.LogException(ex.Message);
                                    bRet = false;
                                }
                            }

                            if( bRet && (dataStream != null) )
                            {
                                try
                                {
                                    byte[] buf = new byte[512];
                                    int numBytesToRead = contentLength;

                                    while (numBytesToRead > 0)
                                    {
                                        int readed, readcount;

                                        readcount = 512;
                                        if( readcount > numBytesToRead )
                                            readcount = numBytesToRead;

                                        readed = stream.Read(buf, 0, readcount);

                                        if( readed > 0 )
                                        {
                                            dataStream.Write(buf, 0, readed);
                                            numBytesToRead -= readed;
                                        }
                                        else if( readed < 0 )
                                        {
                                            logger.LogError("BAD Read for data");
                                            bRet = false;
                                            break;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.LogException(ex.Message);
                                    bRet = false;
                                }

                            }
                            else
                            {
                                logger.LogError("Error alloc memory for data");
                                bRet = false;
                            }

                            // FIN
                            break;
                        }
                        else
                            break;
                            
                    }
                }
                
            }
            else
            {
                logger.LogError("BAD Read Status (line:" + lig + ")");
                bRet = false;
            }
        }


        if (bRet && (dataStream != null))
        {
            try
            {
                dataStream.Flush();
                dataStream.Position = 0;
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
                bRet = false;
            }
        }


        try
        {
            if ( (cli != null) && cli.Connected )
                cli.Close();
        }
        catch (Exception ex)
        {
            logger.LogException(ex.Message);
            bRet = false;
        }

        cli = null;
        stream = null;
            
        return bRet;
    }



    private bool ServerTaskMail( PIServerTask task )
    {
        bool bRet = false;
        Uri url = null;
        string proxyHost;
        int proxyPort;
        Dictionary<string, string> fields = null;
        int status = 0;
        Stream data = null;
        Encoding dataEncoding = null;

        lock (_syncRoot)
        {
            try
            {
                url = new Uri(_mailURL.AbsoluteUri + "?timecache=" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

            proxyHost = _proxyHost;
            proxyPort = _proxyPort;

            try
            {
                fields = new Dictionary<string, string> ();

                fields.Add("login", _user);
                fields.Add("mdp", _pass);
                fields.Add("action", "Envoyer");
                fields.Add("message", task.Message);

            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

        }

        try
        {
            bRet = HTTPPost(url, proxyHost, proxyPort, fields, out status, out data, out dataEncoding);
        }
        catch (Exception ex)
        {
            logger.LogException(ex.Message);
        }

        return bRet && (status == 200);
    }

    private bool ServerTaskStat( PIServerTask task )
    {
        bool bRet = false;
        Uri url = null;
        string proxyHost;
        int proxyPort;
        Dictionary<string, string> fields = null;
        int status = 0;
        Stream data = null;
        Encoding dataEncoding = null;

        lock (_syncRoot)
        {
            try
            {
                url = new Uri(_phantomJSURL.AbsoluteUri + "?timecache=" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

            proxyHost = _proxyHost;
            proxyPort = _proxyPort;

            try
            {
                fields = new Dictionary<string, string> ();

                fields.Add("login", _user);
                fields.Add("mdp", _pass);
                fields.Add("stat", task.Codes[0] );
                fields.Add("sample", task.Codes[1] );
                fields.Add("present", task.Codes[2] );
                fields.Add("datedeb", task.DateDebut.ToString("dd/MM/yyyy") );
                fields.Add("datefin", task.DateFin.ToString("dd/MM/yyyy") );
                fields.Add("height", task.Height.ToString() );
                fields.Add("width", task.Width.ToString() );
                fields.Add("page", "pistat" );
                fields.Add("render", "PNG" );
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

        }

        try
        {
            bRet = HTTPPost(url, proxyHost, proxyPort, fields, out status, out data, out dataEncoding);

            if( bRet && (status == 200) && (data != null) && (data.Length > 0) )
            {
                try
                {
                    byte[] filePNG = new byte[data.Length];
                    data.Read( filePNG, 0, (int)data.Length );
                         
                    lock (_syncRoot)
                    {
                        task.Image = filePNG;
                    }

                }
                catch (Exception ex)
                {
                    logger.LogException(ex.Message);
                    bRet = false;
                }


            }
        }
        catch (Exception ex)
        {
            logger.LogException(ex.Message);
        }

        return bRet && (status == 200);
    }

    private bool ServerTaskDataRealTime( )
    {
        bool bRet = false;
        Uri url = null;
        string proxyHost;
        int proxyPort;
        Dictionary<string, string> fields = null;
        int status = 0;
        Stream data = null;
        Encoding dataEncoding = null;

        lock (_syncRoot)
        {
            try
            {
                url = new Uri(_getJsonURL.AbsoluteUri + "?timecache=" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

            proxyHost = _proxyHost;
            proxyPort = _proxyPort;

            try
            {
                fields = new Dictionary<string, string> ();

                fields.Add("login", _user);
                fields.Add("mdp", _pass);
                fields.Add("date", DateTime.Now.ToString("dd/MM/yyyy") );

            }
            catch (Exception ex)
            {
                logger.LogException(ex.Message);
            }

        }

        try
        {
            bRet = HTTPPost(url, proxyHost, proxyPort, fields, out status, out data, out dataEncoding);

            if( bRet && (status == 200) && (data != null) )
            {
                //

                try
                {
                    StreamReader reader = new StreamReader( data, dataEncoding );
                    string txtJson = reader.ReadToEnd();

                    if( txtJson.Length > 0 )
                    {
                        //List <object> lstObj = (List <object>)Json.Deserialize(txtJson);
                        IDictionary lstObj = (IDictionary)Json.Deserialize(txtJson);
                        //IDictionary lstObj =  (IDictionary)JsonUtility.FromJson<IDictionary>(txtJson);

                        if( lstObj != null )
                        {
                            lock (_syncRoot)
                            {
                                //_realTimeData = lstObj;
                                AddAllRealTimeData( lstObj );
                            }
                        }

                        try
                        {
                            // Persistance du dernier fichier lu
                            File.WriteAllText(_realTimeDataFile, txtJson);
                        }
                        catch (Exception ex)
                        {
                            logger.LogWarn(ex.Message);

                            // Simple Warning, pas d'erreur dans ce cas car erreur mineur
                            //bRet = false;
                        }
                      
                    }

                }
                catch (Exception ex)
                {
                    logger.LogException(ex.Message);
                    bRet = false;
                }


            }
        }
        catch (Exception ex)
        {
            logger.LogException(ex.Message);
        }

        return bRet && (status == 200);
    }
   
    private void ServerThread( object param )
    {
        int loopTime;
        bool bStopServer;
        PIServerTask task;
        int maxTaskTry;
        DateTime curTime;
        DateTime lastTimeTryDataRealTime = DateTime.MinValue;

        lock (_syncRoot)
        {
            bStopServer = _bStopServer;
        }

        while (!bStopServer)
        {
            curTime = DateTime.Now;

            lock (_syncRoot)
            {
                loopTime = _loopTime;
                maxTaskTry = _maxTaskTry;

                task = null;
                if (_serverTaskList != null)
                {
                    foreach (PIServerTask t in _serverTaskList)
                    {
                        if (t.LastTimeTry.AddSeconds(loopTime / 1000) < curTime) // loopTime en ms -> / 1000
                        {
                            task = t;
                            break;
                        }
                    }
                }
            }

            // Traitement des task
            if (task != null)
            {
                bool bRet = false;

                if (task.IsCanceled)
                    bRet = true;
                else
                {
                    switch (task.TypeTask)
                    {
                        case PIServerTask.TypeServerTask.MAIL:
                            bRet = ServerTaskMail(task);
                            break;
                    
                        case PIServerTask.TypeServerTask.STAT:
                            bRet = ServerTaskStat(task);
                            break;

                        default:
                            bRet = true;
                            break;
                    }
                }

                curTime = DateTime.Now;

                task.LastTimeTry = curTime;
                task.TryCount += 1;
                        

                if (bRet || (task.TryCount >= maxTaskTry))
                {
                    task.IsFinished = true;

                    // On enleve la Tache
                    lock (_syncRoot)
                    {
                        if (_serverTaskList != null)
                        {
                            _serverTaskList.Remove(task);
                        }
                    }
                }
            }
            else
            {
                // Recuperation des Temps Réel
                if (lastTimeTryDataRealTime.AddSeconds(loopTime / 1000) < curTime) // loopTime en ms -> / 1000
                {
                    ServerTaskDataRealTime();
                    lastTimeTryDataRealTime = DateTime.Now;
                }
                else
                {
                    loopTime = (int)(curTime - lastTimeTryDataRealTime).TotalMilliseconds;

                     if( loopTime < 1000 )
                            loopTime = 1000; // Minimun une oause de 1S
                }


                if (_eWaitServer != null)
                    _eWaitServer.WaitOne(loopTime);
                else
                    Thread.Sleep(loopTime);
            }

            lock (_syncRoot)
            {
                bStopServer = _bStopServer;
            }
        }
    }


    // Getter / Setter
    public bool InitOK
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _initOK;
            }
        }
    }

    public IDictionary RealTimeData
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _realTimeData;
            }
        }
    }

    public DateTime RealTimeDate
    {
        get 
        { 
            lock (_syncRoot)
            {
                return _realTimeDate;
            }
        }
    }
        

    // Methode

    public float GetTagFloat( string tag )
    {
        float val = 0;

        if( (tag != null) && (tag.Length > 0) )
        {
            lock (_syncRoot)
            {
                try
                {
                    if (_realTimeData.ContainsKey(tag.ToLower()))
                        val = Convert.ToSingle(_realTimeData[tag.ToLower()]);
                }
                catch (Exception ex)
                {
                    logger.LogException(ex.Message);
                }
            }
        }

        return val;
    }

    public string GetTagString( string tag )
    {
        string val = "";

        if( (tag != null) && (tag.Length > 0) )
        {

            lock (_syncRoot)
            {
                try
                {
                    if (_realTimeData.ContainsKey(tag.ToLower()))
                        val = Convert.ToString(_realTimeData[tag.ToLower()]);
                }
                catch (Exception ex)
                {
                    logger.LogException(ex.Message);
                }
            }
        }

        return val;
    }

	public DateTime GetTagDate( string tag )
	{
		DateTime val = DateTime.MinValue;

		if( (tag != null) && (tag.Length > 0) )
		{

			lock (_syncRoot)
			{
				try
				{
					if (_realTimeData.ContainsKey(tag.ToLower()))
					{
						//DateTime.TryParse((string)_realTimeData[tag.ToLower()], out val);
						object t = _realTimeData[tag.ToLower()];
						if ((string)t == "")
						{
							val = DateTime.MinValue;
						}
						else
						{
							val = Convert.ToDateTime(t);
						}
					}
				}
				catch (Exception ex)
				{
					logger.LogException(ex.Message);
				}
			}
		}

		return val;
	}
        
   
    public PIServerTask SendMailMessage( string className, string methodeName, string mess )
    {
        StringBuilder message = new StringBuilder();
        PIServerTask task = null;

        logger.LogDebug(mess);

        if ((className.Length + methodeName.Length) > 0)
        {
            message.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            message.Append(" - " + className + "." + methodeName + "\r\n");
        }
        message.Append( mess );

        lock (_syncRoot)
        {
            if (_serverTaskList != null)
            {
                task = new PIServerTask(PIServerTask.TypeServerTask.MAIL, message.ToString());

                _serverTaskList.Add( task );

                if (_eWaitServer != null)
                    _eWaitServer.Set();
            }
        }

        return task;
    }


    public PIServerTask SendStatRequest( string codeStat, string codeSample, string codeGraph, DateTime dateDebut, DateTime dateFin, int width, int height )
    {
        PIServerTask task = null;

        logger.LogDebug(codeStat);

        lock (_syncRoot)
        {
            if (_serverTaskList != null)
            {
                task = new PIServerTask(PIServerTask.TypeServerTask.STAT, codeStat, codeSample, codeGraph, dateDebut, dateFin, width, height);

                _serverTaskList.Add( task );

                if (_eWaitServer != null)
                    _eWaitServer.Set();
            }
        }

        return task;
    }



    // Static Methode
    public static PIServerTask MailMessage( string className, string methodeName, string mess )
    {
        piserver piserverManager = piserver.Instance;

        return piserverManager.SendMailMessage(className, methodeName, mess);
    }

    public static PIServerTask MailMessage( string mess )
    {
        piserver piserverManager = piserver.Instance;

        return piserverManager.SendMailMessage("", "", mess);
    }

    public static PIServerTask StatRequest( string codeStat, string codeSample, string codeGraph, DateTime dateDebut, DateTime dateFin, int width, int height )
    {
        piserver piserverManager = piserver.Instance;

        return piserverManager.SendStatRequest( codeStat, codeSample, codeGraph, dateDebut, dateFin, width, height);
    }


    // Methode / Getter / Setter Static
    public static bool Ready
    {
        get 
        { 
            piserver piserverManager = piserver.Instance;
            return piserverManager.InitOK;
        }
    }   

}
