﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class slideshow : MonoBehaviour {
    public Texture2D iconSlideShowON;
    public Texture2D iconSlideShowOFF;
    public Image Cadenas;

    private bool initOK = false;
    private bool SlideShowActivate = true;
    private clientsetting client = null;
    private poi_data[] alldevices = null;
    keyboard keyboardobj = null;
    private int device_actif = 0;
    private int device_stat_active = 0;
    private int device_etape = 0;
    float timer_slideshow = 0.0f;


    private void Init()
    {
        keyboardobj = GameObject.FindObjectOfType<keyboard>();

        if (client != null)
        {
            // Force le prochain update
            SlideShowActivate = !client.PlayerConfig.SlideShowActivate;

            if( client.PlayerConfig.SlideShowActivate )
                StartCoroutine(SlideShow());
        }

        initOK = true;
    }

    // Use this for initialization
    IEnumerator Start () {

        client = GameObject.FindObjectOfType<clientsetting>();
        alldevices = poi_data.GetAllDevice();

        while (!downloadmanager.AllDownloaded)
            yield return null;

        if (client != null)
        {
            while (!client.PlayerConfigInitOK)
                yield return null;
        }

        if (alldevices != null)
        {
            bool test = false;
            while (!test)
            {
                test = true;
                for (int i = 0; i < alldevices.Length; i++)
                    if (!alldevices[i].TagsConfigInitOK)
                        test = false;
                yield return null;
            }
        }

        Init();
    }
	
	// Update is called once per frame
	void Update () {
        if (initOK)
        {
            if( (client != null) && (SlideShowActivate != client.PlayerConfig.SlideShowActivate) )
            {
                Texture2D tex = null;

                tex = (client.PlayerConfig.SlideShowActivate ? iconSlideShowON : iconSlideShowOFF);

                if( Cadenas != null )
                    Cadenas.sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 ());

                SlideShowActivate = client.PlayerConfig.SlideShowActivate;
            }
        }
	}
        
    IEnumerator SlideShow()
    {
        while (true)
        {
            timer_slideshow -= Time.deltaTime;
            if (timer_slideshow <= 0)
            {
                device_etape += 1;
                timer_slideshow = 0;

                switch (device_etape)
                {
                    case 1: // Traveling
                        if (keyboardobj)
                            keyboardobj.RemoveAllPopupScreen();
                        
                        if (client != null)
                        {
                            if (device_actif == 0) // Vue Usine
                            {
                                client.PlayerConfig.Area = "Usine";
                                timer_slideshow = 10.0f;
                            }
                            else if ((alldevices != null) && (device_actif <= alldevices.Length))
                            {
                                if (alldevices[device_actif - 1].TagsConfig.DayTime.GoodUnitsNumber.Value.Value <= 0) // Si Pas de piece on ne slide pas dessus (c'est que la ligne ne va pas fonctionner)
                                {
                                    device_actif++;
                                    device_etape = 0;
                                }
                                else
                                {
                                    client.PlayerConfig.Area = alldevices[device_actif - 1].TagsConfig.Name;
                                    timer_slideshow = 5.0f;
                                }
                            }
                            else
                            {
                                device_actif = 0;
                                device_etape = 0;
                            }
                        }
                        break;

                    case 2: // Show Info POI
                        if (client != null)
                        {
                            if ((alldevices != null) && (device_actif > 0) && (device_actif <= alldevices.Length))
                            {
                                if (alldevices[device_actif - 1].poi_fullscreen != null)
                                {
                                    keyboardobj.AddPopupScreen(alldevices[device_actif - 1].poi_fullscreen);
                                    alldevices[device_actif - 1].poi_fullscreen.onscreen = true;

                                    device_stat_active = 0;
                                    timer_slideshow = 10.0f;
                                }
                            }
                        }
                        break;

                    default:
                        device_actif++;
                        device_stat_active = 0;
                        device_etape = 0;

                        break;
                }
            }

            yield return null;
        }
    }

    public void Button_Pressed()
    {
        if (initOK)
        {
            if (client != null)
            {
                client.PlayerConfig.SlideShowActivate = !client.PlayerConfig.SlideShowActivate;
                client.PlayerConfig.SaveConfig();

                StopAllCoroutines();

                device_actif = 0;
                device_stat_active = 0;
                device_etape = 0;
                timer_slideshow = 0.0f;

                if( client.PlayerConfig.SlideShowActivate )
                    StartCoroutine(SlideShow());
            }
        }
    }
}
