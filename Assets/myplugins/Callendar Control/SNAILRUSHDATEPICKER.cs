using UnityEngine;
using System.Collections;
using System; //Unity does not add this by default when creating a new script
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Globalization;


public class SNAILRUSHDATEPICKER : MonoBehaviour 
{
	public GUISkin skin;
    public float CallendarScale;
    public bool AutoCulture;
    public string CallendarCulture;
    public string[] CustomdaysText;
    public int DayNamesChars;
    public bool UpperCase;

    private bool _DatePicker = false;
    private string _monthName;
    private string[] _days;


    private string[,] _CustomCallendar;
	
	private int _SelectCustomselectedday;
	
    private int _selectedday;
    private int _selectedrealday;
    private int _currentmonth=System.DateTime.Now.Month;
    private int _currentyear=System.DateTime.Now.Year;
    private int _currentDay=System.DateTime.Now.Day;

    private  bool _reInitCurrentDate = false;
    private  Rect _CallendarRectwindow = new Rect();
    private  Rect _CallendarRectwindowScaled = new Rect();

	//private GameObject _datepickerobject;

    // Ne Pas utiliser Awake mais PIAwake pour l'init
    void Awake()
	{
		//_datepickerobject = GameObject.Find("CanvasScreen").FindInChildren("datepickerobject");
	}

	void Start()
	{
        if (CallendarScale <= 0)
            CallendarScale = 1;

        float height, skinHeight;
        float width, skinWidth;

        GetSkinDefaultSize(out skinWidth, out skinHeight);

        /// Skin format skinWidth x skinHeight : on respect ce scale
        /// avec un date picker qui prends CallendarScale du screen
        if (Screen.width > Screen.height)
        {
            if (skinWidth >= skinHeight)
            {
                width = Screen.width * CallendarScale;
                height = width * skinHeight / skinWidth;
            }
            else
            {
                height = Screen.height * CallendarScale;
                width = height * skinWidth / skinHeight;
            }  
        }
        else
        {
            if (skinWidth >= skinHeight)
            {
                height = Screen.height * CallendarScale;
                width = height * skinWidth / skinHeight;
            }
            else
            {
                width = Screen.width * CallendarScale;
                height = width * skinHeight / skinWidth;
            }
        }
        
        _CallendarRectwindowScaled.width = width;
        _CallendarRectwindowScaled.height = height;
        _CallendarRectwindowScaled.x = (Screen.width - _CallendarRectwindowScaled.width)/2;
        _CallendarRectwindowScaled.y = (Screen.height - _CallendarRectwindowScaled.height)/2;

        _CallendarRectwindow.x = _CallendarRectwindowScaled.x;
        _CallendarRectwindow.y = _CallendarRectwindowScaled.y;
        _CallendarRectwindow.width = _CallendarRectwindowScaled.width;
        _CallendarRectwindow.height = _CallendarRectwindowScaled.height;



        if (AutoCulture)
        {
            for (int i = 1; i <= 7; ++i)
            {
                Debug.Log(i);
                if (i == 7)
                {
                    if (UpperCase)
                    {
                        CustomdaysText[i - 1] = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0].ToString().Substring(0, DayNamesChars).ToUpper();
                    }
                    else
                    {
                        CustomdaysText[i - 1] = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0].ToString().Substring(0, DayNamesChars);
                    }
                }
                else
                {	
                    if (UpperCase)
                    {
                        CustomdaysText[i - 1] = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[i].ToString().Substring(0, DayNamesChars).ToUpper();
                    }
                    else
                    {
                        CustomdaysText[i - 1] = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[i].ToString().Substring(0, DayNamesChars);
                    }
                }
            }
        }

        ReInitCalendar();
	}

	void Update()
	{
      /*  if (_datepickerobject != null)
        {
            if (_DatePicker)
                _datepickerobject.SetActive(true);
            else
                _datepickerobject.SetActive(false);
        }
*/
        // Verification du bon positionnement du DatePicker
        if (_CallendarRectwindowScaled.x < 0)
            _CallendarRectwindowScaled.x = 0;
        
        if ((_CallendarRectwindowScaled.x + _CallendarRectwindowScaled.width) > Screen.width)
            _CallendarRectwindowScaled.x = Screen.width - _CallendarRectwindowScaled.width;

        if (_CallendarRectwindowScaled.x < 0) // Centre en X
            _CallendarRectwindowScaled.x = (Screen.width - _CallendarRectwindowScaled.width)/2;


        if (_CallendarRectwindowScaled.y < 0)
            _CallendarRectwindowScaled.y = 0;

        if ((_CallendarRectwindowScaled.y + _CallendarRectwindowScaled.width) > Screen.height)
            _CallendarRectwindowScaled.y = Screen.height - _CallendarRectwindowScaled.height;

        if (_CallendarRectwindowScaled.y < 0) // Centre en Y
            _CallendarRectwindowScaled.y = (Screen.height - _CallendarRectwindowScaled.height)/2;
        
       
            
        _CallendarRectwindow.x = _CallendarRectwindowScaled.x;
        _CallendarRectwindow.y = _CallendarRectwindowScaled.y;
        _CallendarRectwindow.height = _CallendarRectwindowScaled.height;
        _CallendarRectwindow.width = _CallendarRectwindowScaled.width;
	}

   
    private bool GetImageSize(Texture2D asset, out int width, out int height)
    {
		/*
        if (asset != null) {
            string assetPath = UnityEditor.AssetDatabase.GetAssetPath(asset);
            UnityEditor.TextureImporter importer = UnityEditor.AssetImporter.GetAtPath(assetPath) as UnityEditor.TextureImporter;

            if (importer != null) {
                object[] args = new object[2] { 0, 0 };
                System.Reflection.MethodInfo mi = typeof(UnityEditor.TextureImporter).GetMethod("GetWidthAndHeight", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                mi.Invoke(importer, args);

                width = (int)args[0];
                height = (int)args[1];

                return true;
            }
        }
*/
        height = width = 0;
        return false;
    }

    private void GetSkinDefaultSize( out float skinWidth, out float skinHeight )
    {
        GUIStyle styleBackground = null;
        int texWidth = 0;
        int texHeight = 0;

        skinWidth = 0;
        skinHeight = 0;

        if( skin != null )
            styleBackground = skin.FindStyle( "datepickerwindow" );

        if (styleBackground != null)
        {
            skinWidth = styleBackground.fixedWidth;
            skinHeight = styleBackground.fixedHeight;

            if (styleBackground.normal.background != null)
                GetImageSize(styleBackground.normal.background, out texWidth, out  texHeight);

            // Si pas de taille fixe on prends la taille de la texture du skin comme reference
            if ((skinWidth <= 0) && (skinHeight <= 0))
            {
                skinWidth = (float)texWidth;
                skinHeight = (float)texHeight;
            }
            else if (skinWidth <= 0)
            {
                if( (texHeight > 0) && (texWidth > 0) )
                    skinWidth = texWidth * skinHeight / texHeight; 
            }
            else if (skinHeight <= 0)
            {
                if( (texHeight > 0) && (texWidth > 0) )
                    skinWidth = texHeight * skinWidth / texWidth;
            }
        }

        if (skinWidth <= 0)
            skinWidth = Screen.width;

        if (skinHeight <= 0)
            skinHeight = Screen.height;
    }

    private GUIStyle GetScaledStyle(GUIStyle style)
    {
        GUIStyle styleScaled = null;
        float skinWidth = 0;
        float skinHeight = 0;

        GetSkinDefaultSize(out skinWidth, out skinHeight);

        if( style != null )
        {
            styleScaled = new GUIStyle(style);

            if (style.fixedHeight > 0)
                styleScaled.fixedHeight = _CallendarRectwindowScaled.height * style.fixedHeight / skinHeight;

            if (style.fixedWidth > 0)
                styleScaled.fixedWidth = _CallendarRectwindowScaled.width * style.fixedWidth / skinWidth;

            if (style.fontSize > 0)
            {
                if( _CallendarRectwindowScaled.width < _CallendarRectwindowScaled.height )
                    styleScaled.fontSize = (int)(_CallendarRectwindowScaled.width * style.fontSize / skinWidth);
                else
                    styleScaled.fontSize = (int)(_CallendarRectwindowScaled.height * style.fontSize / skinHeight);  

                if (styleScaled.fontSize <= 0)
                    styleScaled.fontSize = 1;
            }
        }

        return styleScaled;
    }

    private GUIStyle GetScaledStyle(string styleName)
    {
        return( GetScaledStyle(skin.FindStyle(styleName)) );
    }

    private GUISkin GetScaledSkin(GUISkin skin)
    {
        GUISkin skinScaled = ScriptableObject.CreateInstance<GUISkin>();


        if ((skinScaled != null) && (skin != null))
        {

            skinScaled.font = skin.font;
            skinScaled.hideFlags = HideFlags.HideAndDontSave;
            skinScaled.name = skin.name+"_scaled";
            skinScaled.settings.cursorColor = skin.settings.cursorColor;
            skinScaled.settings.cursorFlashSpeed = skin.settings.cursorFlashSpeed;
            skinScaled.settings.doubleClickSelectsWord = skin.settings.doubleClickSelectsWord;
            skinScaled.settings.selectionColor = skin.settings.selectionColor;
            skinScaled.settings.tripleClickSelectsLine = skin.settings.tripleClickSelectsLine;

            skinScaled.box = GetScaledStyle(skin.box);
            skinScaled.button = GetScaledStyle(skin.button);
            skinScaled.horizontalScrollbar = GetScaledStyle(skin.horizontalScrollbar);
            skinScaled.horizontalScrollbarLeftButton = GetScaledStyle(skin.horizontalScrollbarLeftButton);
            skinScaled.horizontalScrollbarRightButton = GetScaledStyle(skin.horizontalScrollbarRightButton);
            skinScaled.horizontalScrollbarThumb = GetScaledStyle(skin.horizontalScrollbarThumb);
            skinScaled.horizontalSlider = GetScaledStyle(skin.horizontalSlider);
            skinScaled.horizontalSliderThumb = GetScaledStyle(skin.horizontalSliderThumb);
            skinScaled.label = GetScaledStyle(skin.label);
            skinScaled.scrollView = GetScaledStyle(skin.scrollView);
            skinScaled.textArea = GetScaledStyle(skin.textArea);
            skinScaled.textField = GetScaledStyle(skin.textField);
            skinScaled.toggle = GetScaledStyle(skin.toggle);
            skinScaled.verticalScrollbar = GetScaledStyle(skin.verticalScrollbar);
            skinScaled.verticalScrollbarDownButton = GetScaledStyle(skin.verticalScrollbarDownButton);
            skinScaled.verticalScrollbarThumb = GetScaledStyle(skin.verticalScrollbarThumb);
            skinScaled.verticalScrollbarUpButton = GetScaledStyle(skin.verticalScrollbarUpButton);
            skinScaled.verticalSlider = GetScaledStyle(skin.verticalSlider);
            skinScaled.verticalSliderThumb = GetScaledStyle(skin.verticalSliderThumb);
            skinScaled.window = GetScaledStyle(skin.window);

            skinScaled.customStyles = new GUIStyle[skin.customStyles.Length];



            for (int i = 0; i < skin.customStyles.Length; i++)
            {
                skinScaled.customStyles.SetValue( GetScaledStyle(skin.customStyles[i]), i );
            }

            //GUIStyle style = skinScaled.FindStyle("datepickerwindow");

        }

        //UnityEditor.AssetDatabase.CreateAsset(skinScaled, "Assets");

        return skinScaled;
    }

	void OnGUI () 
	{
		if (_DatePicker)
		{
            //if (skinScaled == null)
            //   skinScaled = GetScaledSkin(skin);
            GUI.skin = skin;
            _CallendarRectwindow = GUI.Window(26, _CallendarRectwindow, HISTORYWINDOWDATA,"",GetScaledStyle("datepickerwindow"));
			GUI.BringWindowToFront(26);
		}
	}
	
	private void HISTORYWINDOWDATA(int windowID)
    {
		GUILayout.BeginHorizontal();		
		//backbutton
        if(GUILayout.Button("", GetScaledStyle("datepickerarrowback") )) //,GUILayout.MaxWidth(40))){
        {
		    if (_currentmonth > 1)
            {
                _currentmonth -= 1;
            }
            else
            {
                _currentmonth = 12;
                _currentyear -= 1;
            }

            _reInitCurrentDate = true;
		}

        GUILayout.Label(_monthName + " ("+_currentyear+")",GetScaledStyle("datepickeaarseleection"));

		//nextbutton
        if(GUILayout.Button("", GetScaledStyle("datepickerarrowfront") )) //,GUILayout.MaxWidth(40))){
        {
		    if (_currentmonth < 12)
            {
                _currentmonth += 1;
            }
            else
            {
                _currentmonth = 1;
                _currentyear += 1;
            }

            _reInitCurrentDate = true;
		}

        if (_reInitCurrentDate)
        {
            ReInitCalendar();
            _reInitCurrentDate = false;
        }

       

		GUILayout.EndHorizontal();

		//GUILayout.Space(1);
		_SelectCustomselectedday=-1;
        _SelectCustomselectedday = GUILayout.SelectionGrid(_SelectCustomselectedday,CustomdaysText,_CustomCallendar.GetLength(1),GetScaledStyle("datepickerdatedays"));

		//GUILayout.Space(0);
            _selectedday= GUILayout.SelectionGrid(_selectedday,_days, _CustomCallendar.GetLength(1),GetScaledStyle("datepickerdatebuttons"));

		//GUILayout.Space(5);

		GUILayout.BeginHorizontal();
		//if(GUILayout.Button("Annuler",mystyle,GUILayout.MaxWidth(CallendarRectwindow.width/2)))
        if(GUILayout.Button("Annuler",GetScaledStyle("datepickerOKCancelButtons")))
		{
			_selectedrealday = 0;
			_DatePicker = false;
		}
		//if(GUILayout.Button("Valider",mystyle,GUILayout.MaxWidth(CallendarRectwindow.width/2)))
        if(GUILayout.Button("Valider",GetScaledStyle("datepickerOKCancelButtons")))
		{
			string myday = _days[_selectedday];
			if (myday != " ")
				int.TryParse(myday,out _selectedrealday);				 
			else
				_selectedrealday = 1;
			
			_DatePicker = false;
		}
		
        GUILayout.EndHorizontal();

        GUI.DragWindow (new Rect (0,0, 10000, 20));
	}




    private void ReInitCalendar( )
    {
        if (AutoCulture){
            //Culture of the user
            _monthName = new DateTime(_currentyear, _currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CurrentUICulture);
        }
        else
        {
            //Custom Culture
            _monthName = new DateTime(_currentyear, _currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CreateSpecificCulture(CallendarCulture));
        }
        _CustomCallendar = CreateCallendar(_currentmonth, _currentyear);
        _selectedday=0;  
        _days= new string [42];
        int k =0;
        for (int i = 0; i < 6; ++i)
        {
            for (int j = 0; j < 7; ++j)
            {
                //                  if (k<3) Debug.Log (CustomCallendar[i, j]);
                _days[k]= _CustomCallendar[i, j];
                if (_CustomCallendar[i, j]!=" ")
                {
                    /*
                    if (currentmonth==DateTime.Now.Month && currentyear==DateTime.Now.Year && DateTime.Now.Day==System.Convert.ToInt16(CustomCallendar[i, j]))
                    {
                        selectedday=k;
                        //                          Debug.Log ("Selected" + k);
                    }
                    */
                    if( _currentDay == System.Convert.ToInt16(_CustomCallendar[i, j]) )
                        _selectedday=k;
                }
                k+=1;
            }
        }
    }
	

    private String[,] CreateCallendar(int month, int year)  
    {
        string[,] customcallendar = new string[6, 7];
        int secondweekfirstday = 0;
		DateTime DateTrick= new DateTime();
		DateTrick=System.Convert.ToDateTime("11/01/" + year);
		DayOfWeek firstday;

		if (DateTrick.Month==1)
		{
		    firstday = System.Convert.ToDateTime("01/" + month + "/" + year).DayOfWeek;
		}
		else
		{
		    firstday = System.Convert.ToDateTime(month + "/01" + "/" + year).DayOfWeek;	
		}	
		switch (firstday)
        {
            case DayOfWeek.Monday:
                secondweekfirstday = 8;
                customcallendar[0, 0] = "1";
                customcallendar[0, 1] = "2";
                customcallendar[0, 2] = "3";
                customcallendar[0, 3] = "4";
                customcallendar[0, 4] = "5";
                customcallendar[0, 5] = "6";
                customcallendar[0, 6] = "7";
                break;
            case DayOfWeek.Tuesday:
                secondweekfirstday = 7;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = "1";
                customcallendar[0, 2] = "2";
                customcallendar[0, 3] = "3";
                customcallendar[0, 4] = "4";
                customcallendar[0, 5] = "5";
                customcallendar[0, 6] = "6";
                break;
            case DayOfWeek.Wednesday:
                secondweekfirstday = 6;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = " ";
                customcallendar[0, 2] = "1";
                customcallendar[0, 3] = "2";
                customcallendar[0, 4] = "3";
                customcallendar[0, 5] = "4";
                customcallendar[0, 6] = "5";
                break;
            case DayOfWeek.Thursday:
    	        secondweekfirstday = 5;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = " ";
                customcallendar[0, 2] = " ";
                customcallendar[0, 3] = "1";
                customcallendar[0, 4] = "2";
                customcallendar[0, 5] = "3";
                customcallendar[0, 6] = "4";
                break;
            case DayOfWeek.Friday:
                secondweekfirstday = 4;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = " ";
                customcallendar[0, 2] = " ";
                customcallendar[0, 3] = " ";
                customcallendar[0, 4] = "1";
                customcallendar[0, 5] = "2";
                customcallendar[0, 6] = "3";
                break;
            case DayOfWeek.Saturday:
                secondweekfirstday = 3;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = " ";
                customcallendar[0, 2] = " ";
                customcallendar[0, 3] = " ";
                customcallendar[0, 4] = " ";
                customcallendar[0, 5] = "1";
                customcallendar[0, 6] = "2";
                break;
            case DayOfWeek.Sunday:
                secondweekfirstday = 2;
                customcallendar[0, 0] = " ";
                customcallendar[0, 1] = " ";
                customcallendar[0, 2] = " ";
                customcallendar[0, 3] = " ";
                customcallendar[0, 4] = " ";
                customcallendar[0, 5] = " ";
                customcallendar[0, 6] = "1";
                break;
        }

        int daysInMonth = System.DateTime.DaysInMonth(_currentyear, _currentmonth);

        for (int i = 1; i < 6; ++i)
        {

            for (int j = 0; j < 7; ++j)
            {
                if (secondweekfirstday <= daysInMonth)
                {
                    customcallendar[i, j] = secondweekfirstday.ToString();
                    secondweekfirstday += 1;
                }
                else
                {
                    customcallendar[i, j] = " ";
                }

            }// j for
        }// i for

        return customcallendar;
    }


    public void ShowCalendar(DateTime date, RectTransform rectOwner)
    {
        

        _CallendarRectwindowScaled.x = (Screen.width - _CallendarRectwindowScaled.width)/2;
        _CallendarRectwindowScaled.y = (Screen.height - _CallendarRectwindowScaled.height)/2;

        if (rectOwner != null)
        {
            Vector3 worldPos;
            GameObject camGO = GameObject.Find("3D Camera");
            Camera cam = null;
        
            if( camGO != null )
                cam = camGO.GetComponent<Camera>();
            
            if( cam != null )
            {
                /*
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rectOwner, new Vector2(rectOwner.rect.width / 2, rectOwner.rect.height / 2), cam, out worldPos))
                {
                    _CallendarRectwindowScaled.x = worldPos.x;
                    _CallendarRectwindowScaled.y = worldPos.y;
                }
                */
            }

            //_CallendarRectwindowScaled.x = 100;
            //_CallendarRectwindowScaled.y = 200;
        }

        _currentyear = date.Year; 
        _currentmonth = date.Month;
        _currentDay = date.Day;

        _selectedrealday = 0;
        _reInitCurrentDate = true;
        _DatePicker = true;
    }

    public void ShowCalendar(DateTime date)
    {
        ShowCalendar(date, null);
    }

    public void HideCalendar( )
    {
        _DatePicker = false;
    }

    public bool CalendarIsShow( )
    {
        return _DatePicker;
    }

    public bool CalendarDateIsSelected(out DateTime date)
    {
        bool bOK = !_DatePicker;

        date = DateTime.MinValue;

        if (bOK && (_selectedrealday > 0))
        {
            date = new DateTime(_currentyear, _currentmonth, _selectedrealday, 00, 00, 00, 000);
        }

        return bOK;
    }




}